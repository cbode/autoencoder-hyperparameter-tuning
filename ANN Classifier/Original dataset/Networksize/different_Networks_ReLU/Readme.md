
Readme with Additional Information
==================================


This file contains additional Information for 20210918stratified_sampled_Dataset_different_Networks_ReLU.
# General Information


The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

2 [[64, 'relu'], [32, 'relu'], [6, 'softmax']]

3 [[128, 'relu'], [64, 'relu'], [32, 'relu'], [6, 'softmax']]

4 [[256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [6, 'softmax']]

5 [[512, 'relu'], [256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L2 Regularization: [None]

DropOut Regularization: [0.0]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7feceda5b0d0>, <keras.optimizers.SGD object at 0x7fecef6b1a90>, <keras.optimizers.SGD object at 0x7fed37dd19d0>]

batch_size: 100
# Personal Notes/Additional Information
