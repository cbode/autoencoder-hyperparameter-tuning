
Readme with Additional Information
==================================


This file contains additional Information for 20210920stratified_sampled_Dataset_different_Networks_tanh_v2.
# General Information


The Network Structures in detail are: 

1 [[32, 'tanh'], [6, 'softmax']]

2 [[64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

3 [[128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

4 [[256, 'tanh'], [128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

5 [[512, 'tanh'], [256, 'tanh'], [128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L2 Regularization: [None]

DropOut Regularization: [0.0]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7febdeb94f50>, <keras.optimizers.SGD object at 0x7febae935f50>, <keras.optimizers.SGD object at 0x7feaaeca98d0>]

batch_size: 100
# Personal Notes/Additional Information
