
Readme with Additional Information
==================================


This file contains additional Information for 20210927stratified_sampled_Dataset_L1 Regularization_different_DropOut.
# General Information


The Network Structures in detail are: 

1 [[512, 'tanh'], [256, 'tanh'], [128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L1 Regularization: [0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03]

DropOut Regularization: [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7fb4006d2690>, <keras.optimizers.SGD object at 0x7fbae9ce3750>, <keras.optimizers.SGD object at 0x7fb400f57a50>]

batch_size: 100
# Personal Notes/Additional Information
