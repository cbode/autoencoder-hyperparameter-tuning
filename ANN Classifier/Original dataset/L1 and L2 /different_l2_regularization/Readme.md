
Readme with Additional Information
==================================


This file contains additional Information for 20210922stratified_sampled_Dataset_different_l2_regularization_v2.
# General Information


The Network Structures in detail are: 

1 [[512, 'tanh'], [256, 'tanh'], [128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L2 Regularization: [1e-06, 1e-06, 1e-06, 3e-06, 3e-06, 3e-06, 7e-06, 7e-06, 7e-06, 1e-05, 1e-05, 1e-05, 3e-05, 3e-05, 3e-05, 7e-05, 7e-05, 7e-05, 0.0001, 0.0001, 0.0001, 0.0003, 0.0003, 0.0003, 0.0007, 0.0007, 0.0007, 0.001, 0.001, 0.001, 0.003, 0.003, 0.003, 0.007, 0.007, 0.007, 0.01, 0.01, 0.01, 0.03, 0.03, 0.03, 0.07, 0.07, 0.07, 0.1, 0.1, 0.1, 1e-06, 1e-06, 1e-06, 3e-06, 3e-06, 3e-06, 7e-06, 7e-06, 7e-06, 1e-05, 1e-05, 1e-05, 3e-05, 3e-05, 3e-05, 7e-05, 7e-05, 7e-05, 0.0001, 0.0001, 0.0001, 0.0003, 0.0003, 0.0003, 0.0007, 0.0007, 0.0007, 0.001, 0.001, 0.001, 0.003, 0.003, 0.003, 0.007, 0.007, 0.007, 0.01, 0.01, 0.01, 0.03, 0.03, 0.03, 0.07, 0.07, 0.07, 0.1, 0.1, 0.1, 1e-06, 1e-06, 1e-06, 3e-06, 3e-06, 3e-06, 7e-06, 7e-06, 7e-06, 1e-05, 1e-05, 1e-05, 3e-05, 3e-05, 3e-05, 7e-05, 7e-05, 7e-05, 0.0001, 0.0001, 0.0001, 0.0003, 0.0003, 0.0003, 0.0007, 0.0007, 0.0007, 0.001, 0.001, 0.001, 0.003, 0.003, 0.003, 0.007, 0.007, 0.007, 0.01, 0.01, 0.01, 0.03, 0.03, 0.03, 0.07, 0.07, 0.07, 0.1, 0.1, 0.1]

DropOut Regularization: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7fea7a4dbd10>, <keras.optimizers.SGD object at 0x7fea7a4db490>, <keras.optimizers.SGD object at 0x7feb61fc5610>]

batch_size: 100
# Personal Notes/Additional Information
