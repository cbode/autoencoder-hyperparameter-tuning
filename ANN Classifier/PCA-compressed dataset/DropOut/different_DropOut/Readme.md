
Readme with Additional Information
==================================


This file contains additional Information for different_DropOut
# General Information


The Network Structures in detail are: 

1 [[512, 'relu'], [256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

EarlyStopping: 200 Epochs after highest Training Accuracy

LR: [0.001]

DropOut = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5]
L2 Regularization = None 

Max number of Epochs: 5000

Momentum = 0.9 
Opimizer = [<keras.optimizers.SGD object at 0x7f848eb2d3d0>, <keras.optimizers.SGD object at 0x7f859e7ac550>, <keras.optimizers.SGD object at 0x7f859e7ac2d0>]

batch_size = 150
# Personal Notes/Additional Information
There is also a smaller Rerun without EarlyStopping (see 20210611_Comparison_DropOut_without_EarlyStopping)