#Readme with additional Information
This file contains additional Information for different_Networksizes_different_Learningrate
The Data is: 32_dim_PCA_compressed
The Network Structures in detail are: 

1. [[16,'relu'],[6,'softmax']] 
2. [[32,'relu'],[16, 'relu'],[6,'softmax']]
3. [[128,'relu'],[64,'relu'],[32,'relu'],[16, 'relu'],[6,'softmax']]
4. [[256,'relu'],[128,'relu'],[64,'relu'],[32,'relu'],[16, 'relu'],[6,'softmax']]
5. [[512,'relu'],[256,'relu'],[128,'relu'],[64,'relu'],[32,'relu'],[16, 'relu'],[6,'softmax']]

EarlyStopping: 200 Epochs after highest Training Accuracy
LR: 0.0001, 0.001, 0.01
Max Number Epochs = 5000
optimizer = SGD (Momentum = 0.9)
The optimizer was run twice like that 
batch_size = 150 
