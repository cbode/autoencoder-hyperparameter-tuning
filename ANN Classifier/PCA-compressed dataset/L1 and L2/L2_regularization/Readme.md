
Readme with Additional Information
==================================


This file contains additional Information for L2_regularization
# General Information


The Network Structures in detail are: 

1 [[256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

EarlyStopping: 200 Epochs after highest Training Accuracy

LR: [0.001]

Max number of Epochs: 5000

Opimizer = [<keras.optimizers.SGD object at 0x7f872b714490>, <keras.optimizers.SGD object at 0x7f872b7144d0>, <keras.optimizers.SGD object at 0x7f86d89ec590>]

batch_size150
# Personal Notes/Additional Information
