
Readme with Additional Information
==================================


This file contains additional Information for different_Networksizes_tanh_Relu_comparison_different_Learningrate
# General Information
Data: 32_dim_PCA_compressed
The Network Structures in detail are: 

1 [[32, 'relu'], [16, 'relu'], [6, 'softmax']]

2 [[512, 'relu'], [256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

3 [[32, 'tanh'], [16, 'tanh'], [6, 'softmax']]

4 [[512, 'tanh'], [256, 'tanh'], [128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [16, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after highest Training Accuracy

LR: [0.001]

Max number of Epochs: 5000

Opimizer = [<keras.optimizers.SGD, Momentum = 0.9>, <keras.optimizers.SGD, Momentum = 0.9>]

batch_size: 150

# Personal Notes/Additional Information
