
Readme with Additional Information
==================================


This file contains additional Information for 20211029stratified_sampled_Dataset_different_Activation_function_different_optimizers.
# General Information


The Network Structures in detail are: 

1 [[64, 'relu'], [32, 'relu'], [6, 'softmax']]

2 [[64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L1 Regularization: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

DropOut Regularization: [0.0]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.Adam object at 0x7fafe3609210>, <keras.optimizers.Adam object at 0x7fafe2500410>, <keras.optimizers.Adam object at 0x7fafe2500f10>, <keras.optimizers.SGD object at 0x7fafbaac7150>, <keras.optimizers.SGD object at 0x7faf57cc27d0>, <keras.optimizers.SGD object at 0x7faf57de08d0>]

batch_size: 100
# Personal Notes/Additional Information
