
Readme with Additional Information
==================================


This file contains additional Information for 20211105stratified_sampled_Dataset_on_reconstructed_Dataset_DropOut_only_Inputlayer_fixed.
# General Information


The Network Structures in detail are: 

1 [[64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L1 Regularization: [0.0]	 on layer: None

L2 Regularization: [0.0001]	 on layer: Input

DropOut Regularization: [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]	 on layer: Input

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7f83b7232510>, <keras.optimizers.SGD object at 0x7f84334a0c10>, <keras.optimizers.SGD object at 0x7f84334a0590>]

batch_size: 150
# Personal Notes/Additional Information
