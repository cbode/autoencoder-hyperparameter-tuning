
Readme with Additional Information
==================================


This file contains additional Information for 20211027stratified_sampled_Dataset_different_Networksizes.
# General Information


The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

2 [[64, 'relu'], [32, 'relu'], [6, 'softmax']]

3 [[128, 'relu'], [64, 'relu'], [32, 'relu'], [6, 'softmax']]

4 [[32, 'tanh'], [6, 'softmax']]

5 [[64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

6 [[128, 'tanh'], [64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: 200 Epochs after lowest val loss

LR: [0.0001]

L1 Regularization: [0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03]

DropOut Regularization: [0.0]

Number of Epochs: 3000

Opimizer: [<keras.optimizers.SGD object at 0x7fea37a08e50>, <keras.optimizers.SGD object at 0x7fe88ce05990>, <keras.optimizers.SGD object at 0x7fe88af39950>]

batch_size: 100
# Personal Notes/Additional Information
