
Readme with Additional Information
==================================


This file contains additional Information for 20211020_32_dim_2000_Epochs_new_Stratified_Sampled_different_Networks .
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[16, 'relu'], [6, 'softmax']]

2 [[32, 'relu'], [6, 'softmax']]

3 [[32, 'relu'], [16, 'relu'], [6, 'softmax']]

4 [[128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

5 [[256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

6 [[512, 'relu'], [256, 'relu'], [128, 'relu'], [64, 'relu'], [32, 'relu'], [16, 'relu'], [6, 'softmax']]

EarlyStopping: None

LR: [0.001]

L2 Regularization: [None]

DropOut Regularization: [0.0]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7fb6705f8910>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb54cd49f90>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb5b0b2b2d0>, 'Adam'], [<keras.optimizers.SGD object at 0x7fb4b5f53190>, 'SGD'], [<keras.optimizers.SGD object at 0x7fb3764e9fd0>, 'SGD'], [<keras.optimizers.SGD object at 0x7fb3764e9ed0>, 'SGD']]

batch_size: 150
# Personal Notes/Additional Information
