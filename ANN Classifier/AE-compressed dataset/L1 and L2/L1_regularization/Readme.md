
Readme with Additional Information
==================================


This file contains additional Information for 20211020_32_dim_2000_Epochs_new_Stratified_Sampled_L1_regularization_on_Inputlayer.
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

EarlyStopping: None

LR: [0.0001]

L1 Regularization: [0.0,0.000001,0.000003,0.000007,0.00001,0.00003,0.00007,0.0001,0.0003,0.0007,0.001,0.003,0.007,0.01,0.03,0.07,0.1,0.3,0.7]

L2 Regularization: [0.0]

DropOut Regularization: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7fb225620050>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb225527f50>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb225527a90>, 'Adam']]

batch_size: 150
# Personal Notes/Additional Information
