
Readme with Additional Information
==================================


This file contains additional Information for 20211021_32_dim_2000_Epochs_new_Stratified_Sampled_L2_regularization_on_Inputlayer_high_values.
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

EarlyStopping: None

LR: [0.0001]

L2 Regularization: [<keras.regularizers.L1L2 object at 0x7fb41c676dd0>, <keras.regularizers.L1L2 object at 0x7fb445cd5550>, <keras.regularizers.L1L2 object at 0x7fb445cd5390>, <keras.regularizers.L1L2 object at 0x7fb445cd5510>, <keras.regularizers.L1L2 object at 0x7fb445cd5610>, <keras.regularizers.L1L2 object at 0x7fb445cd5590>, <keras.regularizers.L1L2 object at 0x7fb445cd5710>, <keras.regularizers.L1L2 object at 0x7fb445cd54d0>]

DropOut Regularization: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7fb2b4ed4e10>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb2740823d0>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb2753cf910>, 'Adam']]

batch_size: 150
# Personal Notes/Additional Information
