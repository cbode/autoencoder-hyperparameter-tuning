
Readme with Additional Information
==================================


This file contains additional Information for 20211021_32_dim_2000_Epochs_new_Stratified_Sampled_different_optimizer_different_activation_function.
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

2 [[64, 'relu'], [32, 'relu'], [6, 'softmax']]

3 [[32, 'tanh'], [6, 'softmax']]

4 [[64, 'tanh'], [32, 'tanh'], [6, 'softmax']]

EarlyStopping: None

LR: [0.0001]

L2 Regularization: [None]

DropOut Regularization: [0.0]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7fb2b4ed4e10>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb2302c6e10>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb6b0511e50>, 'Adam'], [<keras.optimizers.SGD object at 0x7fb22006a050>, 'SGD'], [<keras.optimizers.SGD object at 0x7faf40c77a10>, 'SGD'], [<keras.optimizers.SGD object at 0x7fb2ca74a510>, 'SGD']]

batch_size: 150
# Personal Notes/Additional Information
