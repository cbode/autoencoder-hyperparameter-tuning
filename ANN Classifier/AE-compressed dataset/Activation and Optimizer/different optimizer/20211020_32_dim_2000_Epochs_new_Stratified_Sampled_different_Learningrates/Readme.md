
Readme with Additional Information
==================================


This file contains additional Information for 20211020_32_dim_2000_Epochs_new_Stratified_Sampled_different_Learningrates.
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

EarlyStopping: None

LR: [1e-06, 1e-05, 0.0001, 0.001, 0.01, 0.1]

L2 Regularization: [None]

DropOut Regularization: [0.0]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7fb6705f8910>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb3761b7c50>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb3dfbc9c10>, 'Adam'], [<keras.optimizers.SGD object at 0x7fb1dbbc2bd0>, 'SGD'], [<keras.optimizers.SGD object at 0x7fb244974090>, 'SGD'], [<keras.optimizers.SGD object at 0x7fb2449748d0>, 'SGD']]

batch_size: 150
# Personal Notes/Additional Information
