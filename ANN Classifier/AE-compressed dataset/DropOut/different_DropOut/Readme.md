
Readme with Additional Information
==================================


This file contains additional Information for 20211021_32_dim_2000_Epochs_new_Stratified_Sampled_different_DropOut_in_Inputlayer.
# General Information


Dataset: 32_dim_2000_Epochs_new

Split Style: Stratified based on the Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

The Network Structures in detail are: 

1 [[32, 'relu'], [6, 'softmax']]

EarlyStopping: None

LR: [0.0001]

L2 Regularization: [None, None, None, None, None, None, None, None, None, None]

DropOut Regularization: [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

Max number of Epochs: 2000

Opimizer: [[<keras.optimizers.Adam object at 0x7faffcc5bcd0>, 'Adam'], [<keras.optimizers.Adam object at 0x7faffcc656d0>, 'Adam'], [<keras.optimizers.Adam object at 0x7fb3cc73a110>, 'Adam']]

batch_size: 150
# Personal Notes/Additional Information
