
Readme with Additional Information
==================================


This file contains additional Information for 20220113_NB_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 16.67%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 25.00%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 29.17%	Rep3: 37.50%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 41.67%	Rep3: 12.50%	Rep4: 33.33%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 25.00%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 16.67%	Rep3: 16.67%	Rep4: 33.33%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 20.83%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ062', 'TSQ119', 'TSQ078', 'TSQ046', 'TSQ028', 'TSQ049', 'TSQ051',
       'TSQ026', 'TSQ126', 'TSQ158', 'TSQ143', 'TSQ159', 'TSQ089', 'TSQ104',
       'TSQ076', 'TSQ043', 'TSQ156', 'TSQ166', 'TSQ038', 'TSQ005', 'TSQ153',
       'TSQ030', 'TSQ095', 'TSQ123'],
      dtype='object', name='Sample_ID'), Index(['TSQ016', 'TSQ152', 'TSQ090', 'TSQ114', 'TSQ118', 'TSQ145', 'TSQ066',
       'TSQ129', 'TSQ100', 'TSQ038', 'TSQ055', 'TSQ023', 'TSQ165', 'TSQ119',
       'TSQ026', 'TSQ111', 'TSQ140', 'TSQ086', 'TSQ044', 'TSQ125', 'TSQ057',
       'TSQ045', 'TSQ124', 'TSQ112'],
      dtype='object', name='Sample_ID'), Index(['TSQ009', 'TSQ038', 'TSQ106', 'TSQ125', 'TSQ104', 'TSQ073', 'TSQ130',
       'TSQ070', 'TSQ086', 'TSQ093', 'TSQ148', 'TSQ101', 'TSQ029', 'TSQ109',
       'TSQ040', 'TSQ021', 'TSQ057', 'TSQ003', 'TSQ016', 'TSQ128', 'TSQ163',
       'TSQ118', 'TSQ136', 'TSQ111'],
      dtype='object', name='Sample_ID'), Index(['TSQ086', 'TSQ072', 'TSQ021', 'TSQ118', 'TSQ027', 'TSQ122', 'TSQ133',
       'TSQ046', 'TSQ087', 'TSQ091', 'TSQ125', 'TSQ154', 'TSQ073', 'TSQ159',
       'TSQ135', 'TSQ065', 'TSQ006', 'TSQ099', 'TSQ170', 'TSQ077', 'TSQ126',
       'TSQ152', 'TSQ134', 'TSQ153'],
      dtype='object', name='Sample_ID'), Index(['TSQ163', 'TSQ161', 'TSQ114', 'TSQ009', 'TSQ143', 'TSQ127', 'TSQ112',
       'TSQ019', 'TSQ053', 'TSQ007', 'TSQ032', 'TSQ038', 'TSQ168', 'TSQ023',
       'TSQ030', 'TSQ051', 'TSQ052', 'TSQ020', 'TSQ154', 'TSQ109', 'TSQ174',
       'TSQ069', 'TSQ152', 'TSQ043'],
      dtype='object', name='Sample_ID'), Index(['TSQ115', 'TSQ165', 'TSQ079', 'TSQ092', 'TSQ076', 'TSQ003', 'TSQ074',
       'TSQ032', 'TSQ134', 'TSQ070', 'TSQ109', 'TSQ087', 'TSQ043', 'TSQ037',
       'TSQ130', 'TSQ065', 'TSQ135', 'TSQ167', 'TSQ118', 'TSQ162', 'TSQ124',
       'TSQ119', 'TSQ170', 'TSQ068'],
      dtype='object', name='Sample_ID'), Index(['TSQ154', 'TSQ137', 'TSQ026', 'TSQ067', 'TSQ035', 'TSQ091', 'TSQ038',
       'TSQ130', 'TSQ140', 'TSQ151', 'TSQ117', 'TSQ049', 'TSQ147', 'TSQ123',
       'TSQ176', 'TSQ016', 'TSQ163', 'TSQ030', 'TSQ174', 'TSQ019', 'TSQ002',
       'TSQ054', 'TSQ072', 'TSQ086'],
      dtype='object', name='Sample_ID'), Index(['TSQ019', 'TSQ075', 'TSQ027', 'TSQ157', 'TSQ089', 'TSQ122', 'TSQ109',
       'TSQ161', 'TSQ055', 'TSQ124', 'TSQ078', 'TSQ067', 'TSQ125', 'TSQ127',
       'TSQ135', 'TSQ159', 'TSQ047', 'TSQ076', 'TSQ176', 'TSQ059', 'TSQ028',
       'TSQ099', 'TSQ025', 'TSQ072'],
      dtype='object', name='Sample_ID'), Index(['TSQ026', 'TSQ072', 'TSQ009', 'TSQ112', 'TSQ158', 'TSQ089', 'TSQ075',
       'TSQ124', 'TSQ020', 'TSQ044', 'TSQ063', 'TSQ001', 'TSQ059', 'TSQ079',
       'TSQ099', 'TSQ069', 'TSQ005', 'TSQ170', 'TSQ130', 'TSQ161', 'TSQ041',
       'TSQ143', 'TSQ107', 'TSQ156'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ174', 'TSQ073', 'TSQ045', 'TSQ023', 'TSQ167', 'TSQ162', 'TSQ057',
       'TSQ054', 'TSQ171', 'TSQ145',
       ...
       'TSQ101', 'TSQ010', 'TSQ129', 'TSQ135', 'TSQ114', 'TSQ125', 'TSQ035',
       'TSQ092', 'TSQ112', 'TSQ152'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ019', 'TSQ065', 'TSQ130', 'TSQ072', 'TSQ089', 'TSQ115', 'TSQ082',
       'TSQ158', 'TSQ013', 'TSQ154',
       ...
       'TSQ151', 'TSQ108', 'TSQ106', 'TSQ126', 'TSQ169', 'TSQ006', 'TSQ021',
       'TSQ146', 'TSQ078', 'TSQ049'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ174', 'TSQ108', 'TSQ023', 'TSQ107', 'TSQ077', 'TSQ127',
       'TSQ126', 'TSQ044', 'TSQ103',
       ...
       'TSQ010', 'TSQ020', 'TSQ156', 'TSQ025', 'TSQ046', 'TSQ007', 'TSQ144',
       'TSQ037', 'TSQ137', 'TSQ113'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ140', 'TSQ069', 'TSQ032', 'TSQ174', 'TSQ168', 'TSQ123', 'TSQ074',
       'TSQ078', 'TSQ146', 'TSQ093',
       ...
       'TSQ049', 'TSQ088', 'TSQ175', 'TSQ113', 'TSQ101', 'TSQ020', 'TSQ039',
       'TSQ043', 'TSQ079', 'TSQ067'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ046', 'TSQ106', 'TSQ167', 'TSQ025', 'TSQ130', 'TSQ060', 'TSQ074',
       'TSQ137', 'TSQ156', 'TSQ072',
       ...
       'TSQ126', 'TSQ003', 'TSQ026', 'TSQ100', 'TSQ047', 'TSQ029', 'TSQ146',
       'TSQ087', 'TSQ123', 'TSQ176'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ153', 'TSQ147', 'TSQ114', 'TSQ104', 'TSQ121', 'TSQ091', 'TSQ143',
       'TSQ052', 'TSQ137', 'TSQ045',
       ...
       'TSQ030', 'TSQ002', 'TSQ023', 'TSQ128', 'TSQ039', 'TSQ151', 'TSQ026',
       'TSQ113', 'TSQ123', 'TSQ107'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ108', 'TSQ135', 'TSQ167', 'TSQ060', 'TSQ144', 'TSQ121', 'TSQ159',
       'TSQ124', 'TSQ133', 'TSQ175',
       ...
       'TSQ032', 'TSQ005', 'TSQ028', 'TSQ092', 'TSQ126', 'TSQ090', 'TSQ125',
       'TSQ023', 'TSQ009', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ016', 'TSQ087', 'TSQ082', 'TSQ032', 'TSQ165', 'TSQ007', 'TSQ005',
       'TSQ119', 'TSQ039', 'TSQ175',
       ...
       'TSQ134', 'TSQ080', 'TSQ044', 'TSQ045', 'TSQ057', 'TSQ174', 'TSQ029',
       'TSQ043', 'TSQ144', 'TSQ121'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ171', 'TSQ175', 'TSQ087', 'TSQ091', 'TSQ055', 'TSQ106', 'TSQ117',
       'TSQ028', 'TSQ024', 'TSQ080',
       ...
       'TSQ046', 'TSQ074', 'TSQ129', 'TSQ118', 'TSQ077', 'TSQ021', 'TSQ060',
       'TSQ114', 'TSQ078', 'TSQ108'],
      dtype='object', name='Sample_ID', length=109)]