
Readme with Additional Information
==================================


This file contains additional Information for 20220113_NB_9_random_runs.
# General Information


Gaussian naive Bayes Classifier 
with default settings.

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 12.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 37.50%	Rep2: 16.67%	Rep3: 29.17%	Rep4: 16.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 16.67%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
		 Fb_RKG	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 29.17%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 4.17%	Rep3: 29.17%	Rep4: 37.50%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 25.00%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ122', 'TSQ002', 'TSQ007', 'TSQ072', 'TSQ078', 'TSQ001', 'TSQ108',
       'TSQ070', 'TSQ043', 'TSQ077', 'TSQ047', 'TSQ127', 'TSQ113', 'TSQ067',
       'TSQ148', 'TSQ090', 'TSQ006', 'TSQ023', 'TSQ115', 'TSQ073', 'TSQ144',
       'TSQ174', 'TSQ092', 'TSQ062'],
      dtype='object', name='Sample_ID'), Index(['TSQ117', 'TSQ162', 'TSQ168', 'TSQ023', 'TSQ152', 'TSQ134', 'TSQ055',
       'TSQ057', 'TSQ088', 'TSQ027', 'TSQ062', 'TSQ049', 'TSQ137', 'TSQ080',
       'TSQ039', 'TSQ111', 'TSQ119', 'TSQ005', 'TSQ016', 'TSQ060', 'TSQ112',
       'TSQ093', 'TSQ092', 'TSQ082'],
      dtype='object', name='Sample_ID'), Index(['TSQ154', 'TSQ169', 'TSQ049', 'TSQ175', 'TSQ079', 'TSQ086', 'TSQ101',
       'TSQ002', 'TSQ078', 'TSQ162', 'TSQ127', 'TSQ153', 'TSQ030', 'TSQ023',
       'TSQ028', 'TSQ063', 'TSQ077', 'TSQ099', 'TSQ157', 'TSQ136', 'TSQ054',
       'TSQ167', 'TSQ037', 'TSQ029'],
      dtype='object', name='Sample_ID'), Index(['TSQ114', 'TSQ028', 'TSQ069', 'TSQ039', 'TSQ109', 'TSQ111', 'TSQ117',
       'TSQ070', 'TSQ095', 'TSQ135', 'TSQ029', 'TSQ025', 'TSQ078', 'TSQ176',
       'TSQ026', 'TSQ159', 'TSQ147', 'TSQ023', 'TSQ093', 'TSQ104', 'TSQ030',
       'TSQ079', 'TSQ076', 'TSQ088'],
      dtype='object', name='Sample_ID'), Index(['TSQ103', 'TSQ157', 'TSQ099', 'TSQ167', 'TSQ069', 'TSQ063', 'TSQ078',
       'TSQ037', 'TSQ045', 'TSQ128', 'TSQ077', 'TSQ067', 'TSQ059', 'TSQ088',
       'TSQ168', 'TSQ060', 'TSQ072', 'TSQ145', 'TSQ126', 'TSQ112', 'TSQ010',
       'TSQ006', 'TSQ082', 'TSQ174'],
      dtype='object', name='Sample_ID'), Index(['TSQ158', 'TSQ117', 'TSQ157', 'TSQ099', 'TSQ027', 'TSQ041', 'TSQ076',
       'TSQ052', 'TSQ051', 'TSQ031', 'TSQ169', 'TSQ044', 'TSQ174', 'TSQ078',
       'TSQ108', 'TSQ137', 'TSQ063', 'TSQ068', 'TSQ101', 'TSQ176', 'TSQ111',
       'TSQ130', 'TSQ032', 'TSQ127'],
      dtype='object', name='Sample_ID'), Index(['TSQ145', 'TSQ135', 'TSQ169', 'TSQ117', 'TSQ041', 'TSQ091', 'TSQ156',
       'TSQ115', 'TSQ153', 'TSQ068', 'TSQ108', 'TSQ163', 'TSQ078', 'TSQ024',
       'TSQ025', 'TSQ175', 'TSQ111', 'TSQ047', 'TSQ069', 'TSQ106', 'TSQ129',
       'TSQ161', 'TSQ109', 'TSQ034'],
      dtype='object', name='Sample_ID'), Index(['TSQ152', 'TSQ122', 'TSQ078', 'TSQ130', 'TSQ107', 'TSQ075', 'TSQ060',
       'TSQ006', 'TSQ065', 'TSQ113', 'TSQ073', 'TSQ026', 'TSQ115', 'TSQ039',
       'TSQ070', 'TSQ153', 'TSQ104', 'TSQ063', 'TSQ118', 'TSQ123', 'TSQ148',
       'TSQ157', 'TSQ170', 'TSQ121'],
      dtype='object', name='Sample_ID'), Index(['TSQ151', 'TSQ101', 'TSQ113', 'TSQ087', 'TSQ127', 'TSQ125', 'TSQ001',
       'TSQ051', 'TSQ075', 'TSQ158', 'TSQ077', 'TSQ034', 'TSQ037', 'TSQ070',
       'TSQ134', 'TSQ103', 'TSQ167', 'TSQ111', 'TSQ052', 'TSQ148', 'TSQ091',
       'TSQ163', 'TSQ165', 'TSQ095'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ059', 'TSQ053', 'TSQ156', 'TSQ152', 'TSQ087', 'TSQ068', 'TSQ154',
       'TSQ032', 'TSQ158', 'TSQ169',
       ...
       'TSQ119', 'TSQ124', 'TSQ143', 'TSQ074', 'TSQ128', 'TSQ010', 'TSQ063',
       'TSQ162', 'TSQ075', 'TSQ104'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ146', 'TSQ103', 'TSQ157', 'TSQ133', 'TSQ053', 'TSQ127', 'TSQ054',
       'TSQ034', 'TSQ070', 'TSQ059',
       ...
       'TSQ020', 'TSQ121', 'TSQ107', 'TSQ101', 'TSQ068', 'TSQ151', 'TSQ063',
       'TSQ032', 'TSQ076', 'TSQ037'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ075', 'TSQ118', 'TSQ130', 'TSQ021', 'TSQ053', 'TSQ067', 'TSQ082',
       'TSQ043', 'TSQ088', 'TSQ124',
       ...
       'TSQ003', 'TSQ146', 'TSQ114', 'TSQ032', 'TSQ104', 'TSQ013', 'TSQ126',
       'TSQ108', 'TSQ135', 'TSQ115'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ019', 'TSQ174', 'TSQ170', 'TSQ055', 'TSQ145', 'TSQ063', 'TSQ027',
       'TSQ003', 'TSQ020', 'TSQ040',
       ...
       'TSQ054', 'TSQ062', 'TSQ087', 'TSQ065', 'TSQ037', 'TSQ068', 'TSQ112',
       'TSQ119', 'TSQ016', 'TSQ167'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ057', 'TSQ152', 'TSQ091', 'TSQ108', 'TSQ117', 'TSQ159', 'TSQ122',
       'TSQ092', 'TSQ107', 'TSQ153',
       ...
       'TSQ165', 'TSQ125', 'TSQ040', 'TSQ111', 'TSQ154', 'TSQ171', 'TSQ054',
       'TSQ028', 'TSQ120', 'TSQ163'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ007', 'TSQ065', 'TSQ144', 'TSQ103', 'TSQ093', 'TSQ040', 'TSQ114',
       'TSQ088', 'TSQ072', 'TSQ152',
       ...
       'TSQ023', 'TSQ171', 'TSQ109', 'TSQ156', 'TSQ104', 'TSQ124', 'TSQ146',
       'TSQ079', 'TSQ143', 'TSQ107'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ051', 'TSQ057', 'TSQ130', 'TSQ123', 'TSQ035', 'TSQ114', 'TSQ089',
       'TSQ170', 'TSQ040', 'TSQ126',
       ...
       'TSQ146', 'TSQ104', 'TSQ049', 'TSQ023', 'TSQ093', 'TSQ101', 'TSQ055',
       'TSQ026', 'TSQ144', 'TSQ157'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ062', 'TSQ079', 'TSQ074', 'TSQ109', 'TSQ144', 'TSQ001', 'TSQ013',
       'TSQ166', 'TSQ140', 'TSQ029',
       ...
       'TSQ145', 'TSQ092', 'TSQ147', 'TSQ072', 'TSQ126', 'TSQ041', 'TSQ005',
       'TSQ028', 'TSQ044', 'TSQ156'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ092', 'TSQ118', 'TSQ135', 'TSQ162', 'TSQ089', 'TSQ144',
       'TSQ068', 'TSQ047', 'TSQ007',
       ...
       'TSQ090', 'TSQ062', 'TSQ046', 'TSQ035', 'TSQ106', 'TSQ039', 'TSQ174',
       'TSQ045', 'TSQ067', 'TSQ026'],
      dtype='object', name='Sample_ID', length=109)]