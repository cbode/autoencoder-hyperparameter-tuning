
Readme with Additional Information
==================================


This file contains additional Information for 20220131_nb_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 8.33%	Rep2: 29.17%	Rep3: 41.67%	Rep4: 20.83%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 16.67%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 37.50%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 16.67%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 33.33%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 8.33%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 16.67%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 8.33%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 12.50%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 29.17%	Rep3: 16.67%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ151', 'TSQ092', 'TSQ089', 'TSQ144', 'TSQ176', 'TSQ075', 'TSQ162',
       'TSQ082', 'TSQ029', 'TSQ095', 'TSQ040', 'TSQ106', 'TSQ031', 'TSQ112',
       'TSQ100', 'TSQ035', 'TSQ053', 'TSQ124', 'TSQ103', 'TSQ136', 'TSQ028',
       'TSQ167', 'TSQ074', 'TSQ146'],
      dtype='object', name='Sample_ID'), Index(['TSQ119', 'TSQ129', 'TSQ165', 'TSQ115', 'TSQ127', 'TSQ046', 'TSQ030',
       'TSQ095', 'TSQ040', 'TSQ089', 'TSQ055', 'TSQ016', 'TSQ059', 'TSQ135',
       'TSQ106', 'TSQ026', 'TSQ065', 'TSQ117', 'TSQ028', 'TSQ125', 'TSQ154',
       'TSQ052', 'TSQ104', 'TSQ175'],
      dtype='object', name='Sample_ID'), Index(['TSQ136', 'TSQ154', 'TSQ166', 'TSQ039', 'TSQ044', 'TSQ024', 'TSQ111',
       'TSQ068', 'TSQ021', 'TSQ075', 'TSQ003', 'TSQ070', 'TSQ143', 'TSQ151',
       'TSQ092', 'TSQ093', 'TSQ060', 'TSQ158', 'TSQ175', 'TSQ165', 'TSQ047',
       'TSQ086', 'TSQ139', 'TSQ101'],
      dtype='object', name='Sample_ID'), Index(['TSQ040', 'TSQ063', 'TSQ091', 'TSQ126', 'TSQ134', 'TSQ093', 'TSQ026',
       'TSQ003', 'TSQ045', 'TSQ123', 'TSQ159', 'TSQ034', 'TSQ127', 'TSQ118',
       'TSQ073', 'TSQ052', 'TSQ066', 'TSQ147', 'TSQ162', 'TSQ025', 'TSQ074',
       'TSQ133', 'TSQ117', 'TSQ035'],
      dtype='object', name='Sample_ID'), Index(['TSQ157', 'TSQ044', 'TSQ028', 'TSQ101', 'TSQ109', 'TSQ059', 'TSQ001',
       'TSQ127', 'TSQ079', 'TSQ126', 'TSQ156', 'TSQ045', 'TSQ010', 'TSQ029',
       'TSQ088', 'TSQ122', 'TSQ080', 'TSQ086', 'TSQ103', 'TSQ002', 'TSQ066',
       'TSQ007', 'TSQ027', 'TSQ003'],
      dtype='object', name='Sample_ID'), Index(['TSQ113', 'TSQ170', 'TSQ089', 'TSQ057', 'TSQ082', 'TSQ044', 'TSQ037',
       'TSQ006', 'TSQ068', 'TSQ034', 'TSQ088', 'TSQ114', 'TSQ055', 'TSQ090',
       'TSQ175', 'TSQ021', 'TSQ047', 'TSQ095', 'TSQ045', 'TSQ016', 'TSQ161',
       'TSQ153', 'TSQ002', 'TSQ030'],
      dtype='object', name='Sample_ID'), Index(['TSQ053', 'TSQ176', 'TSQ072', 'TSQ047', 'TSQ174', 'TSQ093', 'TSQ104',
       'TSQ044', 'TSQ147', 'TSQ077', 'TSQ027', 'TSQ122', 'TSQ167', 'TSQ112',
       'TSQ062', 'TSQ002', 'TSQ052', 'TSQ090', 'TSQ079', 'TSQ034', 'TSQ041',
       'TSQ168', 'TSQ157', 'TSQ045'],
      dtype='object', name='Sample_ID'), Index(['TSQ128', 'TSQ016', 'TSQ062', 'TSQ137', 'TSQ030', 'TSQ034', 'TSQ065',
       'TSQ075', 'TSQ140', 'TSQ099', 'TSQ093', 'TSQ157', 'TSQ079', 'TSQ069',
       'TSQ060', 'TSQ146', 'TSQ053', 'TSQ159', 'TSQ104', 'TSQ092', 'TSQ006',
       'TSQ019', 'TSQ070', 'TSQ100'],
      dtype='object', name='Sample_ID'), Index(['TSQ148', 'TSQ143', 'TSQ019', 'TSQ027', 'TSQ065', 'TSQ005', 'TSQ032',
       'TSQ057', 'TSQ095', 'TSQ175', 'TSQ163', 'TSQ046', 'TSQ020', 'TSQ054',
       'TSQ104', 'TSQ023', 'TSQ038', 'TSQ076', 'TSQ156', 'TSQ176', 'TSQ079',
       'TSQ167', 'TSQ072', 'TSQ062'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ059', 'TSQ013', 'TSQ134', 'TSQ093', 'TSQ135', 'TSQ088', 'TSQ104',
       'TSQ126', 'TSQ060', 'TSQ147',
       ...
       'TSQ171', 'TSQ127', 'TSQ077', 'TSQ115', 'TSQ120', 'TSQ044', 'TSQ128',
       'TSQ062', 'TSQ170', 'TSQ002'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ140', 'TSQ114', 'TSQ090', 'TSQ009', 'TSQ161', 'TSQ054', 'TSQ063',
       'TSQ087', 'TSQ144', 'TSQ082',
       ...
       'TSQ041', 'TSQ092', 'TSQ157', 'TSQ070', 'TSQ171', 'TSQ078', 'TSQ002',
       'TSQ120', 'TSQ170', 'TSQ077'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ007', 'TSQ049', 'TSQ038', 'TSQ117', 'TSQ157', 'TSQ108', 'TSQ163',
       'TSQ079', 'TSQ121', 'TSQ025',
       ...
       'TSQ113', 'TSQ073', 'TSQ053', 'TSQ119', 'TSQ077', 'TSQ013', 'TSQ153',
       'TSQ030', 'TSQ001', 'TSQ002'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ005', 'TSQ161', 'TSQ152', 'TSQ079', 'TSQ078', 'TSQ013', 'TSQ107',
       'TSQ076', 'TSQ082', 'TSQ154',
       ...
       'TSQ156', 'TSQ016', 'TSQ112', 'TSQ169', 'TSQ080', 'TSQ032', 'TSQ086',
       'TSQ059', 'TSQ087', 'TSQ001'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ046', 'TSQ166', 'TSQ099', 'TSQ055', 'TSQ143', 'TSQ170', 'TSQ087',
       'TSQ005', 'TSQ139', 'TSQ070',
       ...
       'TSQ031', 'TSQ136', 'TSQ175', 'TSQ148', 'TSQ114', 'TSQ091', 'TSQ025',
       'TSQ113', 'TSQ153', 'TSQ060'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ109', 'TSQ165', 'TSQ053', 'TSQ133', 'TSQ001', 'TSQ078', 'TSQ009',
       'TSQ038', 'TSQ168', 'TSQ063',
       ...
       'TSQ026', 'TSQ108', 'TSQ125', 'TSQ003', 'TSQ060', 'TSQ119', 'TSQ087',
       'TSQ039', 'TSQ162', 'TSQ152'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ159', 'TSQ039', 'TSQ037', 'TSQ162', 'TSQ065', 'TSQ070', 'TSQ128',
       'TSQ030', 'TSQ166', 'TSQ051',
       ...
       'TSQ123', 'TSQ169', 'TSQ171', 'TSQ025', 'TSQ059', 'TSQ137', 'TSQ118',
       'TSQ126', 'TSQ113', 'TSQ136'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ088', 'TSQ031', 'TSQ163', 'TSQ176', 'TSQ106', 'TSQ143', 'TSQ129',
       'TSQ028', 'TSQ166', 'TSQ039',
       ...
       'TSQ072', 'TSQ007', 'TSQ073', 'TSQ027', 'TSQ109', 'TSQ170', 'TSQ130',
       'TSQ124', 'TSQ121', 'TSQ101'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ130', 'TSQ144', 'TSQ063', 'TSQ122', 'TSQ099', 'TSQ021', 'TSQ024',
       'TSQ077', 'TSQ078', 'TSQ059',
       ...
       'TSQ075', 'TSQ126', 'TSQ145', 'TSQ133', 'TSQ030', 'TSQ073', 'TSQ039',
       'TSQ049', 'TSQ128', 'TSQ026'],
      dtype='object', name='Sample_ID', length=109)]