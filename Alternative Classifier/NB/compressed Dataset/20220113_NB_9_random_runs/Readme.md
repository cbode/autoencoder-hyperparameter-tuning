
Readme with Additional Information
==================================


This file contains additional Information for 20220113_NB_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 16.67%	Rep4: 25.00%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 20.83%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 8.33%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 16.67%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 41.67%	Rep3: 16.67%	Rep4: 12.50%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 29.17%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 41.67%	Rep2: 12.50%	Rep3: 33.33%	Rep4: 12.50%

# Random TestSet IDs: 


[Index(['TSQ082', 'TSQ113', 'TSQ158', 'TSQ070', 'TSQ001', 'TSQ086', 'TSQ121',
       'TSQ175', 'TSQ109', 'TSQ044', 'TSQ159', 'TSQ005', 'TSQ162', 'TSQ030',
       'TSQ100', 'TSQ118', 'TSQ059', 'TSQ039', 'TSQ003', 'TSQ045', 'TSQ074',
       'TSQ009', 'TSQ154', 'TSQ040'],
      dtype='object', name='Sample_ID'), Index(['TSQ139', 'TSQ092', 'TSQ001', 'TSQ128', 'TSQ165', 'TSQ035', 'TSQ144',
       'TSQ029', 'TSQ052', 'TSQ074', 'TSQ031', 'TSQ006', 'TSQ005', 'TSQ123',
       'TSQ093', 'TSQ170', 'TSQ143', 'TSQ148', 'TSQ133', 'TSQ025', 'TSQ120',
       'TSQ082', 'TSQ057', 'TSQ027'],
      dtype='object', name='Sample_ID'), Index(['TSQ069', 'TSQ144', 'TSQ037', 'TSQ146', 'TSQ041', 'TSQ122', 'TSQ154',
       'TSQ034', 'TSQ107', 'TSQ003', 'TSQ073', 'TSQ019', 'TSQ133', 'TSQ045',
       'TSQ059', 'TSQ174', 'TSQ057', 'TSQ168', 'TSQ044', 'TSQ167', 'TSQ136',
       'TSQ023', 'TSQ126', 'TSQ128'],
      dtype='object', name='Sample_ID'), Index(['TSQ074', 'TSQ087', 'TSQ027', 'TSQ026', 'TSQ119', 'TSQ068', 'TSQ075',
       'TSQ111', 'TSQ039', 'TSQ045', 'TSQ101', 'TSQ100', 'TSQ024', 'TSQ168',
       'TSQ148', 'TSQ002', 'TSQ010', 'TSQ020', 'TSQ114', 'TSQ157', 'TSQ001',
       'TSQ091', 'TSQ144', 'TSQ151'],
      dtype='object', name='Sample_ID'), Index(['TSQ171', 'TSQ159', 'TSQ034', 'TSQ100', 'TSQ055', 'TSQ126', 'TSQ028',
       'TSQ136', 'TSQ088', 'TSQ101', 'TSQ078', 'TSQ057', 'TSQ086', 'TSQ120',
       'TSQ043', 'TSQ119', 'TSQ161', 'TSQ112', 'TSQ065', 'TSQ024', 'TSQ163',
       'TSQ080', 'TSQ039', 'TSQ002'],
      dtype='object', name='Sample_ID'), Index(['TSQ062', 'TSQ005', 'TSQ140', 'TSQ028', 'TSQ021', 'TSQ030', 'TSQ038',
       'TSQ122', 'TSQ146', 'TSQ013', 'TSQ065', 'TSQ060', 'TSQ052', 'TSQ031',
       'TSQ075', 'TSQ080', 'TSQ066', 'TSQ156', 'TSQ046', 'TSQ079', 'TSQ152',
       'TSQ135', 'TSQ151', 'TSQ053'],
      dtype='object', name='Sample_ID'), Index(['TSQ060', 'TSQ028', 'TSQ059', 'TSQ054', 'TSQ130', 'TSQ073', 'TSQ147',
       'TSQ067', 'TSQ089', 'TSQ111', 'TSQ165', 'TSQ093', 'TSQ159', 'TSQ051',
       'TSQ158', 'TSQ176', 'TSQ068', 'TSQ049', 'TSQ119', 'TSQ040', 'TSQ053',
       'TSQ154', 'TSQ034', 'TSQ134'],
      dtype='object', name='Sample_ID'), Index(['TSQ095', 'TSQ080', 'TSQ101', 'TSQ157', 'TSQ031', 'TSQ139', 'TSQ076',
       'TSQ169', 'TSQ151', 'TSQ003', 'TSQ010', 'TSQ043', 'TSQ088', 'TSQ122',
       'TSQ040', 'TSQ021', 'TSQ124', 'TSQ079', 'TSQ129', 'TSQ035', 'TSQ119',
       'TSQ013', 'TSQ099', 'TSQ121'],
      dtype='object', name='Sample_ID'), Index(['TSQ111', 'TSQ128', 'TSQ157', 'TSQ067', 'TSQ072', 'TSQ076', 'TSQ035',
       'TSQ049', 'TSQ114', 'TSQ171', 'TSQ069', 'TSQ021', 'TSQ136', 'TSQ059',
       'TSQ003', 'TSQ112', 'TSQ038', 'TSQ078', 'TSQ090', 'TSQ154', 'TSQ074',
       'TSQ176', 'TSQ070', 'TSQ125'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ166', 'TSQ106', 'TSQ076', 'TSQ144', 'TSQ051', 'TSQ035', 'TSQ112',
       'TSQ130', 'TSQ108', 'TSQ125',
       ...
       'TSQ002', 'TSQ140', 'TSQ075', 'TSQ007', 'TSQ163', 'TSQ115', 'TSQ124',
       'TSQ034', 'TSQ090', 'TSQ165'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ073', 'TSQ009', 'TSQ013', 'TSQ046', 'TSQ095', 'TSQ130', 'TSQ112',
       'TSQ068', 'TSQ126', 'TSQ146',
       ...
       'TSQ129', 'TSQ078', 'TSQ062', 'TSQ065', 'TSQ088', 'TSQ100', 'TSQ054',
       'TSQ020', 'TSQ159', 'TSQ041'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ135', 'TSQ113', 'TSQ175', 'TSQ170', 'TSQ040', 'TSQ166', 'TSQ043',
       'TSQ002', 'TSQ089', 'TSQ026',
       ...
       'TSQ070', 'TSQ060', 'TSQ047', 'TSQ029', 'TSQ147', 'TSQ075', 'TSQ049',
       'TSQ027', 'TSQ123', 'TSQ140'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ121', 'TSQ089', 'TSQ174', 'TSQ072', 'TSQ107', 'TSQ165', 'TSQ066',
       'TSQ147', 'TSQ120', 'TSQ060',
       ...
       'TSQ016', 'TSQ108', 'TSQ055', 'TSQ127', 'TSQ025', 'TSQ104', 'TSQ152',
       'TSQ133', 'TSQ092', 'TSQ049'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ174', 'TSQ107', 'TSQ148', 'TSQ066', 'TSQ162', 'TSQ114', 'TSQ090',
       'TSQ169', 'TSQ156', 'TSQ135',
       ...
       'TSQ104', 'TSQ077', 'TSQ027', 'TSQ165', 'TSQ146', 'TSQ059', 'TSQ153',
       'TSQ176', 'TSQ016', 'TSQ032'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ125', 'TSQ162', 'TSQ002', 'TSQ104', 'TSQ120', 'TSQ055', 'TSQ026',
       'TSQ039', 'TSQ167', 'TSQ154',
       ...
       'TSQ074', 'TSQ111', 'TSQ078', 'TSQ032', 'TSQ099', 'TSQ165', 'TSQ027',
       'TSQ016', 'TSQ147', 'TSQ051'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ031', 'TSQ043', 'TSQ029', 'TSQ104', 'TSQ168', 'TSQ057',
       'TSQ170', 'TSQ046', 'TSQ005',
       ...
       'TSQ156', 'TSQ120', 'TSQ086', 'TSQ157', 'TSQ080', 'TSQ003', 'TSQ114',
       'TSQ078', 'TSQ082', 'TSQ072'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ066', 'TSQ016', 'TSQ128', 'TSQ078', 'TSQ034', 'TSQ057', 'TSQ049',
       'TSQ054', 'TSQ087', 'TSQ075',
       ...
       'TSQ148', 'TSQ166', 'TSQ136', 'TSQ108', 'TSQ020', 'TSQ147', 'TSQ068',
       'TSQ171', 'TSQ041', 'TSQ062'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ054', 'TSQ010', 'TSQ040', 'TSQ156', 'TSQ148', 'TSQ120', 'TSQ126',
       'TSQ117', 'TSQ174', 'TSQ121',
       ...
       'TSQ093', 'TSQ082', 'TSQ108', 'TSQ170', 'TSQ077', 'TSQ165', 'TSQ175',
       'TSQ152', 'TSQ026', 'TSQ169'],
      dtype='object', name='Sample_ID', length=109)]