
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_9_random_runs.
# General Information


SVM Classifier 
C = 1 
kernel = 'linear'
other settings are set default 


The Dataset was split randomly into a test and a training set 

# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 16.67%	Rep2: 29.17%	Rep3: 16.67%	Rep4: 37.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 37.50%	Rep2: 12.50%	Rep3: 29.17%	Rep4: 20.83%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 25.00%	Rep4: 37.50%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 8.33%	Rep3: 54.17%	Rep4: 20.83%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 41.67%	Rep3: 16.67%	Rep4: 20.83%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	24 		 that equals to 22.02% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 29.17%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 41.67%	Rep2: 20.83%	Rep3: 16.67%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ171', 'TSQ126', 'TSQ127', 'TSQ035', 'TSQ140', 'TSQ100', 'TSQ076',
       'TSQ143', 'TSQ044', 'TSQ026', 'TSQ128', 'TSQ153', 'TSQ119', 'TSQ109',
       'TSQ175', 'TSQ052', 'TSQ069', 'TSQ045', 'TSQ029', 'TSQ088', 'TSQ062',
       'TSQ111', 'TSQ032', 'TSQ118'],
      dtype='object', name='Sample_ID'), Index(['TSQ057', 'TSQ089', 'TSQ053', 'TSQ106', 'TSQ025', 'TSQ068', 'TSQ148',
       'TSQ070', 'TSQ145', 'TSQ092', 'TSQ128', 'TSQ119', 'TSQ151', 'TSQ001',
       'TSQ127', 'TSQ123', 'TSQ144', 'TSQ079', 'TSQ072', 'TSQ034', 'TSQ134',
       'TSQ051', 'TSQ046', 'TSQ032'],
      dtype='object', name='Sample_ID'), Index(['TSQ162', 'TSQ075', 'TSQ120', 'TSQ080', 'TSQ073', 'TSQ074', 'TSQ093',
       'TSQ146', 'TSQ118', 'TSQ082', 'TSQ005', 'TSQ060', 'TSQ154', 'TSQ137',
       'TSQ112', 'TSQ001', 'TSQ139', 'TSQ077', 'TSQ029', 'TSQ021', 'TSQ041',
       'TSQ127', 'TSQ166', 'TSQ023'],
      dtype='object', name='Sample_ID'), Index(['TSQ130', 'TSQ104', 'TSQ147', 'TSQ112', 'TSQ029', 'TSQ072', 'TSQ095',
       'TSQ108', 'TSQ118', 'TSQ117', 'TSQ068', 'TSQ157', 'TSQ139', 'TSQ078',
       'TSQ005', 'TSQ162', 'TSQ040', 'TSQ127', 'TSQ156', 'TSQ031', 'TSQ024',
       'TSQ092', 'TSQ076', 'TSQ121'],
      dtype='object', name='Sample_ID'), Index(['TSQ114', 'TSQ024', 'TSQ140', 'TSQ109', 'TSQ125', 'TSQ005', 'TSQ171',
       'TSQ168', 'TSQ089', 'TSQ113', 'TSQ135', 'TSQ134', 'TSQ126', 'TSQ111',
       'TSQ034', 'TSQ099', 'TSQ031', 'TSQ127', 'TSQ124', 'TSQ087', 'TSQ025',
       'TSQ101', 'TSQ106', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ046', 'TSQ134', 'TSQ129', 'TSQ067', 'TSQ101', 'TSQ074', 'TSQ019',
       'TSQ161', 'TSQ037', 'TSQ065', 'TSQ010', 'TSQ119', 'TSQ153', 'TSQ091',
       'TSQ001', 'TSQ069', 'TSQ133', 'TSQ009', 'TSQ021', 'TSQ095', 'TSQ052',
       'TSQ041', 'TSQ040', 'TSQ073'],
      dtype='object', name='Sample_ID'), Index(['TSQ019', 'TSQ080', 'TSQ037', 'TSQ156', 'TSQ163', 'TSQ112', 'TSQ168',
       'TSQ073', 'TSQ162', 'TSQ070', 'TSQ122', 'TSQ158', 'TSQ103', 'TSQ130',
       'TSQ136', 'TSQ144', 'TSQ053', 'TSQ134', 'TSQ171', 'TSQ109', 'TSQ021',
       'TSQ099', 'TSQ046', 'TSQ066'],
      dtype='object', name='Sample_ID'), Index(['TSQ125', 'TSQ154', 'TSQ135', 'TSQ055', 'TSQ119', 'TSQ023', 'TSQ109',
       'TSQ146', 'TSQ143', 'TSQ054', 'TSQ124', 'TSQ165', 'TSQ046', 'TSQ148',
       'TSQ005', 'TSQ019', 'TSQ041', 'TSQ088', 'TSQ129', 'TSQ031', 'TSQ013',
       'TSQ134', 'TSQ117', 'TSQ078'],
      dtype='object', name='Sample_ID'), Index(['TSQ151', 'TSQ040', 'TSQ118', 'TSQ005', 'TSQ146', 'TSQ156', 'TSQ082',
       'TSQ070', 'TSQ126', 'TSQ078', 'TSQ001', 'TSQ054', 'TSQ021', 'TSQ037',
       'TSQ024', 'TSQ106', 'TSQ020', 'TSQ075', 'TSQ079', 'TSQ120', 'TSQ002',
       'TSQ158', 'TSQ152', 'TSQ067'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ046', 'TSQ169', 'TSQ043', 'TSQ054', 'TSQ051', 'TSQ158', 'TSQ151',
       'TSQ021', 'TSQ166', 'TSQ134',
       ...
       'TSQ059', 'TSQ006', 'TSQ087', 'TSQ034', 'TSQ170', 'TSQ103', 'TSQ020',
       'TSQ095', 'TSQ163', 'TSQ082'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ075', 'TSQ118', 'TSQ146', 'TSQ107', 'TSQ125', 'TSQ112', 'TSQ140',
       'TSQ153', 'TSQ069', 'TSQ117',
       ...
       'TSQ028', 'TSQ101', 'TSQ136', 'TSQ076', 'TSQ037', 'TSQ162', 'TSQ040',
       'TSQ065', 'TSQ078', 'TSQ165'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ169', 'TSQ065', 'TSQ114', 'TSQ174', 'TSQ069', 'TSQ035', 'TSQ158',
       'TSQ086', 'TSQ123', 'TSQ070',
       ...
       'TSQ039', 'TSQ122', 'TSQ161', 'TSQ125', 'TSQ057', 'TSQ087', 'TSQ026',
       'TSQ049', 'TSQ016', 'TSQ115'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ163', 'TSQ047', 'TSQ013', 'TSQ165', 'TSQ077', 'TSQ167', 'TSQ101',
       'TSQ046', 'TSQ002', 'TSQ090',
       ...
       'TSQ054', 'TSQ151', 'TSQ128', 'TSQ044', 'TSQ136', 'TSQ082', 'TSQ070',
       'TSQ134', 'TSQ079', 'TSQ025'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ107', 'TSQ063', 'TSQ108', 'TSQ167', 'TSQ020', 'TSQ077', 'TSQ027',
       'TSQ041', 'TSQ028', 'TSQ088',
       ...
       'TSQ154', 'TSQ046', 'TSQ091', 'TSQ026', 'TSQ074', 'TSQ049', 'TSQ066',
       'TSQ070', 'TSQ007', 'TSQ151'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ038', 'TSQ053', 'TSQ057', 'TSQ121', 'TSQ106', 'TSQ108', 'TSQ118',
       'TSQ020', 'TSQ140', 'TSQ117',
       ...
       'TSQ144', 'TSQ027', 'TSQ163', 'TSQ171', 'TSQ087', 'TSQ154', 'TSQ016',
       'TSQ120', 'TSQ060', 'TSQ128'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ092', 'TSQ111', 'TSQ091', 'TSQ030', 'TSQ128', 'TSQ005',
       'TSQ135', 'TSQ104', 'TSQ123',
       ...
       'TSQ147', 'TSQ152', 'TSQ063', 'TSQ024', 'TSQ031', 'TSQ148', 'TSQ047',
       'TSQ006', 'TSQ140', 'TSQ035'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ089', 'TSQ027', 'TSQ030', 'TSQ016', 'TSQ007', 'TSQ171', 'TSQ115',
       'TSQ163', 'TSQ002', 'TSQ118',
       ...
       'TSQ152', 'TSQ065', 'TSQ162', 'TSQ034', 'TSQ024', 'TSQ107', 'TSQ100',
       'TSQ170', 'TSQ095', 'TSQ168'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ121', 'TSQ145', 'TSQ043', 'TSQ092', 'TSQ129', 'TSQ073', 'TSQ135',
       'TSQ122', 'TSQ170', 'TSQ104',
       ...
       'TSQ114', 'TSQ072', 'TSQ069', 'TSQ023', 'TSQ124', 'TSQ041', 'TSQ133',
       'TSQ044', 'TSQ066', 'TSQ154'],
      dtype='object', name='Sample_ID', length=109)]