# Readme file with additional information
This folder contains the data and the plots for the Estimation of C of a SVM Classifier using an 'linear' kernel. 
The dataset was split in a stratified Sampled Way using the Datasets file found in: Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

Cs tested: [0.01,0.1,1.0,2.0,4.0,8.0,10,20,40,80,100,160]

Values found (mean): 
0.01      0.416667
0.10      0.402778
1.00      0.388889
2.00      0.388889
4.00      0.388889
8.00      0.388889
10.00     0.388889
20.00     0.388889
40.00     0.388889
80.00     0.388889
100.00    0.388889
160.00    0.388889