
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_100_random_runs_v5.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 16.67%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 25.00%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 37.50%	Rep3: 20.83%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 29.17%	Rep4: 33.33%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 20.83%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 4.17%	Rep2: 25.00%	Rep3: 45.83%	Rep4: 25.00%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 37.50%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 20.83%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 41.67%	Rep2: 20.83%	Rep3: 16.67%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ114', 'TSQ095', 'TSQ052', 'TSQ047', 'TSQ175', 'TSQ119', 'TSQ044',
       'TSQ041', 'TSQ089', 'TSQ003', 'TSQ065', 'TSQ021', 'TSQ054', 'TSQ055',
       'TSQ153', 'TSQ169', 'TSQ092', 'TSQ104', 'TSQ093', 'TSQ027', 'TSQ136',
       'TSQ057', 'TSQ158', 'TSQ078'],
      dtype='object', name='Sample_ID'), Index(['TSQ053', 'TSQ162', 'TSQ082', 'TSQ147', 'TSQ143', 'TSQ002', 'TSQ175',
       'TSQ086', 'TSQ020', 'TSQ174', 'TSQ106', 'TSQ054', 'TSQ167', 'TSQ118',
       'TSQ059', 'TSQ044', 'TSQ125', 'TSQ069', 'TSQ114', 'TSQ034', 'TSQ065',
       'TSQ049', 'TSQ047', 'TSQ089'],
      dtype='object', name='Sample_ID'), Index(['TSQ158', 'TSQ032', 'TSQ161', 'TSQ162', 'TSQ144', 'TSQ153', 'TSQ049',
       'TSQ025', 'TSQ053', 'TSQ066', 'TSQ007', 'TSQ040', 'TSQ148', 'TSQ020',
       'TSQ134', 'TSQ006', 'TSQ137', 'TSQ073', 'TSQ047', 'TSQ037', 'TSQ075',
       'TSQ038', 'TSQ108', 'TSQ016'],
      dtype='object', name='Sample_ID'), Index(['TSQ109', 'TSQ066', 'TSQ095', 'TSQ123', 'TSQ057', 'TSQ165', 'TSQ137',
       'TSQ157', 'TSQ134', 'TSQ119', 'TSQ009', 'TSQ151', 'TSQ060', 'TSQ103',
       'TSQ047', 'TSQ114', 'TSQ032', 'TSQ107', 'TSQ068', 'TSQ024', 'TSQ031',
       'TSQ120', 'TSQ161', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ126', 'TSQ039', 'TSQ001', 'TSQ113', 'TSQ055', 'TSQ135', 'TSQ128',
       'TSQ111', 'TSQ021', 'TSQ079', 'TSQ072', 'TSQ163', 'TSQ082', 'TSQ005',
       'TSQ106', 'TSQ170', 'TSQ114', 'TSQ026', 'TSQ130', 'TSQ080', 'TSQ052',
       'TSQ127', 'TSQ040', 'TSQ046'],
      dtype='object', name='Sample_ID'), Index(['TSQ144', 'TSQ126', 'TSQ093', 'TSQ091', 'TSQ087', 'TSQ163', 'TSQ123',
       'TSQ135', 'TSQ077', 'TSQ114', 'TSQ130', 'TSQ153', 'TSQ168', 'TSQ019',
       'TSQ075', 'TSQ041', 'TSQ115', 'TSQ161', 'TSQ005', 'TSQ100', 'TSQ020',
       'TSQ095', 'TSQ108', 'TSQ122'],
      dtype='object', name='Sample_ID'), Index(['TSQ029', 'TSQ148', 'TSQ024', 'TSQ121', 'TSQ087', 'TSQ161', 'TSQ086',
       'TSQ007', 'TSQ166', 'TSQ047', 'TSQ124', 'TSQ134', 'TSQ038', 'TSQ170',
       'TSQ078', 'TSQ135', 'TSQ125', 'TSQ140', 'TSQ101', 'TSQ117', 'TSQ092',
       'TSQ175', 'TSQ045', 'TSQ037'],
      dtype='object', name='Sample_ID'), Index(['TSQ176', 'TSQ143', 'TSQ025', 'TSQ135', 'TSQ009', 'TSQ007', 'TSQ034',
       'TSQ137', 'TSQ103', 'TSQ065', 'TSQ063', 'TSQ129', 'TSQ029', 'TSQ026',
       'TSQ168', 'TSQ165', 'TSQ093', 'TSQ046', 'TSQ118', 'TSQ090', 'TSQ066',
       'TSQ126', 'TSQ171', 'TSQ123'],
      dtype='object', name='Sample_ID'), Index(['TSQ111', 'TSQ134', 'TSQ127', 'TSQ069', 'TSQ045', 'TSQ082', 'TSQ044',
       'TSQ035', 'TSQ067', 'TSQ112', 'TSQ025', 'TSQ074', 'TSQ076', 'TSQ088',
       'TSQ068', 'TSQ176', 'TSQ146', 'TSQ119', 'TSQ039', 'TSQ037', 'TSQ038',
       'TSQ049', 'TSQ108', 'TSQ099'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ016', 'TSQ062', 'TSQ133', 'TSQ113', 'TSQ019', 'TSQ165', 'TSQ002',
       'TSQ106', 'TSQ152', 'TSQ123',
       ...
       'TSQ126', 'TSQ090', 'TSQ128', 'TSQ122', 'TSQ035', 'TSQ161', 'TSQ010',
       'TSQ103', 'TSQ176', 'TSQ070'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ156', 'TSQ100', 'TSQ023', 'TSQ115', 'TSQ005', 'TSQ007', 'TSQ024',
       'TSQ113', 'TSQ088', 'TSQ040',
       ...
       'TSQ062', 'TSQ006', 'TSQ035', 'TSQ079', 'TSQ057', 'TSQ140', 'TSQ111',
       'TSQ019', 'TSQ134', 'TSQ159'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ122', 'TSQ115', 'TSQ147', 'TSQ124', 'TSQ111', 'TSQ062', 'TSQ089',
       'TSQ101', 'TSQ151', 'TSQ035',
       ...
       'TSQ078', 'TSQ005', 'TSQ029', 'TSQ121', 'TSQ145', 'TSQ165', 'TSQ030',
       'TSQ167', 'TSQ023', 'TSQ041'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ027', 'TSQ076', 'TSQ147', 'TSQ055', 'TSQ001', 'TSQ021', 'TSQ106',
       'TSQ166', 'TSQ069', 'TSQ171',
       ...
       'TSQ044', 'TSQ174', 'TSQ016', 'TSQ169', 'TSQ075', 'TSQ054', 'TSQ127',
       'TSQ080', 'TSQ125', 'TSQ010'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ143', 'TSQ044', 'TSQ053', 'TSQ117', 'TSQ095', 'TSQ146', 'TSQ134',
       'TSQ068', 'TSQ076', 'TSQ153',
       ...
       'TSQ086', 'TSQ087', 'TSQ158', 'TSQ169', 'TSQ077', 'TSQ129', 'TSQ049',
       'TSQ099', 'TSQ101', 'TSQ032'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ037', 'TSQ001', 'TSQ109', 'TSQ086', 'TSQ103', 'TSQ121', 'TSQ043',
       'TSQ162', 'TSQ025', 'TSQ101',
       ...
       'TSQ156', 'TSQ069', 'TSQ143', 'TSQ040', 'TSQ076', 'TSQ167', 'TSQ171',
       'TSQ021', 'TSQ119', 'TSQ107'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ107', 'TSQ158', 'TSQ040', 'TSQ075', 'TSQ001', 'TSQ009', 'TSQ066',
       'TSQ068', 'TSQ152', 'TSQ114',
       ...
       'TSQ120', 'TSQ108', 'TSQ019', 'TSQ013', 'TSQ159', 'TSQ163', 'TSQ162',
       'TSQ112', 'TSQ154', 'TSQ171'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ067', 'TSQ104', 'TSQ158', 'TSQ166', 'TSQ039', 'TSQ130', 'TSQ112',
       'TSQ162', 'TSQ100', 'TSQ125',
       ...
       'TSQ070', 'TSQ092', 'TSQ169', 'TSQ027', 'TSQ062', 'TSQ140', 'TSQ043',
       'TSQ080', 'TSQ161', 'TSQ152'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ029', 'TSQ117', 'TSQ001', 'TSQ075', 'TSQ100', 'TSQ157', 'TSQ158',
       'TSQ005', 'TSQ137', 'TSQ109',
       ...
       'TSQ120', 'TSQ161', 'TSQ020', 'TSQ063', 'TSQ021', 'TSQ163', 'TSQ002',
       'TSQ093', 'TSQ080', 'TSQ046'],
      dtype='object', name='Sample_ID', length=109)]