
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_100_random_runs_v3.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 8.33%	Rep2: 29.17%	Rep3: 41.67%	Rep4: 20.83%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 8.33%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 4.17%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 25.00%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 20.83%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 16.67%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 25.00%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 41.67%	Rep3: 25.00%	Rep4: 16.67%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 16.67%

# Random TestSet IDs: 


[Index(['TSQ013', 'TSQ099', 'TSQ100', 'TSQ090', 'TSQ007', 'TSQ117', 'TSQ103',
       'TSQ075', 'TSQ086', 'TSQ120', 'TSQ154', 'TSQ136', 'TSQ162', 'TSQ062',
       'TSQ044', 'TSQ037', 'TSQ148', 'TSQ144', 'TSQ092', 'TSQ111', 'TSQ087',
       'TSQ063', 'TSQ125', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ123', 'TSQ062', 'TSQ080', 'TSQ103', 'TSQ139', 'TSQ026', 'TSQ054',
       'TSQ005', 'TSQ038', 'TSQ176', 'TSQ053', 'TSQ045', 'TSQ092', 'TSQ135',
       'TSQ070', 'TSQ020', 'TSQ136', 'TSQ158', 'TSQ027', 'TSQ023', 'TSQ087',
       'TSQ021', 'TSQ028', 'TSQ029'],
      dtype='object', name='Sample_ID'), Index(['TSQ009', 'TSQ137', 'TSQ122', 'TSQ005', 'TSQ072', 'TSQ025', 'TSQ024',
       'TSQ062', 'TSQ054', 'TSQ047', 'TSQ135', 'TSQ111', 'TSQ080', 'TSQ090',
       'TSQ006', 'TSQ086', 'TSQ100', 'TSQ066', 'TSQ107', 'TSQ147', 'TSQ053',
       'TSQ026', 'TSQ001', 'TSQ038'],
      dtype='object', name='Sample_ID'), Index(['TSQ072', 'TSQ167', 'TSQ112', 'TSQ029', 'TSQ087', 'TSQ119', 'TSQ114',
       'TSQ121', 'TSQ062', 'TSQ174', 'TSQ122', 'TSQ101', 'TSQ076', 'TSQ046',
       'TSQ166', 'TSQ146', 'TSQ059', 'TSQ118', 'TSQ152', 'TSQ052', 'TSQ035',
       'TSQ135', 'TSQ108', 'TSQ156'],
      dtype='object', name='Sample_ID'), Index(['TSQ055', 'TSQ026', 'TSQ038', 'TSQ003', 'TSQ030', 'TSQ073', 'TSQ079',
       'TSQ092', 'TSQ091', 'TSQ143', 'TSQ153', 'TSQ103', 'TSQ161', 'TSQ025',
       'TSQ151', 'TSQ087', 'TSQ089', 'TSQ059', 'TSQ114', 'TSQ078', 'TSQ053',
       'TSQ124', 'TSQ046', 'TSQ163'],
      dtype='object', name='Sample_ID'), Index(['TSQ082', 'TSQ007', 'TSQ159', 'TSQ125', 'TSQ091', 'TSQ023', 'TSQ163',
       'TSQ165', 'TSQ113', 'TSQ112', 'TSQ075', 'TSQ077', 'TSQ001', 'TSQ069',
       'TSQ025', 'TSQ073', 'TSQ147', 'TSQ009', 'TSQ124', 'TSQ121', 'TSQ145',
       'TSQ169', 'TSQ176', 'TSQ089'],
      dtype='object', name='Sample_ID'), Index(['TSQ006', 'TSQ047', 'TSQ161', 'TSQ063', 'TSQ135', 'TSQ130', 'TSQ168',
       'TSQ079', 'TSQ103', 'TSQ078', 'TSQ121', 'TSQ082', 'TSQ076', 'TSQ001',
       'TSQ065', 'TSQ129', 'TSQ046', 'TSQ125', 'TSQ154', 'TSQ028', 'TSQ106',
       'TSQ053', 'TSQ148', 'TSQ087'],
      dtype='object', name='Sample_ID'), Index(['TSQ029', 'TSQ043', 'TSQ076', 'TSQ133', 'TSQ104', 'TSQ019', 'TSQ073',
       'TSQ166', 'TSQ032', 'TSQ091', 'TSQ051', 'TSQ162', 'TSQ045', 'TSQ154',
       'TSQ130', 'TSQ087', 'TSQ037', 'TSQ159', 'TSQ167', 'TSQ053', 'TSQ023',
       'TSQ044', 'TSQ005', 'TSQ121'],
      dtype='object', name='Sample_ID'), Index(['TSQ035', 'TSQ003', 'TSQ154', 'TSQ009', 'TSQ054', 'TSQ130', 'TSQ079',
       'TSQ161', 'TSQ001', 'TSQ019', 'TSQ089', 'TSQ140', 'TSQ075', 'TSQ068',
       'TSQ067', 'TSQ049', 'TSQ157', 'TSQ039', 'TSQ111', 'TSQ122', 'TSQ063',
       'TSQ078', 'TSQ112', 'TSQ073'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ032', 'TSQ016', 'TSQ026', 'TSQ157', 'TSQ153', 'TSQ051', 'TSQ023',
       'TSQ020', 'TSQ021', 'TSQ069',
       ...
       'TSQ074', 'TSQ027', 'TSQ130', 'TSQ068', 'TSQ038', 'TSQ041', 'TSQ174',
       'TSQ079', 'TSQ054', 'TSQ112'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ001', 'TSQ148', 'TSQ024', 'TSQ106', 'TSQ133', 'TSQ143', 'TSQ146',
       'TSQ031', 'TSQ037', 'TSQ112',
       ...
       'TSQ089', 'TSQ065', 'TSQ047', 'TSQ163', 'TSQ019', 'TSQ006', 'TSQ154',
       'TSQ107', 'TSQ040', 'TSQ167'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ089', 'TSQ078', 'TSQ034', 'TSQ055', 'TSQ043', 'TSQ118', 'TSQ091',
       'TSQ070', 'TSQ099', 'TSQ165',
       ...
       'TSQ057', 'TSQ103', 'TSQ158', 'TSQ167', 'TSQ124', 'TSQ073', 'TSQ095',
       'TSQ125', 'TSQ171', 'TSQ108'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ031', 'TSQ090', 'TSQ136', 'TSQ009', 'TSQ067', 'TSQ091', 'TSQ093',
       'TSQ147', 'TSQ148', 'TSQ127',
       ...
       'TSQ057', 'TSQ154', 'TSQ106', 'TSQ145', 'TSQ060', 'TSQ113', 'TSQ111',
       'TSQ170', 'TSQ037', 'TSQ005'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ066', 'TSQ159', 'TSQ037', 'TSQ060', 'TSQ063', 'TSQ158', 'TSQ170',
       'TSQ165', 'TSQ006', 'TSQ169',
       ...
       'TSQ109', 'TSQ118', 'TSQ112', 'TSQ167', 'TSQ032', 'TSQ088', 'TSQ013',
       'TSQ080', 'TSQ140', 'TSQ005'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ060', 'TSQ005', 'TSQ080', 'TSQ068', 'TSQ135', 'TSQ062', 'TSQ070',
       'TSQ143', 'TSQ170', 'TSQ119',
       ...
       'TSQ054', 'TSQ066', 'TSQ154', 'TSQ052', 'TSQ003', 'TSQ038', 'TSQ053',
       'TSQ024', 'TSQ016', 'TSQ153'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ151', 'TSQ019', 'TSQ021', 'TSQ171', 'TSQ077', 'TSQ041',
       'TSQ162', 'TSQ100', 'TSQ043',
       ...
       'TSQ080', 'TSQ039', 'TSQ119', 'TSQ159', 'TSQ049', 'TSQ163', 'TSQ115',
       'TSQ099', 'TSQ166', 'TSQ167'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ075', 'TSQ120', 'TSQ072', 'TSQ134', 'TSQ165', 'TSQ057', 'TSQ125',
       'TSQ148', 'TSQ001', 'TSQ099',
       ...
       'TSQ089', 'TSQ047', 'TSQ175', 'TSQ034', 'TSQ152', 'TSQ002', 'TSQ052',
       'TSQ092', 'TSQ163', 'TSQ040'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ047', 'TSQ028', 'TSQ021', 'TSQ167', 'TSQ086', 'TSQ013',
       'TSQ037', 'TSQ045', 'TSQ118',
       ...
       'TSQ103', 'TSQ145', 'TSQ106', 'TSQ125', 'TSQ038', 'TSQ060', 'TSQ108',
       'TSQ114', 'TSQ044', 'TSQ053'],
      dtype='object', name='Sample_ID', length=109)]