
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_100_random_runs_v4.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 29.17%	Rep3: 37.50%	Rep4: 20.83%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 33.33%	Rep3: 16.67%	Rep4: 16.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 12.50%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 25.00%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 16.67%	Rep3: 25.00%	Rep4: 29.17%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 20.83%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 16.67%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 41.67%	Rep3: 20.83%	Rep4: 25.00%

# Random TestSet IDs: 


[Index(['TSQ078', 'TSQ139', 'TSQ007', 'TSQ153', 'TSQ126', 'TSQ120', 'TSQ125',
       'TSQ137', 'TSQ170', 'TSQ047', 'TSQ044', 'TSQ103', 'TSQ028', 'TSQ089',
       'TSQ091', 'TSQ009', 'TSQ108', 'TSQ069', 'TSQ076', 'TSQ121', 'TSQ010',
       'TSQ104', 'TSQ169', 'TSQ073'],
      dtype='object', name='Sample_ID'), Index(['TSQ065', 'TSQ029', 'TSQ028', 'TSQ167', 'TSQ038', 'TSQ032', 'TSQ035',
       'TSQ092', 'TSQ176', 'TSQ113', 'TSQ016', 'TSQ143', 'TSQ107', 'TSQ079',
       'TSQ043', 'TSQ044', 'TSQ072', 'TSQ068', 'TSQ075', 'TSQ030', 'TSQ023',
       'TSQ060', 'TSQ156', 'TSQ095'],
      dtype='object', name='Sample_ID'), Index(['TSQ032', 'TSQ082', 'TSQ137', 'TSQ013', 'TSQ168', 'TSQ029', 'TSQ067',
       'TSQ106', 'TSQ052', 'TSQ108', 'TSQ035', 'TSQ043', 'TSQ148', 'TSQ040',
       'TSQ045', 'TSQ152', 'TSQ113', 'TSQ151', 'TSQ016', 'TSQ166', 'TSQ021',
       'TSQ127', 'TSQ095', 'TSQ057'],
      dtype='object', name='Sample_ID'), Index(['TSQ169', 'TSQ030', 'TSQ109', 'TSQ118', 'TSQ045', 'TSQ143', 'TSQ136',
       'TSQ076', 'TSQ146', 'TSQ166', 'TSQ148', 'TSQ111', 'TSQ093', 'TSQ175',
       'TSQ101', 'TSQ151', 'TSQ154', 'TSQ053', 'TSQ074', 'TSQ115', 'TSQ039',
       'TSQ080', 'TSQ073', 'TSQ079'],
      dtype='object', name='Sample_ID'), Index(['TSQ171', 'TSQ130', 'TSQ079', 'TSQ057', 'TSQ099', 'TSQ159', 'TSQ021',
       'TSQ108', 'TSQ106', 'TSQ136', 'TSQ151', 'TSQ067', 'TSQ074', 'TSQ088',
       'TSQ143', 'TSQ082', 'TSQ154', 'TSQ016', 'TSQ026', 'TSQ167', 'TSQ125',
       'TSQ126', 'TSQ049', 'TSQ076'],
      dtype='object', name='Sample_ID'), Index(['TSQ029', 'TSQ121', 'TSQ026', 'TSQ052', 'TSQ028', 'TSQ148', 'TSQ115',
       'TSQ107', 'TSQ023', 'TSQ154', 'TSQ093', 'TSQ111', 'TSQ143', 'TSQ087',
       'TSQ027', 'TSQ040', 'TSQ157', 'TSQ144', 'TSQ059', 'TSQ135', 'TSQ007',
       'TSQ117', 'TSQ045', 'TSQ037'],
      dtype='object', name='Sample_ID'), Index(['TSQ001', 'TSQ074', 'TSQ129', 'TSQ055', 'TSQ062', 'TSQ124', 'TSQ158',
       'TSQ089', 'TSQ165', 'TSQ035', 'TSQ091', 'TSQ039', 'TSQ052', 'TSQ082',
       'TSQ133', 'TSQ013', 'TSQ099', 'TSQ122', 'TSQ171', 'TSQ030', 'TSQ095',
       'TSQ025', 'TSQ163', 'TSQ031'],
      dtype='object', name='Sample_ID'), Index(['TSQ052', 'TSQ122', 'TSQ073', 'TSQ099', 'TSQ047', 'TSQ137', 'TSQ005',
       'TSQ108', 'TSQ002', 'TSQ145', 'TSQ106', 'TSQ063', 'TSQ103', 'TSQ007',
       'TSQ162', 'TSQ013', 'TSQ053', 'TSQ006', 'TSQ003', 'TSQ016', 'TSQ046',
       'TSQ148', 'TSQ134', 'TSQ043'],
      dtype='object', name='Sample_ID'), Index(['TSQ133', 'TSQ157', 'TSQ078', 'TSQ041', 'TSQ062', 'TSQ118', 'TSQ039',
       'TSQ130', 'TSQ060', 'TSQ152', 'TSQ073', 'TSQ100', 'TSQ063', 'TSQ010',
       'TSQ095', 'TSQ174', 'TSQ140', 'TSQ007', 'TSQ044', 'TSQ091', 'TSQ093',
       'TSQ075', 'TSQ072', 'TSQ009'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ080', 'TSQ100', 'TSQ136', 'TSQ006', 'TSQ167', 'TSQ002', 'TSQ013',
       'TSQ101', 'TSQ062', 'TSQ128',
       ...
       'TSQ115', 'TSQ175', 'TSQ113', 'TSQ145', 'TSQ060', 'TSQ031', 'TSQ001',
       'TSQ176', 'TSQ117', 'TSQ051'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ099', 'TSQ057', 'TSQ130', 'TSQ170', 'TSQ078', 'TSQ069', 'TSQ154',
       'TSQ151', 'TSQ041', 'TSQ045',
       ...
       'TSQ062', 'TSQ163', 'TSQ024', 'TSQ124', 'TSQ125', 'TSQ145', 'TSQ136',
       'TSQ073', 'TSQ174', 'TSQ001'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ156', 'TSQ170', 'TSQ010', 'TSQ002', 'TSQ161', 'TSQ158', 'TSQ073',
       'TSQ065', 'TSQ074', 'TSQ115',
       ...
       'TSQ077', 'TSQ167', 'TSQ125', 'TSQ068', 'TSQ147', 'TSQ038', 'TSQ112',
       'TSQ122', 'TSQ039', 'TSQ175'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ135', 'TSQ092', 'TSQ062', 'TSQ024', 'TSQ007', 'TSQ049', 'TSQ129',
       'TSQ025', 'TSQ170', 'TSQ077',
       ...
       'TSQ157', 'TSQ038', 'TSQ066', 'TSQ040', 'TSQ046', 'TSQ031', 'TSQ009',
       'TSQ119', 'TSQ114', 'TSQ103'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ117', 'TSQ128', 'TSQ124', 'TSQ153', 'TSQ127', 'TSQ175', 'TSQ007',
       'TSQ161', 'TSQ101', 'TSQ037',
       ...
       'TSQ095', 'TSQ165', 'TSQ137', 'TSQ113', 'TSQ006', 'TSQ072', 'TSQ170',
       'TSQ114', 'TSQ146', 'TSQ069'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ152', 'TSQ137', 'TSQ068', 'TSQ163', 'TSQ118', 'TSQ010', 'TSQ147',
       'TSQ041', 'TSQ082', 'TSQ089',
       ...
       'TSQ069', 'TSQ006', 'TSQ140', 'TSQ077', 'TSQ114', 'TSQ095', 'TSQ079',
       'TSQ054', 'TSQ171', 'TSQ092'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ065', 'TSQ006', 'TSQ067', 'TSQ005', 'TSQ148', 'TSQ070', 'TSQ151',
       'TSQ034', 'TSQ088', 'TSQ086',
       ...
       'TSQ072', 'TSQ159', 'TSQ051', 'TSQ077', 'TSQ144', 'TSQ156', 'TSQ104',
       'TSQ137', 'TSQ176', 'TSQ002'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ135', 'TSQ120', 'TSQ077', 'TSQ023', 'TSQ104', 'TSQ038', 'TSQ072',
       'TSQ067', 'TSQ082', 'TSQ090',
       ...
       'TSQ010', 'TSQ029', 'TSQ020', 'TSQ112', 'TSQ044', 'TSQ086', 'TSQ037',
       'TSQ117', 'TSQ030', 'TSQ133'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ080', 'TSQ023', 'TSQ019', 'TSQ070', 'TSQ151', 'TSQ176', 'TSQ046',
       'TSQ137', 'TSQ026', 'TSQ089',
       ...
       'TSQ127', 'TSQ051', 'TSQ088', 'TSQ068', 'TSQ113', 'TSQ027', 'TSQ126',
       'TSQ054', 'TSQ162', 'TSQ108'],
      dtype='object', name='Sample_ID', length=109)]