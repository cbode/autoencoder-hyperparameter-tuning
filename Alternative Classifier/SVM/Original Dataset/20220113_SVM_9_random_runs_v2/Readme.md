
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_100_random_runs_v2.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 12.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 12.50%	Rep3: 41.67%	Rep4: 20.83%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 4.17%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 33.33%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 33.33%	Rep3: 33.33%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 16.67%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 12.50%	Rep2: 37.50%	Rep3: 37.50%	Rep4: 12.50%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 25.00%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 16.67%

# Random TestSet IDs: 


[Index(['TSQ045', 'TSQ162', 'TSQ090', 'TSQ030', 'TSQ025', 'TSQ037', 'TSQ072',
       'TSQ100', 'TSQ135', 'TSQ052', 'TSQ051', 'TSQ134', 'TSQ109', 'TSQ123',
       'TSQ005', 'TSQ089', 'TSQ076', 'TSQ055', 'TSQ167', 'TSQ113', 'TSQ121',
       'TSQ026', 'TSQ019', 'TSQ002'],
      dtype='object', name='Sample_ID'), Index(['TSQ092', 'TSQ167', 'TSQ046', 'TSQ165', 'TSQ047', 'TSQ054', 'TSQ126',
       'TSQ009', 'TSQ128', 'TSQ118', 'TSQ147', 'TSQ001', 'TSQ122', 'TSQ148',
       'TSQ159', 'TSQ111', 'TSQ038', 'TSQ107', 'TSQ051', 'TSQ154', 'TSQ103',
       'TSQ025', 'TSQ016', 'TSQ144'],
      dtype='object', name='Sample_ID'), Index(['TSQ024', 'TSQ111', 'TSQ047', 'TSQ130', 'TSQ158', 'TSQ147', 'TSQ062',
       'TSQ035', 'TSQ070', 'TSQ122', 'TSQ053', 'TSQ028', 'TSQ046', 'TSQ135',
       'TSQ114', 'TSQ044', 'TSQ049', 'TSQ063', 'TSQ157', 'TSQ167', 'TSQ029',
       'TSQ067', 'TSQ068', 'TSQ134'],
      dtype='object', name='Sample_ID'), Index(['TSQ108', 'TSQ010', 'TSQ044', 'TSQ038', 'TSQ062', 'TSQ067', 'TSQ002',
       'TSQ095', 'TSQ111', 'TSQ140', 'TSQ075', 'TSQ152', 'TSQ107', 'TSQ145',
       'TSQ034', 'TSQ159', 'TSQ154', 'TSQ133', 'TSQ170', 'TSQ039', 'TSQ162',
       'TSQ120', 'TSQ144', 'TSQ072'],
      dtype='object', name='Sample_ID'), Index(['TSQ007', 'TSQ043', 'TSQ070', 'TSQ032', 'TSQ052', 'TSQ028', 'TSQ104',
       'TSQ127', 'TSQ140', 'TSQ067', 'TSQ026', 'TSQ091', 'TSQ101', 'TSQ062',
       'TSQ086', 'TSQ123', 'TSQ126', 'TSQ065', 'TSQ171', 'TSQ128', 'TSQ168',
       'TSQ169', 'TSQ046', 'TSQ038'],
      dtype='object', name='Sample_ID'), Index(['TSQ167', 'TSQ111', 'TSQ060', 'TSQ063', 'TSQ129', 'TSQ028', 'TSQ043',
       'TSQ078', 'TSQ143', 'TSQ153', 'TSQ092', 'TSQ070', 'TSQ055', 'TSQ166',
       'TSQ125', 'TSQ051', 'TSQ049', 'TSQ080', 'TSQ072', 'TSQ144', 'TSQ031',
       'TSQ045', 'TSQ163', 'TSQ062'],
      dtype='object', name='Sample_ID'), Index(['TSQ100', 'TSQ066', 'TSQ146', 'TSQ028', 'TSQ118', 'TSQ095', 'TSQ171',
       'TSQ077', 'TSQ144', 'TSQ134', 'TSQ040', 'TSQ093', 'TSQ045', 'TSQ087',
       'TSQ046', 'TSQ120', 'TSQ092', 'TSQ088', 'TSQ053', 'TSQ074', 'TSQ125',
       'TSQ091', 'TSQ039', 'TSQ082'],
      dtype='object', name='Sample_ID'), Index(['TSQ002', 'TSQ073', 'TSQ077', 'TSQ153', 'TSQ146', 'TSQ148', 'TSQ030',
       'TSQ091', 'TSQ106', 'TSQ003', 'TSQ067', 'TSQ174', 'TSQ093', 'TSQ037',
       'TSQ175', 'TSQ112', 'TSQ145', 'TSQ092', 'TSQ135', 'TSQ038', 'TSQ044',
       'TSQ099', 'TSQ068', 'TSQ163'],
      dtype='object', name='Sample_ID'), Index(['TSQ145', 'TSQ136', 'TSQ106', 'TSQ059', 'TSQ135', 'TSQ095', 'TSQ133',
       'TSQ076', 'TSQ157', 'TSQ159', 'TSQ165', 'TSQ068', 'TSQ027', 'TSQ169',
       'TSQ020', 'TSQ114', 'TSQ120', 'TSQ025', 'TSQ028', 'TSQ001', 'TSQ007',
       'TSQ062', 'TSQ055', 'TSQ148'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ144', 'TSQ111', 'TSQ165', 'TSQ120', 'TSQ020', 'TSQ103', 'TSQ151',
       'TSQ068', 'TSQ034', 'TSQ023',
       ...
       'TSQ047', 'TSQ082', 'TSQ080', 'TSQ060', 'TSQ066', 'TSQ129', 'TSQ044',
       'TSQ021', 'TSQ114', 'TSQ153'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ078', 'TSQ045', 'TSQ135', 'TSQ007', 'TSQ059', 'TSQ125', 'TSQ039',
       'TSQ157', 'TSQ074', 'TSQ162',
       ...
       'TSQ005', 'TSQ079', 'TSQ120', 'TSQ037', 'TSQ093', 'TSQ049', 'TSQ069',
       'TSQ089', 'TSQ040', 'TSQ034'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ088', 'TSQ039', 'TSQ099', 'TSQ129', 'TSQ151', 'TSQ118', 'TSQ077',
       'TSQ040', 'TSQ093', 'TSQ069',
       ...
       'TSQ159', 'TSQ144', 'TSQ109', 'TSQ101', 'TSQ087', 'TSQ007', 'TSQ145',
       'TSQ045', 'TSQ123', 'TSQ023'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ115', 'TSQ146', 'TSQ077', 'TSQ025', 'TSQ007', 'TSQ001', 'TSQ009',
       'TSQ052', 'TSQ123', 'TSQ153',
       ...
       'TSQ057', 'TSQ060', 'TSQ030', 'TSQ121', 'TSQ013', 'TSQ028', 'TSQ080',
       'TSQ161', 'TSQ168', 'TSQ112'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ003', 'TSQ120', 'TSQ054', 'TSQ057', 'TSQ072', 'TSQ006', 'TSQ047',
       'TSQ051', 'TSQ037', 'TSQ068',
       ...
       'TSQ159', 'TSQ088', 'TSQ145', 'TSQ082', 'TSQ029', 'TSQ040', 'TSQ144',
       'TSQ073', 'TSQ162', 'TSQ031'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ158', 'TSQ041', 'TSQ136', 'TSQ117', 'TSQ001', 'TSQ037', 'TSQ082',
       'TSQ029', 'TSQ075', 'TSQ121',
       ...
       'TSQ030', 'TSQ024', 'TSQ165', 'TSQ134', 'TSQ053', 'TSQ140', 'TSQ119',
       'TSQ005', 'TSQ079', 'TSQ154'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ010', 'TSQ176', 'TSQ133', 'TSQ159', 'TSQ020', 'TSQ113', 'TSQ089',
       'TSQ127', 'TSQ121', 'TSQ070',
       ...
       'TSQ166', 'TSQ124', 'TSQ052', 'TSQ072', 'TSQ163', 'TSQ021', 'TSQ029',
       'TSQ065', 'TSQ063', 'TSQ073'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ136', 'TSQ128', 'TSQ009', 'TSQ040', 'TSQ107', 'TSQ117', 'TSQ101',
       'TSQ125', 'TSQ140', 'TSQ026',
       ...
       'TSQ082', 'TSQ151', 'TSQ035', 'TSQ115', 'TSQ059', 'TSQ168', 'TSQ143',
       'TSQ029', 'TSQ170', 'TSQ169'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ049', 'TSQ087', 'TSQ123', 'TSQ005', 'TSQ006', 'TSQ161', 'TSQ139',
       'TSQ099', 'TSQ152', 'TSQ075',
       ...
       'TSQ104', 'TSQ107', 'TSQ052', 'TSQ140', 'TSQ130', 'TSQ072', 'TSQ029',
       'TSQ101', 'TSQ077', 'TSQ038'],
      dtype='object', name='Sample_ID', length=109)]