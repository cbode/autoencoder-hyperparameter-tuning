
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 25.00%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 16.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 8.33%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 29.17%	Rep4: 25.00%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 12.50%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 12.50%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 29.17%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ099', 'TSQ075', 'TSQ041', 'TSQ035', 'TSQ169', 'TSQ158', 'TSQ052',
       'TSQ130', 'TSQ122', 'TSQ115', 'TSQ159', 'TSQ134', 'TSQ171', 'TSQ060',
       'TSQ162', 'TSQ065', 'TSQ127', 'TSQ002', 'TSQ128', 'TSQ034', 'TSQ176',
       'TSQ038', 'TSQ023', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ074', 'TSQ109', 'TSQ086', 'TSQ035', 'TSQ005', 'TSQ169', 'TSQ174',
       'TSQ067', 'TSQ007', 'TSQ147', 'TSQ065', 'TSQ129', 'TSQ170', 'TSQ068',
       'TSQ091', 'TSQ115', 'TSQ093', 'TSQ125', 'TSQ171', 'TSQ002', 'TSQ176',
       'TSQ046', 'TSQ053', 'TSQ026'],
      dtype='object', name='Sample_ID'), Index(['TSQ029', 'TSQ126', 'TSQ147', 'TSQ049', 'TSQ144', 'TSQ040', 'TSQ134',
       'TSQ046', 'TSQ066', 'TSQ023', 'TSQ053', 'TSQ115', 'TSQ067', 'TSQ047',
       'TSQ090', 'TSQ167', 'TSQ148', 'TSQ001', 'TSQ078', 'TSQ030', 'TSQ095',
       'TSQ079', 'TSQ091', 'TSQ139'],
      dtype='object', name='Sample_ID'), Index(['TSQ073', 'TSQ115', 'TSQ108', 'TSQ099', 'TSQ145', 'TSQ040', 'TSQ111',
       'TSQ159', 'TSQ062', 'TSQ087', 'TSQ054', 'TSQ037', 'TSQ136', 'TSQ127',
       'TSQ016', 'TSQ130', 'TSQ003', 'TSQ041', 'TSQ151', 'TSQ174', 'TSQ100',
       'TSQ158', 'TSQ030', 'TSQ063'],
      dtype='object', name='Sample_ID'), Index(['TSQ169', 'TSQ145', 'TSQ007', 'TSQ053', 'TSQ057', 'TSQ032', 'TSQ114',
       'TSQ037', 'TSQ103', 'TSQ151', 'TSQ093', 'TSQ092', 'TSQ157', 'TSQ046',
       'TSQ153', 'TSQ133', 'TSQ111', 'TSQ021', 'TSQ013', 'TSQ134', 'TSQ072',
       'TSQ162', 'TSQ019', 'TSQ027'],
      dtype='object', name='Sample_ID'), Index(['TSQ080', 'TSQ072', 'TSQ126', 'TSQ040', 'TSQ112', 'TSQ162', 'TSQ065',
       'TSQ062', 'TSQ114', 'TSQ078', 'TSQ076', 'TSQ135', 'TSQ148', 'TSQ117',
       'TSQ037', 'TSQ174', 'TSQ168', 'TSQ046', 'TSQ088', 'TSQ039', 'TSQ057',
       'TSQ023', 'TSQ158', 'TSQ053'],
      dtype='object', name='Sample_ID'), Index(['TSQ045', 'TSQ038', 'TSQ093', 'TSQ100', 'TSQ157', 'TSQ124', 'TSQ067',
       'TSQ010', 'TSQ088', 'TSQ032', 'TSQ151', 'TSQ037', 'TSQ121', 'TSQ159',
       'TSQ112', 'TSQ113', 'TSQ041', 'TSQ013', 'TSQ002', 'TSQ069', 'TSQ139',
       'TSQ016', 'TSQ090', 'TSQ070'],
      dtype='object', name='Sample_ID'), Index(['TSQ023', 'TSQ092', 'TSQ158', 'TSQ176', 'TSQ067', 'TSQ052', 'TSQ073',
       'TSQ106', 'TSQ066', 'TSQ051', 'TSQ143', 'TSQ055', 'TSQ145', 'TSQ108',
       'TSQ139', 'TSQ163', 'TSQ047', 'TSQ112', 'TSQ025', 'TSQ118', 'TSQ034',
       'TSQ134', 'TSQ024', 'TSQ072'],
      dtype='object', name='Sample_ID'), Index(['TSQ091', 'TSQ175', 'TSQ028', 'TSQ072', 'TSQ099', 'TSQ021', 'TSQ093',
       'TSQ009', 'TSQ075', 'TSQ065', 'TSQ038', 'TSQ165', 'TSQ145', 'TSQ051',
       'TSQ148', 'TSQ133', 'TSQ030', 'TSQ153', 'TSQ060', 'TSQ023', 'TSQ152',
       'TSQ057', 'TSQ045', 'TSQ024'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ068', 'TSQ049', 'TSQ066', 'TSQ031', 'TSQ092', 'TSQ043', 'TSQ073',
       'TSQ121', 'TSQ074', 'TSQ009',
       ...
       'TSQ161', 'TSQ039', 'TSQ072', 'TSQ013', 'TSQ170', 'TSQ101', 'TSQ157',
       'TSQ146', 'TSQ114', 'TSQ027'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ151', 'TSQ025', 'TSQ167', 'TSQ112', 'TSQ040', 'TSQ003', 'TSQ038',
       'TSQ163', 'TSQ154', 'TSQ121',
       ...
       'TSQ119', 'TSQ124', 'TSQ135', 'TSQ139', 'TSQ100', 'TSQ024', 'TSQ148',
       'TSQ075', 'TSQ101', 'TSQ031'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ135', 'TSQ087', 'TSQ019', 'TSQ174', 'TSQ038', 'TSQ062', 'TSQ074',
       'TSQ152', 'TSQ068', 'TSQ101',
       ...
       'TSQ175', 'TSQ107', 'TSQ170', 'TSQ072', 'TSQ041', 'TSQ092', 'TSQ158',
       'TSQ021', 'TSQ073', 'TSQ077'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ025', 'TSQ078', 'TSQ143', 'TSQ114', 'TSQ088', 'TSQ024', 'TSQ156',
       'TSQ170', 'TSQ029', 'TSQ154',
       ...
       'TSQ148', 'TSQ039', 'TSQ113', 'TSQ106', 'TSQ146', 'TSQ067', 'TSQ171',
       'TSQ117', 'TSQ009', 'TSQ046'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ115', 'TSQ112', 'TSQ171', 'TSQ106', 'TSQ090', 'TSQ070', 'TSQ140',
       'TSQ001', 'TSQ158', 'TSQ040',
       ...
       'TSQ125', 'TSQ054', 'TSQ005', 'TSQ031', 'TSQ117', 'TSQ135', 'TSQ024',
       'TSQ026', 'TSQ075', 'TSQ035'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ028', 'TSQ146', 'TSQ159', 'TSQ175', 'TSQ101', 'TSQ125', 'TSQ106',
       'TSQ026', 'TSQ163', 'TSQ030',
       ...
       'TSQ120', 'TSQ020', 'TSQ038', 'TSQ113', 'TSQ092', 'TSQ100', 'TSQ001',
       'TSQ119', 'TSQ137', 'TSQ029'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ128', 'TSQ176', 'TSQ074', 'TSQ163', 'TSQ087', 'TSQ063', 'TSQ075',
       'TSQ118', 'TSQ034', 'TSQ089',
       ...
       'TSQ091', 'TSQ053', 'TSQ079', 'TSQ156', 'TSQ052', 'TSQ046', 'TSQ171',
       'TSQ065', 'TSQ137', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ088', 'TSQ124', 'TSQ140', 'TSQ040', 'TSQ030', 'TSQ129', 'TSQ068',
       'TSQ126', 'TSQ125', 'TSQ091',
       ...
       'TSQ109', 'TSQ095', 'TSQ174', 'TSQ128', 'TSQ135', 'TSQ147', 'TSQ062',
       'TSQ016', 'TSQ020', 'TSQ162'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ169', 'TSQ115', 'TSQ095', 'TSQ114', 'TSQ092', 'TSQ120', 'TSQ135',
       'TSQ077', 'TSQ157', 'TSQ101',
       ...
       'TSQ063', 'TSQ055', 'TSQ013', 'TSQ103', 'TSQ043', 'TSQ134', 'TSQ147',
       'TSQ046', 'TSQ062', 'TSQ089'],
      dtype='object', name='Sample_ID', length=109)]