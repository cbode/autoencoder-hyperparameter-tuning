
Readme with Additional Information
==================================


This file contains additional Information for 20220113_SVM_9_random_runs.
# General Information

SVM Classifier 
C = 0.01
kernel = 'linear'
all other settings were set to default. 

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 20.83%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 12.50%	Rep3: 33.33%	Rep4: 33.33%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 37.50%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 16.67%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 50.00%	Rep4: 12.50%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 25.00%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 16.67%	Rep4: 33.33%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 12.50%

# Random TestSet IDs: 


[Index(['TSQ153', 'TSQ123', 'TSQ166', 'TSQ028', 'TSQ174', 'TSQ086', 'TSQ129',
       'TSQ119', 'TSQ040', 'TSQ059', 'TSQ057', 'TSQ013', 'TSQ047', 'TSQ076',
       'TSQ090', 'TSQ077', 'TSQ111', 'TSQ156', 'TSQ052', 'TSQ072', 'TSQ148',
       'TSQ154', 'TSQ068', 'TSQ100'],
      dtype='object', name='Sample_ID'), Index(['TSQ055', 'TSQ062', 'TSQ112', 'TSQ109', 'TSQ139', 'TSQ095', 'TSQ165',
       'TSQ144', 'TSQ159', 'TSQ153', 'TSQ078', 'TSQ013', 'TSQ170', 'TSQ140',
       'TSQ005', 'TSQ113', 'TSQ166', 'TSQ034', 'TSQ016', 'TSQ115', 'TSQ120',
       'TSQ126', 'TSQ107', 'TSQ066'],
      dtype='object', name='Sample_ID'), Index(['TSQ010', 'TSQ134', 'TSQ054', 'TSQ080', 'TSQ041', 'TSQ089', 'TSQ168',
       'TSQ167', 'TSQ139', 'TSQ099', 'TSQ130', 'TSQ077', 'TSQ156', 'TSQ009',
       'TSQ169', 'TSQ013', 'TSQ065', 'TSQ144', 'TSQ152', 'TSQ031', 'TSQ145',
       'TSQ082', 'TSQ092', 'TSQ040'],
      dtype='object', name='Sample_ID'), Index(['TSQ124', 'TSQ009', 'TSQ020', 'TSQ091', 'TSQ115', 'TSQ082', 'TSQ175',
       'TSQ143', 'TSQ067', 'TSQ069', 'TSQ161', 'TSQ135', 'TSQ068', 'TSQ060',
       'TSQ092', 'TSQ145', 'TSQ166', 'TSQ087', 'TSQ136', 'TSQ007', 'TSQ095',
       'TSQ072', 'TSQ171', 'TSQ053'],
      dtype='object', name='Sample_ID'), Index(['TSQ039', 'TSQ134', 'TSQ068', 'TSQ028', 'TSQ123', 'TSQ002', 'TSQ146',
       'TSQ101', 'TSQ047', 'TSQ032', 'TSQ170', 'TSQ062', 'TSQ125', 'TSQ059',
       'TSQ025', 'TSQ139', 'TSQ127', 'TSQ073', 'TSQ107', 'TSQ020', 'TSQ135',
       'TSQ115', 'TSQ024', 'TSQ167'],
      dtype='object', name='Sample_ID'), Index(['TSQ163', 'TSQ029', 'TSQ125', 'TSQ144', 'TSQ139', 'TSQ003', 'TSQ093',
       'TSQ111', 'TSQ136', 'TSQ005', 'TSQ013', 'TSQ009', 'TSQ027', 'TSQ134',
       'TSQ092', 'TSQ168', 'TSQ156', 'TSQ152', 'TSQ122', 'TSQ053', 'TSQ060',
       'TSQ166', 'TSQ032', 'TSQ089'],
      dtype='object', name='Sample_ID'), Index(['TSQ075', 'TSQ090', 'TSQ130', 'TSQ032', 'TSQ165', 'TSQ153', 'TSQ156',
       'TSQ027', 'TSQ166', 'TSQ026', 'TSQ030', 'TSQ035', 'TSQ057', 'TSQ176',
       'TSQ168', 'TSQ045', 'TSQ078', 'TSQ106', 'TSQ019', 'TSQ103', 'TSQ108',
       'TSQ113', 'TSQ021', 'TSQ059'],
      dtype='object', name='Sample_ID'), Index(['TSQ079', 'TSQ001', 'TSQ122', 'TSQ156', 'TSQ057', 'TSQ029', 'TSQ054',
       'TSQ174', 'TSQ165', 'TSQ153', 'TSQ106', 'TSQ006', 'TSQ143', 'TSQ123',
       'TSQ066', 'TSQ086', 'TSQ135', 'TSQ163', 'TSQ129', 'TSQ069', 'TSQ077',
       'TSQ023', 'TSQ108', 'TSQ027'],
      dtype='object', name='Sample_ID'), Index(['TSQ104', 'TSQ157', 'TSQ171', 'TSQ123', 'TSQ031', 'TSQ069', 'TSQ146',
       'TSQ030', 'TSQ122', 'TSQ005', 'TSQ151', 'TSQ028', 'TSQ053', 'TSQ046',
       'TSQ128', 'TSQ099', 'TSQ032', 'TSQ063', 'TSQ024', 'TSQ159', 'TSQ021',
       'TSQ013', 'TSQ003', 'TSQ167'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ055', 'TSQ120', 'TSQ049', 'TSQ117', 'TSQ093', 'TSQ009', 'TSQ171',
       'TSQ104', 'TSQ161', 'TSQ016',
       ...
       'TSQ091', 'TSQ176', 'TSQ025', 'TSQ044', 'TSQ122', 'TSQ143', 'TSQ134',
       'TSQ170', 'TSQ139', 'TSQ168'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ089', 'TSQ157', 'TSQ001', 'TSQ051', 'TSQ029', 'TSQ023', 'TSQ074',
       'TSQ045', 'TSQ119', 'TSQ030',
       ...
       'TSQ047', 'TSQ104', 'TSQ082', 'TSQ063', 'TSQ080', 'TSQ106', 'TSQ088',
       'TSQ067', 'TSQ031', 'TSQ154'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ049', 'TSQ076', 'TSQ118', 'TSQ039', 'TSQ046', 'TSQ079', 'TSQ123',
       'TSQ002', 'TSQ128', 'TSQ175',
       ...
       'TSQ007', 'TSQ146', 'TSQ078', 'TSQ024', 'TSQ159', 'TSQ107', 'TSQ027',
       'TSQ038', 'TSQ137', 'TSQ121'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ002', 'TSQ130', 'TSQ032', 'TSQ046', 'TSQ156', 'TSQ066', 'TSQ062',
       'TSQ137', 'TSQ019', 'TSQ152',
       ...
       'TSQ112', 'TSQ121', 'TSQ038', 'TSQ159', 'TSQ031', 'TSQ063', 'TSQ108',
       'TSQ041', 'TSQ076', 'TSQ169'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ128', 'TSQ140', 'TSQ075', 'TSQ103', 'TSQ152', 'TSQ049',
       'TSQ053', 'TSQ031', 'TSQ154',
       ...
       'TSQ117', 'TSQ151', 'TSQ029', 'TSQ176', 'TSQ070', 'TSQ021', 'TSQ086',
       'TSQ027', 'TSQ100', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ147', 'TSQ165', 'TSQ167', 'TSQ007', 'TSQ140', 'TSQ154',
       'TSQ099', 'TSQ143', 'TSQ109',
       ...
       'TSQ043', 'TSQ023', 'TSQ076', 'TSQ082', 'TSQ002', 'TSQ047', 'TSQ104',
       'TSQ146', 'TSQ030', 'TSQ037'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ002', 'TSQ171', 'TSQ010', 'TSQ041', 'TSQ120', 'TSQ121', 'TSQ127',
       'TSQ047', 'TSQ128', 'TSQ013',
       ...
       'TSQ052', 'TSQ038', 'TSQ158', 'TSQ076', 'TSQ146', 'TSQ023', 'TSQ162',
       'TSQ088', 'TSQ040', 'TSQ100'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ147', 'TSQ067', 'TSQ152', 'TSQ016', 'TSQ118', 'TSQ082', 'TSQ046',
       'TSQ021', 'TSQ170', 'TSQ140',
       ...
       'TSQ041', 'TSQ030', 'TSQ162', 'TSQ144', 'TSQ120', 'TSQ130', 'TSQ128',
       'TSQ119', 'TSQ045', 'TSQ035'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ066', 'TSQ038', 'TSQ037', 'TSQ165', 'TSQ080', 'TSQ161', 'TSQ059',
       'TSQ023', 'TSQ154', 'TSQ113',
       ...
       'TSQ035', 'TSQ045', 'TSQ100', 'TSQ136', 'TSQ125', 'TSQ152', 'TSQ088',
       'TSQ112', 'TSQ092', 'TSQ143'],
      dtype='object', name='Sample_ID', length=109)]