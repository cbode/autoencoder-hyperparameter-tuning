
Readme with Additional Information
==================================


This file contains additional Information for 20220129_RF_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 33.33%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 8.33%	Rep3: 25.00%	Rep4: 41.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 12.50%	Rep3: 33.33%	Rep4: 29.17%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 41.67%	Rep4: 20.83%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 25.00%	Rep4: 37.50%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 33.33%	Rep2: 12.50%	Rep3: 29.17%	Rep4: 25.00%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 25.00%

# Random TestSet IDs: 


[Index(['TSQ013', 'TSQ070', 'TSQ129', 'TSQ166', 'TSQ099', 'TSQ035', 'TSQ090',
       'TSQ159', 'TSQ089', 'TSQ086', 'TSQ157', 'TSQ167', 'TSQ156', 'TSQ063',
       'TSQ074', 'TSQ130', 'TSQ078', 'TSQ073', 'TSQ120', 'TSQ009', 'TSQ175',
       'TSQ118', 'TSQ151', 'TSQ154'],
      dtype='object', name='Sample_ID'), Index(['TSQ095', 'TSQ112', 'TSQ101', 'TSQ162', 'TSQ127', 'TSQ166', 'TSQ140',
       'TSQ118', 'TSQ087', 'TSQ047', 'TSQ167', 'TSQ139', 'TSQ157', 'TSQ003',
       'TSQ037', 'TSQ076', 'TSQ130', 'TSQ146', 'TSQ026', 'TSQ109', 'TSQ128',
       'TSQ005', 'TSQ154', 'TSQ027'],
      dtype='object', name='Sample_ID'), Index(['TSQ005', 'TSQ082', 'TSQ167', 'TSQ093', 'TSQ043', 'TSQ035', 'TSQ168',
       'TSQ128', 'TSQ068', 'TSQ054', 'TSQ099', 'TSQ027', 'TSQ120', 'TSQ103',
       'TSQ104', 'TSQ157', 'TSQ151', 'TSQ028', 'TSQ156', 'TSQ066', 'TSQ020',
       'TSQ170', 'TSQ046', 'TSQ135'],
      dtype='object', name='Sample_ID'), Index(['TSQ095', 'TSQ101', 'TSQ156', 'TSQ092', 'TSQ068', 'TSQ086', 'TSQ079',
       'TSQ034', 'TSQ077', 'TSQ126', 'TSQ107', 'TSQ121', 'TSQ007', 'TSQ133',
       'TSQ059', 'TSQ088', 'TSQ108', 'TSQ148', 'TSQ129', 'TSQ029', 'TSQ137',
       'TSQ123', 'TSQ115', 'TSQ114'],
      dtype='object', name='Sample_ID'), Index(['TSQ099', 'TSQ030', 'TSQ046', 'TSQ055', 'TSQ067', 'TSQ167', 'TSQ107',
       'TSQ101', 'TSQ045', 'TSQ137', 'TSQ092', 'TSQ106', 'TSQ139', 'TSQ024',
       'TSQ176', 'TSQ005', 'TSQ120', 'TSQ113', 'TSQ136', 'TSQ025', 'TSQ134',
       'TSQ135', 'TSQ130', 'TSQ073'],
      dtype='object', name='Sample_ID'), Index(['TSQ019', 'TSQ103', 'TSQ137', 'TSQ082', 'TSQ060', 'TSQ125', 'TSQ119',
       'TSQ038', 'TSQ157', 'TSQ158', 'TSQ041', 'TSQ162', 'TSQ099', 'TSQ040',
       'TSQ128', 'TSQ146', 'TSQ124', 'TSQ020', 'TSQ113', 'TSQ092', 'TSQ130',
       'TSQ075', 'TSQ024', 'TSQ136'],
      dtype='object', name='Sample_ID'), Index(['TSQ139', 'TSQ168', 'TSQ037', 'TSQ129', 'TSQ134', 'TSQ118', 'TSQ001',
       'TSQ009', 'TSQ151', 'TSQ121', 'TSQ143', 'TSQ047', 'TSQ120', 'TSQ136',
       'TSQ020', 'TSQ034', 'TSQ159', 'TSQ170', 'TSQ148', 'TSQ106', 'TSQ003',
       'TSQ066', 'TSQ162', 'TSQ030'],
      dtype='object', name='Sample_ID'), Index(['TSQ069', 'TSQ109', 'TSQ037', 'TSQ176', 'TSQ057', 'TSQ099', 'TSQ133',
       'TSQ049', 'TSQ101', 'TSQ118', 'TSQ002', 'TSQ137', 'TSQ122', 'TSQ038',
       'TSQ052', 'TSQ121', 'TSQ091', 'TSQ146', 'TSQ120', 'TSQ103', 'TSQ045',
       'TSQ074', 'TSQ076', 'TSQ024'],
      dtype='object', name='Sample_ID'), Index(['TSQ133', 'TSQ106', 'TSQ171', 'TSQ010', 'TSQ025', 'TSQ165', 'TSQ108',
       'TSQ130', 'TSQ090', 'TSQ006', 'TSQ054', 'TSQ039', 'TSQ167', 'TSQ114',
       'TSQ115', 'TSQ051', 'TSQ035', 'TSQ078', 'TSQ077', 'TSQ109', 'TSQ019',
       'TSQ159', 'TSQ154', 'TSQ170'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ082', 'TSQ030', 'TSQ054', 'TSQ119', 'TSQ108', 'TSQ062', 'TSQ069',
       'TSQ049', 'TSQ029', 'TSQ158',
       ...
       'TSQ091', 'TSQ161', 'TSQ006', 'TSQ016', 'TSQ163', 'TSQ040', 'TSQ112',
       'TSQ109', 'TSQ107', 'TSQ145'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ043', 'TSQ091', 'TSQ159', 'TSQ114', 'TSQ060', 'TSQ120', 'TSQ082',
       'TSQ028', 'TSQ106', 'TSQ001',
       ...
       'TSQ100', 'TSQ068', 'TSQ031', 'TSQ156', 'TSQ135', 'TSQ059', 'TSQ039',
       'TSQ057', 'TSQ158', 'TSQ111'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ153', 'TSQ171', 'TSQ001', 'TSQ125', 'TSQ088', 'TSQ063',
       'TSQ076', 'TSQ158', 'TSQ136',
       ...
       'TSQ079', 'TSQ060', 'TSQ041', 'TSQ169', 'TSQ091', 'TSQ070', 'TSQ114',
       'TSQ024', 'TSQ075', 'TSQ095'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ049', 'TSQ053', 'TSQ038', 'TSQ134', 'TSQ041', 'TSQ039',
       'TSQ127', 'TSQ090', 'TSQ093',
       ...
       'TSQ109', 'TSQ013', 'TSQ009', 'TSQ145', 'TSQ130', 'TSQ002', 'TSQ046',
       'TSQ089', 'TSQ080', 'TSQ106'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ086', 'TSQ108', 'TSQ057', 'TSQ019', 'TSQ029', 'TSQ035', 'TSQ153',
       'TSQ062', 'TSQ112', 'TSQ031',
       ...
       'TSQ082', 'TSQ147', 'TSQ127', 'TSQ074', 'TSQ003', 'TSQ148', 'TSQ013',
       'TSQ119', 'TSQ165', 'TSQ026'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ123', 'TSQ030', 'TSQ047', 'TSQ044', 'TSQ112', 'TSQ068',
       'TSQ118', 'TSQ107', 'TSQ066',
       ...
       'TSQ143', 'TSQ003', 'TSQ026', 'TSQ046', 'TSQ009', 'TSQ170', 'TSQ063',
       'TSQ166', 'TSQ169', 'TSQ121'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ111', 'TSQ158', 'TSQ099', 'TSQ067', 'TSQ174', 'TSQ152', 'TSQ137',
       'TSQ117', 'TSQ040', 'TSQ073',
       ...
       'TSQ112', 'TSQ088', 'TSQ090', 'TSQ028', 'TSQ089', 'TSQ092', 'TSQ019',
       'TSQ025', 'TSQ147', 'TSQ006'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ073', 'TSQ062', 'TSQ112', 'TSQ166', 'TSQ080', 'TSQ066', 'TSQ152',
       'TSQ027', 'TSQ113', 'TSQ129',
       ...
       'TSQ088', 'TSQ009', 'TSQ140', 'TSQ020', 'TSQ079', 'TSQ154', 'TSQ055',
       'TSQ053', 'TSQ021', 'TSQ016'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ118', 'TSQ091', 'TSQ057', 'TSQ128', 'TSQ001', 'TSQ065', 'TSQ136',
       'TSQ005', 'TSQ137', 'TSQ044',
       ...
       'TSQ151', 'TSQ144', 'TSQ020', 'TSQ047', 'TSQ145', 'TSQ060', 'TSQ119',
       'TSQ074', 'TSQ163', 'TSQ122'],
      dtype='object', name='Sample_ID', length=109)]