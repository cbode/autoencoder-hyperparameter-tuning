
Readme with Additional Information
==================================


This file contains additional Information for 20220129_RF_9_random_runs_v3.
# General Information

C = 0.01
kernel = linear


The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 20.83%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 12.50%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 33.33%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 16.67%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 16.67%	Rep3: 29.17%	Rep4: 25.00%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 16.67%	Rep3: 16.67%	Rep4: 29.17%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 12.50%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 25.00%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 20.83%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ026', 'TSQ078', 'TSQ127', 'TSQ133', 'TSQ146', 'TSQ031', 'TSQ079',
       'TSQ148', 'TSQ151', 'TSQ001', 'TSQ046', 'TSQ070', 'TSQ007', 'TSQ128',
       'TSQ145', 'TSQ053', 'TSQ113', 'TSQ129', 'TSQ021', 'TSQ066', 'TSQ077',
       'TSQ072', 'TSQ108', 'TSQ122'],
      dtype='object', name='Sample_ID'), Index(['TSQ001', 'TSQ170', 'TSQ168', 'TSQ174', 'TSQ133', 'TSQ093', 'TSQ069',
       'TSQ161', 'TSQ005', 'TSQ114', 'TSQ047', 'TSQ112', 'TSQ032', 'TSQ052',
       'TSQ104', 'TSQ118', 'TSQ044', 'TSQ065', 'TSQ054', 'TSQ045', 'TSQ063',
       'TSQ111', 'TSQ003', 'TSQ030'],
      dtype='object', name='Sample_ID'), Index(['TSQ062', 'TSQ127', 'TSQ025', 'TSQ065', 'TSQ117', 'TSQ074', 'TSQ118',
       'TSQ108', 'TSQ134', 'TSQ029', 'TSQ144', 'TSQ166', 'TSQ057', 'TSQ165',
       'TSQ171', 'TSQ040', 'TSQ088', 'TSQ115', 'TSQ059', 'TSQ130', 'TSQ016',
       'TSQ027', 'TSQ175', 'TSQ174'],
      dtype='object', name='Sample_ID'), Index(['TSQ073', 'TSQ034', 'TSQ075', 'TSQ129', 'TSQ078', 'TSQ107', 'TSQ024',
       'TSQ100', 'TSQ115', 'TSQ027', 'TSQ106', 'TSQ113', 'TSQ070', 'TSQ041',
       'TSQ019', 'TSQ003', 'TSQ068', 'TSQ135', 'TSQ093', 'TSQ175', 'TSQ005',
       'TSQ112', 'TSQ146', 'TSQ028'],
      dtype='object', name='Sample_ID'), Index(['TSQ170', 'TSQ154', 'TSQ130', 'TSQ140', 'TSQ169', 'TSQ113', 'TSQ117',
       'TSQ038', 'TSQ112', 'TSQ003', 'TSQ005', 'TSQ161', 'TSQ072', 'TSQ077',
       'TSQ030', 'TSQ095', 'TSQ075', 'TSQ037', 'TSQ063', 'TSQ159', 'TSQ137',
       'TSQ148', 'TSQ024', 'TSQ002'],
      dtype='object', name='Sample_ID'), Index(['TSQ158', 'TSQ009', 'TSQ136', 'TSQ068', 'TSQ028', 'TSQ106', 'TSQ143',
       'TSQ145', 'TSQ032', 'TSQ107', 'TSQ020', 'TSQ161', 'TSQ045', 'TSQ037',
       'TSQ059', 'TSQ046', 'TSQ174', 'TSQ023', 'TSQ072', 'TSQ003', 'TSQ117',
       'TSQ163', 'TSQ167', 'TSQ024'],
      dtype='object', name='Sample_ID'), Index(['TSQ152', 'TSQ148', 'TSQ039', 'TSQ120', 'TSQ145', 'TSQ095', 'TSQ045',
       'TSQ025', 'TSQ040', 'TSQ114', 'TSQ030', 'TSQ159', 'TSQ125', 'TSQ052',
       'TSQ161', 'TSQ174', 'TSQ156', 'TSQ019', 'TSQ133', 'TSQ134', 'TSQ170',
       'TSQ026', 'TSQ163', 'TSQ010'],
      dtype='object', name='Sample_ID'), Index(['TSQ069', 'TSQ140', 'TSQ122', 'TSQ047', 'TSQ080', 'TSQ123', 'TSQ169',
       'TSQ028', 'TSQ073', 'TSQ037', 'TSQ021', 'TSQ051', 'TSQ101', 'TSQ072',
       'TSQ165', 'TSQ117', 'TSQ159', 'TSQ057', 'TSQ128', 'TSQ045', 'TSQ055',
       'TSQ060', 'TSQ129', 'TSQ103'],
      dtype='object', name='Sample_ID'), Index(['TSQ028', 'TSQ059', 'TSQ163', 'TSQ063', 'TSQ091', 'TSQ013', 'TSQ087',
       'TSQ046', 'TSQ095', 'TSQ093', 'TSQ072', 'TSQ088', 'TSQ089', 'TSQ027',
       'TSQ120', 'TSQ049', 'TSQ111', 'TSQ055', 'TSQ126', 'TSQ169', 'TSQ034',
       'TSQ118', 'TSQ174', 'TSQ010'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ028', 'TSQ037', 'TSQ088', 'TSQ165', 'TSQ027', 'TSQ162', 'TSQ009',
       'TSQ090', 'TSQ043', 'TSQ034',
       ...
       'TSQ137', 'TSQ123', 'TSQ076', 'TSQ067', 'TSQ161', 'TSQ109', 'TSQ082',
       'TSQ107', 'TSQ086', 'TSQ112'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ167', 'TSQ086', 'TSQ049', 'TSQ079', 'TSQ090', 'TSQ166', 'TSQ089',
       'TSQ002', 'TSQ157', 'TSQ175',
       ...
       'TSQ035', 'TSQ020', 'TSQ139', 'TSQ053', 'TSQ107', 'TSQ101', 'TSQ046',
       'TSQ067', 'TSQ154', 'TSQ136'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ112', 'TSQ006', 'TSQ023', 'TSQ125', 'TSQ133', 'TSQ120', 'TSQ049',
       'TSQ044', 'TSQ039', 'TSQ145',
       ...
       'TSQ091', 'TSQ038', 'TSQ101', 'TSQ170', 'TSQ163', 'TSQ082', 'TSQ139',
       'TSQ109', 'TSQ069', 'TSQ123'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ121', 'TSQ130', 'TSQ125', 'TSQ136', 'TSQ039', 'TSQ127', 'TSQ006',
       'TSQ163', 'TSQ166', 'TSQ023',
       ...
       'TSQ077', 'TSQ103', 'TSQ120', 'TSQ111', 'TSQ074', 'TSQ029', 'TSQ030',
       'TSQ002', 'TSQ124', 'TSQ066'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ153', 'TSQ111', 'TSQ123', 'TSQ174', 'TSQ045', 'TSQ115', 'TSQ082',
       'TSQ114', 'TSQ046', 'TSQ027',
       ...
       'TSQ090', 'TSQ049', 'TSQ029', 'TSQ039', 'TSQ109', 'TSQ157', 'TSQ088',
       'TSQ108', 'TSQ070', 'TSQ055'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ041', 'TSQ109', 'TSQ154', 'TSQ039', 'TSQ082', 'TSQ051', 'TSQ055',
       'TSQ118', 'TSQ078', 'TSQ144',
       ...
       'TSQ066', 'TSQ151', 'TSQ026', 'TSQ007', 'TSQ019', 'TSQ077', 'TSQ053',
       'TSQ088', 'TSQ067', 'TSQ035'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ082', 'TSQ031', 'TSQ144', 'TSQ065', 'TSQ072', 'TSQ023',
       'TSQ005', 'TSQ123', 'TSQ020',
       ...
       'TSQ038', 'TSQ032', 'TSQ165', 'TSQ002', 'TSQ013', 'TSQ115', 'TSQ143',
       'TSQ077', 'TSQ175', 'TSQ028'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ127', 'TSQ148', 'TSQ163', 'TSQ170', 'TSQ176', 'TSQ099', 'TSQ089',
       'TSQ077', 'TSQ119', 'TSQ005',
       ...
       'TSQ153', 'TSQ167', 'TSQ046', 'TSQ034', 'TSQ031', 'TSQ092', 'TSQ154',
       'TSQ095', 'TSQ010', 'TSQ130'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ140', 'TSQ117', 'TSQ053', 'TSQ076', 'TSQ175', 'TSQ135', 'TSQ021',
       'TSQ133', 'TSQ066', 'TSQ154',
       ...
       'TSQ106', 'TSQ062', 'TSQ060', 'TSQ075', 'TSQ114', 'TSQ121', 'TSQ065',
       'TSQ047', 'TSQ170', 'TSQ101'],
      dtype='object', name='Sample_ID', length=109)]