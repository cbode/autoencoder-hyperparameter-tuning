
Readme with Additional Information
==================================


This file contains additional Information for 20220129_RF_9_random_runs_v2.
# General Information

model_name = 'SVM'
index_name = str(run) + '_' + model_name
svm =  SVC(C = 0.01,kernel = 'linear')

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 29.17%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 45.83%	Rep4: 8.33%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 29.17%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 16.67%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	12 		 that equals to 11.01% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 16.67%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 29.17%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 37.50%	Rep3: 25.00%	Rep4: 12.50%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 12.50%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 25.00%

# Random TestSet IDs: 


[Index(['TSQ100', 'TSQ122', 'TSQ032', 'TSQ163', 'TSQ137', 'TSQ152', 'TSQ099',
       'TSQ133', 'TSQ146', 'TSQ118', 'TSQ038', 'TSQ087', 'TSQ072', 'TSQ049',
       'TSQ089', 'TSQ021', 'TSQ162', 'TSQ154', 'TSQ095', 'TSQ145', 'TSQ041',
       'TSQ111', 'TSQ078', 'TSQ001'],
      dtype='object', name='Sample_ID'), Index(['TSQ091', 'TSQ068', 'TSQ090', 'TSQ001', 'TSQ076', 'TSQ007', 'TSQ144',
       'TSQ028', 'TSQ119', 'TSQ169', 'TSQ087', 'TSQ158', 'TSQ092', 'TSQ051',
       'TSQ086', 'TSQ125', 'TSQ137', 'TSQ026', 'TSQ128', 'TSQ037', 'TSQ103',
       'TSQ101', 'TSQ159', 'TSQ060'],
      dtype='object', name='Sample_ID'), Index(['TSQ118', 'TSQ060', 'TSQ075', 'TSQ062', 'TSQ158', 'TSQ087', 'TSQ117',
       'TSQ059', 'TSQ111', 'TSQ120', 'TSQ073', 'TSQ009', 'TSQ171', 'TSQ163',
       'TSQ112', 'TSQ153', 'TSQ040', 'TSQ154', 'TSQ055', 'TSQ107', 'TSQ047',
       'TSQ065', 'TSQ124', 'TSQ123'],
      dtype='object', name='Sample_ID'), Index(['TSQ147', 'TSQ005', 'TSQ151', 'TSQ082', 'TSQ099', 'TSQ159', 'TSQ175',
       'TSQ029', 'TSQ079', 'TSQ124', 'TSQ156', 'TSQ040', 'TSQ001', 'TSQ134',
       'TSQ119', 'TSQ062', 'TSQ093', 'TSQ077', 'TSQ070', 'TSQ002', 'TSQ146',
       'TSQ088', 'TSQ171', 'TSQ087'],
      dtype='object', name='Sample_ID'), Index(['TSQ019', 'TSQ021', 'TSQ080', 'TSQ147', 'TSQ167', 'TSQ086', 'TSQ123',
       'TSQ044', 'TSQ027', 'TSQ108', 'TSQ170', 'TSQ016', 'TSQ145', 'TSQ154',
       'TSQ175', 'TSQ038', 'TSQ041', 'TSQ093', 'TSQ009', 'TSQ128', 'TSQ171',
       'TSQ122', 'TSQ029', 'TSQ037'],
      dtype='object', name='Sample_ID'), Index(['TSQ035', 'TSQ010', 'TSQ088', 'TSQ115', 'TSQ053', 'TSQ109', 'TSQ005',
       'TSQ121', 'TSQ120', 'TSQ052', 'TSQ103', 'TSQ111', 'TSQ140', 'TSQ069',
       'TSQ125', 'TSQ091', 'TSQ145', 'TSQ047', 'TSQ095', 'TSQ127', 'TSQ003',
       'TSQ163', 'TSQ024', 'TSQ074'],
      dtype='object', name='Sample_ID'), Index(['TSQ144', 'TSQ159', 'TSQ119', 'TSQ029', 'TSQ112', 'TSQ163', 'TSQ079',
       'TSQ021', 'TSQ031', 'TSQ025', 'TSQ065', 'TSQ089', 'TSQ041', 'TSQ115',
       'TSQ106', 'TSQ066', 'TSQ060', 'TSQ091', 'TSQ006', 'TSQ062', 'TSQ026',
       'TSQ020', 'TSQ049', 'TSQ082'],
      dtype='object', name='Sample_ID'), Index(['TSQ115', 'TSQ121', 'TSQ044', 'TSQ053', 'TSQ028', 'TSQ035', 'TSQ114',
       'TSQ079', 'TSQ101', 'TSQ034', 'TSQ016', 'TSQ088', 'TSQ107', 'TSQ073',
       'TSQ024', 'TSQ112', 'TSQ059', 'TSQ045', 'TSQ136', 'TSQ125', 'TSQ135',
       'TSQ170', 'TSQ068', 'TSQ143'],
      dtype='object', name='Sample_ID'), Index(['TSQ176', 'TSQ068', 'TSQ080', 'TSQ166', 'TSQ072', 'TSQ121', 'TSQ117',
       'TSQ009', 'TSQ024', 'TSQ137', 'TSQ089', 'TSQ082', 'TSQ076', 'TSQ074',
       'TSQ111', 'TSQ174', 'TSQ151', 'TSQ127', 'TSQ148', 'TSQ092', 'TSQ087',
       'TSQ035', 'TSQ010', 'TSQ053'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ047', 'TSQ159', 'TSQ168', 'TSQ045', 'TSQ144', 'TSQ101', 'TSQ120',
       'TSQ025', 'TSQ171', 'TSQ166',
       ...
       'TSQ103', 'TSQ073', 'TSQ074', 'TSQ060', 'TSQ090', 'TSQ143', 'TSQ076',
       'TSQ046', 'TSQ125', 'TSQ151'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ023', 'TSQ120', 'TSQ168', 'TSQ003', 'TSQ063', 'TSQ174', 'TSQ043',
       'TSQ005', 'TSQ044', 'TSQ032',
       ...
       'TSQ170', 'TSQ029', 'TSQ100', 'TSQ107', 'TSQ019', 'TSQ130', 'TSQ146',
       'TSQ114', 'TSQ161', 'TSQ104'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ090', 'TSQ006', 'TSQ156', 'TSQ007', 'TSQ145', 'TSQ037', 'TSQ024',
       'TSQ126', 'TSQ080', 'TSQ046',
       ...
       'TSQ001', 'TSQ100', 'TSQ054', 'TSQ170', 'TSQ089', 'TSQ035', 'TSQ101',
       'TSQ078', 'TSQ115', 'TSQ152'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ089', 'TSQ113', 'TSQ016', 'TSQ091', 'TSQ121', 'TSQ115', 'TSQ063',
       'TSQ152', 'TSQ163', 'TSQ114',
       ...
       'TSQ168', 'TSQ100', 'TSQ123', 'TSQ069', 'TSQ135', 'TSQ174', 'TSQ103',
       'TSQ041', 'TSQ074', 'TSQ057'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ075', 'TSQ158', 'TSQ120', 'TSQ020', 'TSQ088', 'TSQ049', 'TSQ035',
       'TSQ139', 'TSQ144', 'TSQ161',
       ...
       'TSQ119', 'TSQ002', 'TSQ157', 'TSQ082', 'TSQ055', 'TSQ059', 'TSQ107',
       'TSQ109', 'TSQ074', 'TSQ129'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ009', 'TSQ176', 'TSQ123', 'TSQ086', 'TSQ041', 'TSQ153', 'TSQ126',
       'TSQ075', 'TSQ170', 'TSQ076',
       ...
       'TSQ013', 'TSQ130', 'TSQ034', 'TSQ137', 'TSQ020', 'TSQ040', 'TSQ063',
       'TSQ023', 'TSQ046', 'TSQ067'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ169', 'TSQ124', 'TSQ077', 'TSQ158', 'TSQ032', 'TSQ074', 'TSQ137',
       'TSQ023', 'TSQ088', 'TSQ157',
       ...
       'TSQ101', 'TSQ100', 'TSQ087', 'TSQ113', 'TSQ117', 'TSQ055', 'TSQ024',
       'TSQ068', 'TSQ093', 'TSQ168'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ128', 'TSQ089', 'TSQ040', 'TSQ175', 'TSQ021', 'TSQ065', 'TSQ093',
       'TSQ032', 'TSQ077', 'TSQ130',
       ...
       'TSQ095', 'TSQ023', 'TSQ166', 'TSQ080', 'TSQ072', 'TSQ137', 'TSQ054',
       'TSQ161', 'TSQ005', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ039', 'TSQ170', 'TSQ073', 'TSQ103', 'TSQ162', 'TSQ143', 'TSQ147',
       'TSQ095', 'TSQ038', 'TSQ167',
       ...
       'TSQ067', 'TSQ106', 'TSQ047', 'TSQ086', 'TSQ144', 'TSQ040', 'TSQ088',
       'TSQ093', 'TSQ169', 'TSQ113'],
      dtype='object', name='Sample_ID', length=109)]