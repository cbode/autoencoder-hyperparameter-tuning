
Readme with Additional Information
==================================


This file contains additional Information for 20220113_RF_9_random_runs.
# General Information

Random Forest Classifier on compressed Dataset 
n_estimator = 100
max_depth = 6 
all other settings were default 


The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 12.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	24 		 that equals to 22.02% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	12 		 that equals to 11.01% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 25.00%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 20.83%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 25.00%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 Oe_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 16.67%	Rep2: 16.67%	Rep3: 41.67%	Rep4: 25.00%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 37.50%	Rep3: 20.83%	Rep4: 25.00%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 16.67%	Rep2: 37.50%	Rep3: 33.33%	Rep4: 12.50%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 16.67%

# Random TestSet IDs: 


[Index(['TSQ054', 'TSQ082', 'TSQ049', 'TSQ075', 'TSQ030', 'TSQ041', 'TSQ108',
       'TSQ028', 'TSQ092', 'TSQ162', 'TSQ025', 'TSQ114', 'TSQ174', 'TSQ133',
       'TSQ148', 'TSQ145', 'TSQ006', 'TSQ091', 'TSQ080', 'TSQ124', 'TSQ037',
       'TSQ125', 'TSQ024', 'TSQ002'],
      dtype='object', name='Sample_ID'), Index(['TSQ162', 'TSQ129', 'TSQ060', 'TSQ065', 'TSQ044', 'TSQ026', 'TSQ134',
       'TSQ119', 'TSQ076', 'TSQ126', 'TSQ121', 'TSQ023', 'TSQ136', 'TSQ092',
       'TSQ144', 'TSQ013', 'TSQ147', 'TSQ005', 'TSQ070', 'TSQ093', 'TSQ095',
       'TSQ019', 'TSQ108', 'TSQ055'],
      dtype='object', name='Sample_ID'), Index(['TSQ130', 'TSQ053', 'TSQ128', 'TSQ089', 'TSQ111', 'TSQ125', 'TSQ002',
       'TSQ086', 'TSQ051', 'TSQ057', 'TSQ154', 'TSQ171', 'TSQ038', 'TSQ117',
       'TSQ082', 'TSQ030', 'TSQ120', 'TSQ023', 'TSQ010', 'TSQ134', 'TSQ135',
       'TSQ168', 'TSQ076', 'TSQ031'],
      dtype='object', name='Sample_ID'), Index(['TSQ052', 'TSQ128', 'TSQ121', 'TSQ175', 'TSQ127', 'TSQ087', 'TSQ020',
       'TSQ130', 'TSQ057', 'TSQ145', 'TSQ158', 'TSQ078', 'TSQ112', 'TSQ074',
       'TSQ047', 'TSQ037', 'TSQ068', 'TSQ032', 'TSQ070', 'TSQ063', 'TSQ002',
       'TSQ163', 'TSQ135', 'TSQ170'],
      dtype='object', name='Sample_ID'), Index(['TSQ104', 'TSQ020', 'TSQ027', 'TSQ126', 'TSQ108', 'TSQ136', 'TSQ158',
       'TSQ175', 'TSQ148', 'TSQ135', 'TSQ029', 'TSQ162', 'TSQ168', 'TSQ077',
       'TSQ165', 'TSQ068', 'TSQ103', 'TSQ009', 'TSQ170', 'TSQ176', 'TSQ025',
       'TSQ153', 'TSQ166', 'TSQ080'],
      dtype='object', name='Sample_ID'), Index(['TSQ089', 'TSQ108', 'TSQ104', 'TSQ088', 'TSQ103', 'TSQ009', 'TSQ076',
       'TSQ127', 'TSQ086', 'TSQ087', 'TSQ029', 'TSQ174', 'TSQ063', 'TSQ077',
       'TSQ070', 'TSQ001', 'TSQ069', 'TSQ143', 'TSQ053', 'TSQ109', 'TSQ080',
       'TSQ119', 'TSQ100', 'TSQ159'],
      dtype='object', name='Sample_ID'), Index(['TSQ016', 'TSQ108', 'TSQ067', 'TSQ040', 'TSQ072', 'TSQ171', 'TSQ125',
       'TSQ049', 'TSQ037', 'TSQ156', 'TSQ166', 'TSQ029', 'TSQ001', 'TSQ032',
       'TSQ109', 'TSQ077', 'TSQ154', 'TSQ163', 'TSQ028', 'TSQ035', 'TSQ170',
       'TSQ117', 'TSQ068', 'TSQ038'],
      dtype='object', name='Sample_ID'), Index(['TSQ057', 'TSQ147', 'TSQ006', 'TSQ169', 'TSQ103', 'TSQ162', 'TSQ089',
       'TSQ145', 'TSQ024', 'TSQ139', 'TSQ100', 'TSQ134', 'TSQ065', 'TSQ077',
       'TSQ044', 'TSQ001', 'TSQ117', 'TSQ035', 'TSQ076', 'TSQ086', 'TSQ158',
       'TSQ041', 'TSQ021', 'TSQ030'],
      dtype='object', name='Sample_ID'), Index(['TSQ126', 'TSQ020', 'TSQ023', 'TSQ029', 'TSQ024', 'TSQ002', 'TSQ104',
       'TSQ111', 'TSQ170', 'TSQ060', 'TSQ145', 'TSQ025', 'TSQ106', 'TSQ095',
       'TSQ075', 'TSQ030', 'TSQ065', 'TSQ003', 'TSQ047', 'TSQ070', 'TSQ118',
       'TSQ093', 'TSQ175', 'TSQ156'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ121', 'TSQ152', 'TSQ166', 'TSQ171', 'TSQ040', 'TSQ090', 'TSQ153',
       'TSQ136', 'TSQ055', 'TSQ143',
       ...
       'TSQ154', 'TSQ123', 'TSQ059', 'TSQ063', 'TSQ074', 'TSQ046', 'TSQ034',
       'TSQ135', 'TSQ043', 'TSQ140'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ057', 'TSQ133', 'TSQ063', 'TSQ107', 'TSQ127', 'TSQ176', 'TSQ078',
       'TSQ112', 'TSQ091', 'TSQ156',
       ...
       'TSQ032', 'TSQ082', 'TSQ163', 'TSQ021', 'TSQ170', 'TSQ166', 'TSQ037',
       'TSQ031', 'TSQ086', 'TSQ043'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ059', 'TSQ073', 'TSQ123', 'TSQ122', 'TSQ027', 'TSQ147', 'TSQ077',
       'TSQ099', 'TSQ115', 'TSQ143',
       ...
       'TSQ025', 'TSQ072', 'TSQ140', 'TSQ024', 'TSQ040', 'TSQ100', 'TSQ044',
       'TSQ166', 'TSQ021', 'TSQ103'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ034', 'TSQ035', 'TSQ075', 'TSQ129', 'TSQ171', 'TSQ161', 'TSQ076',
       'TSQ044', 'TSQ122', 'TSQ165',
       ...
       'TSQ153', 'TSQ162', 'TSQ088', 'TSQ176', 'TSQ117', 'TSQ126', 'TSQ029',
       'TSQ151', 'TSQ100', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ093', 'TSQ044', 'TSQ062', 'TSQ072', 'TSQ030', 'TSQ052', 'TSQ127',
       'TSQ038', 'TSQ028', 'TSQ122',
       ...
       'TSQ100', 'TSQ035', 'TSQ007', 'TSQ037', 'TSQ121', 'TSQ109', 'TSQ089',
       'TSQ107', 'TSQ043', 'TSQ129'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ060', 'TSQ137', 'TSQ065', 'TSQ123', 'TSQ095', 'TSQ147', 'TSQ161',
       'TSQ176', 'TSQ039', 'TSQ112',
       ...
       'TSQ162', 'TSQ157', 'TSQ040', 'TSQ146', 'TSQ072', 'TSQ054', 'TSQ082',
       'TSQ163', 'TSQ038', 'TSQ055'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ130', 'TSQ158', 'TSQ140', 'TSQ114', 'TSQ147', 'TSQ100',
       'TSQ057', 'TSQ031', 'TSQ139',
       ...
       'TSQ066', 'TSQ078', 'TSQ161', 'TSQ134', 'TSQ146', 'TSQ044', 'TSQ027',
       'TSQ128', 'TSQ095', 'TSQ159'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ174', 'TSQ107', 'TSQ127', 'TSQ068', 'TSQ115', 'TSQ133', 'TSQ153',
       'TSQ088', 'TSQ019', 'TSQ051',
       ...
       'TSQ111', 'TSQ016', 'TSQ046', 'TSQ104', 'TSQ040', 'TSQ161', 'TSQ047',
       'TSQ013', 'TSQ167', 'TSQ039'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ128', 'TSQ062', 'TSQ034', 'TSQ059', 'TSQ077', 'TSQ013', 'TSQ165',
       'TSQ039', 'TSQ067', 'TSQ091',
       ...
       'TSQ101', 'TSQ073', 'TSQ174', 'TSQ087', 'TSQ019', 'TSQ148', 'TSQ117',
       'TSQ055', 'TSQ121', 'TSQ010'],
      dtype='object', name='Sample_ID', length=109)]