# Readme file with additional information
This folder contains the data for the Estimation of the max depth and the number of estimators of a Random Forest Classifier. 
The dataset was split in a stratified Sampled Way using the Datasets file found in: Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

max_depth tested: [2,6,10, None]
n_estimators tested: [10,50,100]

all other settings were set to default. The mean of 9 runs (3 * the number of stratified sampled Datasets) was calculated and saved into the file for each setting.


Values found (mean): 
 	        max_depth	n_estimators	mean_error
Setting 1	    2	        10	        0.574074
Setting 2	    2	        50	        0.555556
Setting 3	    2	        100        0.537037
Setting 4	    6	        10	        0.541667
Setting 5	    6	        50	        0.504630
Setting 6	    6	        100	     0.467593
Setting 7	    10	        10	        0.472222
Setting 8	    10	        50	        0.546296
Setting 9	    10	        100	    0.537037
Setting 10	    None	    10	        0.550926
Setting 11	    None	    50	        0.518519
Setting 12	    None	    100	    0.527778
