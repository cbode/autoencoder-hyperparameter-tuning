
Readme with Additional Information
==================================


This file contains additional Information for 20220113_RF_9_random_runs_v2.
# General Information


Reconstructed Dataset Version 2.
max_depth = 6
n_estimators = 10 
all other settings as default.

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 41.67%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 8.33%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 33.33%	Rep4: 25.00%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 4.17%	Rep3: 33.33%	Rep4: 29.17%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 16.67%	Rep3: 29.17%	Rep4: 29.17%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 20.83%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 12.50%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 37.50%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 20.83%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 12.50%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ168', 'TSQ052', 'TSQ107', 'TSQ082', 'TSQ039', 'TSQ047', 'TSQ031',
       'TSQ026', 'TSQ043', 'TSQ005', 'TSQ166', 'TSQ159', 'TSQ126', 'TSQ067',
       'TSQ057', 'TSQ077', 'TSQ118', 'TSQ027', 'TSQ113', 'TSQ123', 'TSQ023',
       'TSQ088', 'TSQ060', 'TSQ045'],
      dtype='object', name='Sample_ID'), Index(['TSQ119', 'TSQ118', 'TSQ006', 'TSQ120', 'TSQ151', 'TSQ107', 'TSQ169',
       'TSQ156', 'TSQ068', 'TSQ133', 'TSQ030', 'TSQ100', 'TSQ124', 'TSQ092',
       'TSQ154', 'TSQ019', 'TSQ126', 'TSQ021', 'TSQ035', 'TSQ001', 'TSQ078',
       'TSQ125', 'TSQ003', 'TSQ002'],
      dtype='object', name='Sample_ID'), Index(['TSQ120', 'TSQ152', 'TSQ035', 'TSQ159', 'TSQ158', 'TSQ082', 'TSQ093',
       'TSQ024', 'TSQ080', 'TSQ137', 'TSQ054', 'TSQ005', 'TSQ175', 'TSQ079',
       'TSQ118', 'TSQ166', 'TSQ099', 'TSQ092', 'TSQ119', 'TSQ122', 'TSQ045',
       'TSQ001', 'TSQ130', 'TSQ112'],
      dtype='object', name='Sample_ID'), Index(['TSQ031', 'TSQ051', 'TSQ106', 'TSQ112', 'TSQ049', 'TSQ069', 'TSQ161',
       'TSQ066', 'TSQ068', 'TSQ127', 'TSQ109', 'TSQ025', 'TSQ129', 'TSQ067',
       'TSQ117', 'TSQ089', 'TSQ157', 'TSQ158', 'TSQ135', 'TSQ159', 'TSQ028',
       'TSQ024', 'TSQ099', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ024', 'TSQ045', 'TSQ140', 'TSQ003', 'TSQ169', 'TSQ152', 'TSQ101',
       'TSQ167', 'TSQ051', 'TSQ062', 'TSQ100', 'TSQ175', 'TSQ039', 'TSQ165',
       'TSQ088', 'TSQ065', 'TSQ089', 'TSQ032', 'TSQ007', 'TSQ156', 'TSQ067',
       'TSQ078', 'TSQ107', 'TSQ038'],
      dtype='object', name='Sample_ID'), Index(['TSQ153', 'TSQ126', 'TSQ047', 'TSQ121', 'TSQ092', 'TSQ009', 'TSQ067',
       'TSQ016', 'TSQ123', 'TSQ113', 'TSQ109', 'TSQ128', 'TSQ120', 'TSQ028',
       'TSQ076', 'TSQ151', 'TSQ041', 'TSQ170', 'TSQ129', 'TSQ124', 'TSQ163',
       'TSQ175', 'TSQ089', 'TSQ040'],
      dtype='object', name='Sample_ID'), Index(['TSQ129', 'TSQ090', 'TSQ052', 'TSQ158', 'TSQ063', 'TSQ113', 'TSQ156',
       'TSQ086', 'TSQ140', 'TSQ070', 'TSQ166', 'TSQ153', 'TSQ169', 'TSQ023',
       'TSQ020', 'TSQ163', 'TSQ038', 'TSQ001', 'TSQ089', 'TSQ137', 'TSQ047',
       'TSQ030', 'TSQ117', 'TSQ025'],
      dtype='object', name='Sample_ID'), Index(['TSQ126', 'TSQ093', 'TSQ037', 'TSQ030', 'TSQ007', 'TSQ148', 'TSQ111',
       'TSQ153', 'TSQ067', 'TSQ145', 'TSQ009', 'TSQ134', 'TSQ091', 'TSQ035',
       'TSQ010', 'TSQ104', 'TSQ031', 'TSQ003', 'TSQ080', 'TSQ099', 'TSQ023',
       'TSQ163', 'TSQ074', 'TSQ021'],
      dtype='object', name='Sample_ID'), Index(['TSQ124', 'TSQ103', 'TSQ025', 'TSQ095', 'TSQ128', 'TSQ026', 'TSQ104',
       'TSQ111', 'TSQ080', 'TSQ176', 'TSQ161', 'TSQ052', 'TSQ166', 'TSQ016',
       'TSQ066', 'TSQ169', 'TSQ125', 'TSQ037', 'TSQ067', 'TSQ020', 'TSQ163',
       'TSQ076', 'TSQ051', 'TSQ023'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ161', 'TSQ134', 'TSQ006', 'TSQ109', 'TSQ169', 'TSQ176', 'TSQ135',
       'TSQ080', 'TSQ059', 'TSQ139',
       ...
       'TSQ100', 'TSQ038', 'TSQ127', 'TSQ124', 'TSQ170', 'TSQ030', 'TSQ119',
       'TSQ157', 'TSQ128', 'TSQ070'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ020', 'TSQ095', 'TSQ158', 'TSQ073', 'TSQ027', 'TSQ088', 'TSQ171',
       'TSQ108', 'TSQ059', 'TSQ062',
       ...
       'TSQ054', 'TSQ041', 'TSQ099', 'TSQ057', 'TSQ082', 'TSQ086', 'TSQ039',
       'TSQ130', 'TSQ090', 'TSQ067'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ139', 'TSQ157', 'TSQ090', 'TSQ049', 'TSQ066', 'TSQ107', 'TSQ026',
       'TSQ059', 'TSQ095', 'TSQ043',
       ...
       'TSQ145', 'TSQ111', 'TSQ034', 'TSQ060', 'TSQ165', 'TSQ134', 'TSQ161',
       'TSQ136', 'TSQ135', 'TSQ176'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ054', 'TSQ174', 'TSQ165', 'TSQ119', 'TSQ152', 'TSQ134', 'TSQ074',
       'TSQ146', 'TSQ108', 'TSQ137',
       ...
       'TSQ003', 'TSQ029', 'TSQ104', 'TSQ121', 'TSQ153', 'TSQ023', 'TSQ021',
       'TSQ007', 'TSQ136', 'TSQ044'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ049', 'TSQ161', 'TSQ118', 'TSQ126', 'TSQ087', 'TSQ162', 'TSQ119',
       'TSQ176', 'TSQ046', 'TSQ040',
       ...
       'TSQ034', 'TSQ134', 'TSQ054', 'TSQ174', 'TSQ117', 'TSQ129', 'TSQ074',
       'TSQ157', 'TSQ079', 'TSQ005'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ144', 'TSQ020', 'TSQ080', 'TSQ019', 'TSQ148', 'TSQ024', 'TSQ101',
       'TSQ001', 'TSQ165', 'TSQ133',
       ...
       'TSQ069', 'TSQ070', 'TSQ166', 'TSQ025', 'TSQ108', 'TSQ130', 'TSQ035',
       'TSQ161', 'TSQ107', 'TSQ090'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ028', 'TSQ006', 'TSQ128', 'TSQ148', 'TSQ151', 'TSQ139', 'TSQ066',
       'TSQ019', 'TSQ175', 'TSQ057',
       ...
       'TSQ095', 'TSQ032', 'TSQ111', 'TSQ147', 'TSQ060', 'TSQ154', 'TSQ103',
       'TSQ088', 'TSQ100', 'TSQ093'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ159', 'TSQ133', 'TSQ078', 'TSQ095', 'TSQ118', 'TSQ122', 'TSQ060',
       'TSQ053', 'TSQ026', 'TSQ109',
       ...
       'TSQ135', 'TSQ019', 'TSQ166', 'TSQ136', 'TSQ124', 'TSQ120', 'TSQ016',
       'TSQ013', 'TSQ073', 'TSQ162'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ122', 'TSQ133', 'TSQ075', 'TSQ152', 'TSQ119', 'TSQ090', 'TSQ003',
       'TSQ134', 'TSQ127', 'TSQ069',
       ...
       'TSQ027', 'TSQ101', 'TSQ010', 'TSQ088', 'TSQ009', 'TSQ156', 'TSQ013',
       'TSQ087', 'TSQ135', 'TSQ108'],
      dtype='object', name='Sample_ID', length=109)]