
Readme with Additional Information
==================================


This file contains additional Information for 20220113_RF_9_random_runs.
# General Information


Reconstructed Dataset 
max_depth = 6
n_estimators = 10 
all other settings as default.

The Dataset was split randomly into a test and a training set 

# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 25.00%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 20.83%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
		 Fb_RKG	12 		 that equals to 11.01% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 41.67%	Rep3: 20.83%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	9 		 that equals to 37.50% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 25.00%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 20.83%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 8.33%	Rep3: 41.67%	Rep4: 29.17%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	9 		 that equals to 37.50% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 29.17%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 25.00%	Rep3: 41.67%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ049', 'TSQ091', 'TSQ005', 'TSQ088', 'TSQ045', 'TSQ099', 'TSQ062',
       'TSQ095', 'TSQ140', 'TSQ080', 'TSQ167', 'TSQ157', 'TSQ104', 'TSQ023',
       'TSQ112', 'TSQ156', 'TSQ021', 'TSQ152', 'TSQ171', 'TSQ163', 'TSQ078',
       'TSQ118', 'TSQ039', 'TSQ076'],
      dtype='object', name='Sample_ID'), Index(['TSQ037', 'TSQ103', 'TSQ163', 'TSQ034', 'TSQ143', 'TSQ007', 'TSQ040',
       'TSQ167', 'TSQ019', 'TSQ047', 'TSQ165', 'TSQ073', 'TSQ109', 'TSQ112',
       'TSQ067', 'TSQ068', 'TSQ157', 'TSQ080', 'TSQ092', 'TSQ126', 'TSQ020',
       'TSQ153', 'TSQ134', 'TSQ089'],
      dtype='object', name='Sample_ID'), Index(['TSQ040', 'TSQ024', 'TSQ100', 'TSQ062', 'TSQ171', 'TSQ092', 'TSQ034',
       'TSQ010', 'TSQ139', 'TSQ052', 'TSQ119', 'TSQ079', 'TSQ112', 'TSQ035',
       'TSQ108', 'TSQ037', 'TSQ051', 'TSQ123', 'TSQ106', 'TSQ044', 'TSQ009',
       'TSQ043', 'TSQ019', 'TSQ066'],
      dtype='object', name='Sample_ID'), Index(['TSQ140', 'TSQ162', 'TSQ038', 'TSQ076', 'TSQ063', 'TSQ151', 'TSQ005',
       'TSQ068', 'TSQ122', 'TSQ148', 'TSQ134', 'TSQ069', 'TSQ007', 'TSQ072',
       'TSQ054', 'TSQ075', 'TSQ119', 'TSQ039', 'TSQ109', 'TSQ156', 'TSQ028',
       'TSQ137', 'TSQ067', 'TSQ133'],
      dtype='object', name='Sample_ID'), Index(['TSQ158', 'TSQ129', 'TSQ154', 'TSQ140', 'TSQ003', 'TSQ168', 'TSQ135',
       'TSQ114', 'TSQ057', 'TSQ054', 'TSQ151', 'TSQ157', 'TSQ128', 'TSQ080',
       'TSQ031', 'TSQ115', 'TSQ073', 'TSQ079', 'TSQ130', 'TSQ062', 'TSQ020',
       'TSQ104', 'TSQ067', 'TSQ086'],
      dtype='object', name='Sample_ID'), Index(['TSQ024', 'TSQ030', 'TSQ166', 'TSQ026', 'TSQ134', 'TSQ122', 'TSQ111',
       'TSQ055', 'TSQ108', 'TSQ137', 'TSQ040', 'TSQ135', 'TSQ016', 'TSQ115',
       'TSQ086', 'TSQ153', 'TSQ151', 'TSQ045', 'TSQ107', 'TSQ075', 'TSQ029',
       'TSQ052', 'TSQ005', 'TSQ175'],
      dtype='object', name='Sample_ID'), Index(['TSQ169', 'TSQ045', 'TSQ143', 'TSQ147', 'TSQ112', 'TSQ122', 'TSQ123',
       'TSQ034', 'TSQ118', 'TSQ130', 'TSQ115', 'TSQ108', 'TSQ003', 'TSQ121',
       'TSQ161', 'TSQ067', 'TSQ126', 'TSQ039', 'TSQ134', 'TSQ082', 'TSQ145',
       'TSQ111', 'TSQ054', 'TSQ127'],
      dtype='object', name='Sample_ID'), Index(['TSQ168', 'TSQ174', 'TSQ039', 'TSQ082', 'TSQ101', 'TSQ062', 'TSQ103',
       'TSQ107', 'TSQ069', 'TSQ106', 'TSQ088', 'TSQ041', 'TSQ157', 'TSQ109',
       'TSQ031', 'TSQ038', 'TSQ046', 'TSQ057', 'TSQ099', 'TSQ175', 'TSQ140',
       'TSQ060', 'TSQ067', 'TSQ134'],
      dtype='object', name='Sample_ID'), Index(['TSQ152', 'TSQ091', 'TSQ145', 'TSQ171', 'TSQ144', 'TSQ117', 'TSQ031',
       'TSQ137', 'TSQ151', 'TSQ135', 'TSQ019', 'TSQ076', 'TSQ147', 'TSQ109',
       'TSQ103', 'TSQ114', 'TSQ119', 'TSQ040', 'TSQ065', 'TSQ001', 'TSQ093',
       'TSQ059', 'TSQ052', 'TSQ013'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ175', 'TSQ020', 'TSQ002', 'TSQ043', 'TSQ074', 'TSQ051', 'TSQ162',
       'TSQ103', 'TSQ108', 'TSQ166',
       ...
       'TSQ174', 'TSQ029', 'TSQ077', 'TSQ070', 'TSQ139', 'TSQ145', 'TSQ122',
       'TSQ068', 'TSQ134', 'TSQ059'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ044', 'TSQ072', 'TSQ161', 'TSQ146', 'TSQ107', 'TSQ168', 'TSQ049',
       'TSQ016', 'TSQ041', 'TSQ099',
       ...
       'TSQ169', 'TSQ128', 'TSQ078', 'TSQ124', 'TSQ038', 'TSQ122', 'TSQ100',
       'TSQ086', 'TSQ003', 'TSQ029'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ067', 'TSQ115', 'TSQ111', 'TSQ148', 'TSQ023', 'TSQ001', 'TSQ152',
       'TSQ057', 'TSQ125', 'TSQ080',
       ...
       'TSQ007', 'TSQ095', 'TSQ088', 'TSQ026', 'TSQ087', 'TSQ104', 'TSQ032',
       'TSQ030', 'TSQ154', 'TSQ073'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ167', 'TSQ031', 'TSQ016', 'TSQ092', 'TSQ029', 'TSQ158', 'TSQ165',
       'TSQ032', 'TSQ090', 'TSQ055',
       ...
       'TSQ136', 'TSQ093', 'TSQ077', 'TSQ146', 'TSQ046', 'TSQ040', 'TSQ100',
       'TSQ127', 'TSQ152', 'TSQ074'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ052', 'TSQ029', 'TSQ156', 'TSQ030', 'TSQ091', 'TSQ010', 'TSQ074',
       'TSQ006', 'TSQ134', 'TSQ124',
       ...
       'TSQ106', 'TSQ027', 'TSQ005', 'TSQ159', 'TSQ145', 'TSQ026', 'TSQ136',
       'TSQ044', 'TSQ133', 'TSQ034'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ144', 'TSQ079', 'TSQ031', 'TSQ133', 'TSQ114', 'TSQ034', 'TSQ069',
       'TSQ152', 'TSQ123', 'TSQ070',
       ...
       'TSQ013', 'TSQ073', 'TSQ113', 'TSQ171', 'TSQ093', 'TSQ126', 'TSQ078',
       'TSQ003', 'TSQ174', 'TSQ129'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ170', 'TSQ092', 'TSQ157', 'TSQ113', 'TSQ101', 'TSQ069', 'TSQ166',
       'TSQ129', 'TSQ152', 'TSQ103',
       ...
       'TSQ021', 'TSQ091', 'TSQ057', 'TSQ041', 'TSQ001', 'TSQ095', 'TSQ168',
       'TSQ016', 'TSQ044', 'TSQ028'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ124', 'TSQ003', 'TSQ135', 'TSQ136', 'TSQ010', 'TSQ111', 'TSQ035',
       'TSQ079', 'TSQ047', 'TSQ053',
       ...
       'TSQ165', 'TSQ151', 'TSQ146', 'TSQ044', 'TSQ037', 'TSQ133', 'TSQ154',
       'TSQ117', 'TSQ104', 'TSQ086'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ054', 'TSQ161', 'TSQ159', 'TSQ067', 'TSQ158', 'TSQ128', 'TSQ104',
       'TSQ045', 'TSQ167', 'TSQ063',
       ...
       'TSQ038', 'TSQ032', 'TSQ009', 'TSQ111', 'TSQ003', 'TSQ134', 'TSQ108',
       'TSQ005', 'TSQ140', 'TSQ066'],
      dtype='object', name='Sample_ID', length=109)]