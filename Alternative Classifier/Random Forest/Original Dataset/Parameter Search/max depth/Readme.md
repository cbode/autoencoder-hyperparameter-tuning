# Readme file with additional information
This folder contains the data and the plots for the Estimation of the max depth of a Random Forest Classifier using n_estimators. 
The dataset was split in a stratified Sampled Way using the Datasets file found in: Stratified_Sampling_Dataset_removed_Outliers_v3.pkl

Cs tested: [2,6,10, None]

Values found (mean): 
2      0.500000
6      0.425926
10     0.439815
None    0.444444