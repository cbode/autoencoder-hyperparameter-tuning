
Readme with Additional Information
==================================


This file contains additional Information for 20220113_NB_9_random_runs.
# General Information

The original Dataset was analyzed

Random Forest Classifier 
n_estimator = 100
max_depth = 10
all other settings were default

The Dataset was split randomly into a test and a training set 

# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 45.83%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 12.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 33.33%	Rep2: 8.33%	Rep3: 33.33%	Rep4: 25.00%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 16.67%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 25.00%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 12.50%	Rep3: 33.33%	Rep4: 20.83%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 16.67%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 12.50%	Rep3: 25.00%	Rep4: 33.33%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 16.67%	Rep3: 37.50%	Rep4: 29.17%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 12.50%	Rep3: 29.17%	Rep4: 29.17%

# Random TestSet IDs: 


[Index(['TSQ151', 'TSQ171', 'TSQ034', 'TSQ057', 'TSQ067', 'TSQ068', 'TSQ070',
       'TSQ144', 'TSQ045', 'TSQ002', 'TSQ079', 'TSQ163', 'TSQ062', 'TSQ078',
       'TSQ154', 'TSQ063', 'TSQ124', 'TSQ145', 'TSQ030', 'TSQ167', 'TSQ082',
       'TSQ055', 'TSQ074', 'TSQ049'],
      dtype='object', name='Sample_ID'), Index(['TSQ139', 'TSQ136', 'TSQ034', 'TSQ046', 'TSQ159', 'TSQ124', 'TSQ117',
       'TSQ104', 'TSQ127', 'TSQ175', 'TSQ169', 'TSQ146', 'TSQ027', 'TSQ047',
       'TSQ067', 'TSQ091', 'TSQ063', 'TSQ060', 'TSQ143', 'TSQ109', 'TSQ049',
       'TSQ040', 'TSQ144', 'TSQ082'],
      dtype='object', name='Sample_ID'), Index(['TSQ135', 'TSQ117', 'TSQ129', 'TSQ082', 'TSQ124', 'TSQ093', 'TSQ136',
       'TSQ075', 'TSQ001', 'TSQ052', 'TSQ013', 'TSQ091', 'TSQ089', 'TSQ077',
       'TSQ111', 'TSQ163', 'TSQ059', 'TSQ023', 'TSQ154', 'TSQ166', 'TSQ019',
       'TSQ146', 'TSQ007', 'TSQ040'],
      dtype='object', name='Sample_ID'), Index(['TSQ087', 'TSQ034', 'TSQ140', 'TSQ176', 'TSQ145', 'TSQ051', 'TSQ122',
       'TSQ099', 'TSQ020', 'TSQ129', 'TSQ063', 'TSQ165', 'TSQ168', 'TSQ003',
       'TSQ153', 'TSQ026', 'TSQ134', 'TSQ089', 'TSQ077', 'TSQ123', 'TSQ126',
       'TSQ100', 'TSQ104', 'TSQ005'],
      dtype='object', name='Sample_ID'), Index(['TSQ163', 'TSQ156', 'TSQ129', 'TSQ159', 'TSQ032', 'TSQ013', 'TSQ079',
       'TSQ059', 'TSQ054', 'TSQ047', 'TSQ113', 'TSQ049', 'TSQ165', 'TSQ043',
       'TSQ078', 'TSQ115', 'TSQ169', 'TSQ106', 'TSQ167', 'TSQ090', 'TSQ069',
       'TSQ158', 'TSQ001', 'TSQ174'],
      dtype='object', name='Sample_ID'), Index(['TSQ139', 'TSQ086', 'TSQ175', 'TSQ039', 'TSQ013', 'TSQ114', 'TSQ101',
       'TSQ143', 'TSQ154', 'TSQ016', 'TSQ090', 'TSQ002', 'TSQ025', 'TSQ009',
       'TSQ001', 'TSQ026', 'TSQ088', 'TSQ006', 'TSQ168', 'TSQ024', 'TSQ020',
       'TSQ169', 'TSQ103', 'TSQ055'],
      dtype='object', name='Sample_ID'), Index(['TSQ070', 'TSQ091', 'TSQ140', 'TSQ054', 'TSQ158', 'TSQ154', 'TSQ109',
       'TSQ049', 'TSQ089', 'TSQ119', 'TSQ027', 'TSQ074', 'TSQ043', 'TSQ166',
       'TSQ013', 'TSQ153', 'TSQ046', 'TSQ025', 'TSQ114', 'TSQ176', 'TSQ128',
       'TSQ038', 'TSQ103', 'TSQ152'],
      dtype='object', name='Sample_ID'), Index(['TSQ039', 'TSQ088', 'TSQ157', 'TSQ100', 'TSQ047', 'TSQ139', 'TSQ167',
       'TSQ130', 'TSQ023', 'TSQ104', 'TSQ165', 'TSQ030', 'TSQ003', 'TSQ114',
       'TSQ120', 'TSQ091', 'TSQ092', 'TSQ040', 'TSQ125', 'TSQ140', 'TSQ025',
       'TSQ108', 'TSQ143', 'TSQ090'],
      dtype='object', name='Sample_ID'), Index(['TSQ082', 'TSQ091', 'TSQ029', 'TSQ037', 'TSQ120', 'TSQ107', 'TSQ104',
       'TSQ115', 'TSQ035', 'TSQ158', 'TSQ108', 'TSQ123', 'TSQ045', 'TSQ038',
       'TSQ027', 'TSQ016', 'TSQ159', 'TSQ119', 'TSQ106', 'TSQ151', 'TSQ086',
       'TSQ113', 'TSQ117', 'TSQ074'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ020', 'TSQ120', 'TSQ122', 'TSQ089', 'TSQ113', 'TSQ010', 'TSQ088',
       'TSQ139', 'TSQ126', 'TSQ051',
       ...
       'TSQ123', 'TSQ100', 'TSQ133', 'TSQ047', 'TSQ117', 'TSQ176', 'TSQ032',
       'TSQ090', 'TSQ080', 'TSQ021'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ128', 'TSQ069', 'TSQ054', 'TSQ052', 'TSQ099', 'TSQ007', 'TSQ013',
       'TSQ154', 'TSQ137', 'TSQ019',
       ...
       'TSQ151', 'TSQ009', 'TSQ090', 'TSQ103', 'TSQ107', 'TSQ113', 'TSQ174',
       'TSQ080', 'TSQ147', 'TSQ145'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ153', 'TSQ088', 'TSQ029', 'TSQ171', 'TSQ137', 'TSQ060', 'TSQ130',
       'TSQ043', 'TSQ095', 'TSQ005',
       ...
       'TSQ003', 'TSQ044', 'TSQ055', 'TSQ134', 'TSQ006', 'TSQ074', 'TSQ010',
       'TSQ176', 'TSQ046', 'TSQ120'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ021', 'TSQ127', 'TSQ086', 'TSQ057', 'TSQ032', 'TSQ066', 'TSQ076',
       'TSQ082', 'TSQ010', 'TSQ103',
       ...
       'TSQ107', 'TSQ028', 'TSQ007', 'TSQ070', 'TSQ109', 'TSQ130', 'TSQ062',
       'TSQ147', 'TSQ068', 'TSQ151'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ074', 'TSQ153', 'TSQ133', 'TSQ123', 'TSQ122', 'TSQ077', 'TSQ070',
       'TSQ152', 'TSQ087', 'TSQ027',
       ...
       'TSQ108', 'TSQ075', 'TSQ104', 'TSQ128', 'TSQ162', 'TSQ002', 'TSQ031',
       'TSQ143', 'TSQ109', 'TSQ021'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ077', 'TSQ047', 'TSQ045', 'TSQ046', 'TSQ151', 'TSQ060', 'TSQ153',
       'TSQ076', 'TSQ070', 'TSQ069',
       ...
       'TSQ067', 'TSQ051', 'TSQ163', 'TSQ167', 'TSQ122', 'TSQ049', 'TSQ054',
       'TSQ145', 'TSQ176', 'TSQ007'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ007', 'TSQ040', 'TSQ147', 'TSQ125', 'TSQ087', 'TSQ060', 'TSQ095',
       'TSQ099', 'TSQ113', 'TSQ133',
       ...
       'TSQ111', 'TSQ072', 'TSQ157', 'TSQ108', 'TSQ130', 'TSQ144', 'TSQ137',
       'TSQ115', 'TSQ127', 'TSQ136'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ045', 'TSQ024', 'TSQ156', 'TSQ095', 'TSQ016', 'TSQ127', 'TSQ145',
       'TSQ068', 'TSQ026', 'TSQ126',
       ...
       'TSQ146', 'TSQ154', 'TSQ066', 'TSQ038', 'TSQ041', 'TSQ163', 'TSQ067',
       'TSQ005', 'TSQ052', 'TSQ122'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ055', 'TSQ089', 'TSQ062', 'TSQ065', 'TSQ040', 'TSQ053', 'TSQ051',
       'TSQ135', 'TSQ127', 'TSQ137',
       ...
       'TSQ147', 'TSQ044', 'TSQ139', 'TSQ070', 'TSQ114', 'TSQ140', 'TSQ148',
       'TSQ068', 'TSQ079', 'TSQ161'],
      dtype='object', name='Sample_ID', length=109)]