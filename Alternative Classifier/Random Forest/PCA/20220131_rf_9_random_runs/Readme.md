
Readme with Additional Information
==================================


This file contains additional Information for 20220131_rf_9_random_runs.
# General Information


Here copy all information of the method

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 29.17%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 33.33%	Rep3: 20.83%	Rep4: 16.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 25.00%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 16.67%	Rep3: 25.00%	Rep4: 37.50%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 20.83%	Rep4: 33.33%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 29.17%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 29.17%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	22 		 that equals to 20.18% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 37.50%	Rep2: 33.33%	Rep3: 8.33%	Rep4: 20.83%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 25.00%

# Random TestSet IDs: 


[Index(['TSQ062', 'TSQ063', 'TSQ076', 'TSQ113', 'TSQ043', 'TSQ175', 'TSQ139',
       'TSQ075', 'TSQ107', 'TSQ129', 'TSQ166', 'TSQ137', 'TSQ165', 'TSQ159',
       'TSQ115', 'TSQ106', 'TSQ109', 'TSQ006', 'TSQ003', 'TSQ146', 'TSQ029',
       'TSQ013', 'TSQ060', 'TSQ009'],
      dtype='object', name='Sample_ID'), Index(['TSQ161', 'TSQ013', 'TSQ154', 'TSQ049', 'TSQ010', 'TSQ122', 'TSQ024',
       'TSQ030', 'TSQ107', 'TSQ007', 'TSQ129', 'TSQ028', 'TSQ062', 'TSQ114',
       'TSQ115', 'TSQ088', 'TSQ039', 'TSQ005', 'TSQ078', 'TSQ080', 'TSQ055',
       'TSQ123', 'TSQ082', 'TSQ145'],
      dtype='object', name='Sample_ID'), Index(['TSQ100', 'TSQ122', 'TSQ013', 'TSQ062', 'TSQ099', 'TSQ090', 'TSQ051',
       'TSQ003', 'TSQ152', 'TSQ154', 'TSQ035', 'TSQ077', 'TSQ031', 'TSQ073',
       'TSQ170', 'TSQ104', 'TSQ162', 'TSQ045', 'TSQ020', 'TSQ047', 'TSQ079',
       'TSQ127', 'TSQ039', 'TSQ139'],
      dtype='object', name='Sample_ID'), Index(['TSQ112', 'TSQ040', 'TSQ049', 'TSQ165', 'TSQ175', 'TSQ126', 'TSQ103',
       'TSQ088', 'TSQ089', 'TSQ099', 'TSQ162', 'TSQ082', 'TSQ152', 'TSQ120',
       'TSQ001', 'TSQ020', 'TSQ091', 'TSQ151', 'TSQ143', 'TSQ037', 'TSQ075',
       'TSQ113', 'TSQ106', 'TSQ057'],
      dtype='object', name='Sample_ID'), Index(['TSQ108', 'TSQ075', 'TSQ026', 'TSQ088', 'TSQ034', 'TSQ127', 'TSQ103',
       'TSQ074', 'TSQ106', 'TSQ025', 'TSQ122', 'TSQ171', 'TSQ163', 'TSQ129',
       'TSQ087', 'TSQ049', 'TSQ139', 'TSQ121', 'TSQ009', 'TSQ167', 'TSQ091',
       'TSQ120', 'TSQ047', 'TSQ077'],
      dtype='object', name='Sample_ID'), Index(['TSQ029', 'TSQ086', 'TSQ108', 'TSQ001', 'TSQ127', 'TSQ047', 'TSQ100',
       'TSQ162', 'TSQ078', 'TSQ143', 'TSQ039', 'TSQ170', 'TSQ169', 'TSQ009',
       'TSQ137', 'TSQ010', 'TSQ089', 'TSQ147', 'TSQ159', 'TSQ151', 'TSQ013',
       'TSQ032', 'TSQ176', 'TSQ118'],
      dtype='object', name='Sample_ID'), Index(['TSQ171', 'TSQ136', 'TSQ109', 'TSQ040', 'TSQ065', 'TSQ069', 'TSQ002',
       'TSQ163', 'TSQ049', 'TSQ007', 'TSQ153', 'TSQ034', 'TSQ129', 'TSQ013',
       'TSQ168', 'TSQ175', 'TSQ087', 'TSQ108', 'TSQ103', 'TSQ121', 'TSQ023',
       'TSQ031', 'TSQ134', 'TSQ045'],
      dtype='object', name='Sample_ID'), Index(['TSQ157', 'TSQ066', 'TSQ139', 'TSQ040', 'TSQ158', 'TSQ082', 'TSQ006',
       'TSQ080', 'TSQ073', 'TSQ019', 'TSQ026', 'TSQ052', 'TSQ078', 'TSQ121',
       'TSQ037', 'TSQ151', 'TSQ120', 'TSQ059', 'TSQ034', 'TSQ130', 'TSQ029',
       'TSQ075', 'TSQ016', 'TSQ072'],
      dtype='object', name='Sample_ID'), Index(['TSQ147', 'TSQ090', 'TSQ176', 'TSQ174', 'TSQ086', 'TSQ171', 'TSQ078',
       'TSQ139', 'TSQ148', 'TSQ087', 'TSQ053', 'TSQ047', 'TSQ038', 'TSQ031',
       'TSQ122', 'TSQ099', 'TSQ159', 'TSQ140', 'TSQ076', 'TSQ060', 'TSQ010',
       'TSQ153', 'TSQ065', 'TSQ062'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ090', 'TSQ111', 'TSQ108', 'TSQ007', 'TSQ136', 'TSQ157', 'TSQ114',
       'TSQ077', 'TSQ125', 'TSQ067',
       ...
       'TSQ066', 'TSQ122', 'TSQ158', 'TSQ140', 'TSQ001', 'TSQ120', 'TSQ134',
       'TSQ088', 'TSQ099', 'TSQ034'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ053', 'TSQ076', 'TSQ118', 'TSQ095', 'TSQ031', 'TSQ054', 'TSQ060',
       'TSQ091', 'TSQ113', 'TSQ152',
       ...
       'TSQ034', 'TSQ151', 'TSQ019', 'TSQ169', 'TSQ134', 'TSQ087', 'TSQ038',
       'TSQ016', 'TSQ037', 'TSQ002'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ067', 'TSQ174', 'TSQ029', 'TSQ171', 'TSQ019', 'TSQ007', 'TSQ037',
       'TSQ124', 'TSQ030', 'TSQ068',
       ...
       'TSQ052', 'TSQ024', 'TSQ088', 'TSQ143', 'TSQ123', 'TSQ161', 'TSQ175',
       'TSQ059', 'TSQ023', 'TSQ136'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ100', 'TSQ156', 'TSQ063', 'TSQ133', 'TSQ021', 'TSQ079', 'TSQ013',
       'TSQ128', 'TSQ104', 'TSQ076',
       ...
       'TSQ159', 'TSQ074', 'TSQ034', 'TSQ078', 'TSQ032', 'TSQ109', 'TSQ002',
       'TSQ065', 'TSQ023', 'TSQ154'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ092', 'TSQ005', 'TSQ090', 'TSQ078', 'TSQ013', 'TSQ060', 'TSQ051',
       'TSQ079', 'TSQ125', 'TSQ114',
       ...
       'TSQ030', 'TSQ158', 'TSQ020', 'TSQ059', 'TSQ130', 'TSQ134', 'TSQ115',
       'TSQ112', 'TSQ109', 'TSQ086'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ023', 'TSQ171', 'TSQ057', 'TSQ079', 'TSQ119', 'TSQ075', 'TSQ158',
       'TSQ133', 'TSQ163', 'TSQ144',
       ...
       'TSQ165', 'TSQ122', 'TSQ130', 'TSQ024', 'TSQ128', 'TSQ136', 'TSQ028',
       'TSQ092', 'TSQ156', 'TSQ114'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ086', 'TSQ176', 'TSQ174', 'TSQ113', 'TSQ072', 'TSQ130', 'TSQ158',
       'TSQ016', 'TSQ074', 'TSQ135',
       ...
       'TSQ073', 'TSQ052', 'TSQ092', 'TSQ143', 'TSQ082', 'TSQ019', 'TSQ066',
       'TSQ167', 'TSQ120', 'TSQ076'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ099', 'TSQ045', 'TSQ122', 'TSQ093', 'TSQ154', 'TSQ092', 'TSQ134',
       'TSQ104', 'TSQ108', 'TSQ049',
       ...
       'TSQ030', 'TSQ007', 'TSQ166', 'TSQ020', 'TSQ009', 'TSQ062', 'TSQ107',
       'TSQ153', 'TSQ088', 'TSQ148'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ121', 'TSQ072', 'TSQ126', 'TSQ124', 'TSQ129', 'TSQ023', 'TSQ002',
       'TSQ039', 'TSQ068', 'TSQ165',
       ...
       'TSQ130', 'TSQ109', 'TSQ106', 'TSQ134', 'TSQ127', 'TSQ151', 'TSQ144',
       'TSQ063', 'TSQ059', 'TSQ166'],
      dtype='object', name='Sample_ID', length=109)]