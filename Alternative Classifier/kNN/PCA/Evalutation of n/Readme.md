#Readme 
File with additional information. 

This experiment was done to try to find the optimal k value for the dataset compressed Datsaet (32 dim compressed with the Autoencoder in 19.10.2021). 
The Dataset was split in a stratified Sampled approach 3 times and the error rate was calculated for every k from 1 to 50. The respective plots can be seen in the powerpoint file. The data was stored in a DataFrame (columns for the 9 runs (Dataset 1,2,3 and this was repeated 3 times) and index for the k-value). The k value with the overall lowest Error will be chosen. The kNN method had default setting (except for k value)

k    Error-rate
1     0.513889
2     0.555556
3     0.611111
4     0.486111
5     0.541667
6     0.500000
7     0.527778
8     0.527778
9     0.569444
10    0.527778
11    0.500000
12    0.527778
13    0.472222
14    0.486111
15    0.500000
16    0.500000
17    0.583333
18    0.569444
19    0.583333
20    0.569444
21    0.597222
22    0.611111
23    0.625000
24    0.652778
25    0.638889
26    0.694444
27    0.694444
28    0.666667
29    0.666667
30    0.652778
31    0.694444
32    0.708333
33    0.694444
34    0.694444
35    0.680556
36    0.680556
37    0.680556
38    0.666667
39    0.666667
40    0.680556
41    0.708333
42    0.722222
43    0.708333
44    0.694444
45    0.694444
46    0.694444
47    0.750000
48    0.736111
49    0.722222
50    0.708333 
