
Readme with Additional Information
==================================


This file contains additional Information for 20220131_kNN_9_random_runs.
# General Information


kNN algortihm with 
k = 13 

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 33.33%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 29.17%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 16.67%	Rep4: 33.33%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 41.67%	Rep4: 8.33%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 16.67%	Rep3: 20.83%	Rep4: 33.33%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 12.50%	Rep3: 37.50%	Rep4: 33.33%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	10 		 that equals to 41.67% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 41.67%	Rep2: 12.50%	Rep3: 29.17%	Rep4: 16.67%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 41.67%	Rep4: 8.33%

# Random TestSet IDs: 


[Index(['TSQ074', 'TSQ115', 'TSQ021', 'TSQ029', 'TSQ034', 'TSQ028', 'TSQ134',
       'TSQ135', 'TSQ082', 'TSQ046', 'TSQ176', 'TSQ137', 'TSQ109', 'TSQ111',
       'TSQ140', 'TSQ129', 'TSQ127', 'TSQ019', 'TSQ045', 'TSQ121', 'TSQ136',
       'TSQ114', 'TSQ161', 'TSQ128'],
      dtype='object', name='Sample_ID'), Index(['TSQ051', 'TSQ158', 'TSQ057', 'TSQ088', 'TSQ019', 'TSQ171', 'TSQ119',
       'TSQ063', 'TSQ139', 'TSQ165', 'TSQ128', 'TSQ020', 'TSQ152', 'TSQ027',
       'TSQ037', 'TSQ002', 'TSQ129', 'TSQ100', 'TSQ030', 'TSQ159', 'TSQ124',
       'TSQ121', 'TSQ001', 'TSQ157'],
      dtype='object', name='Sample_ID'), Index(['TSQ125', 'TSQ052', 'TSQ009', 'TSQ136', 'TSQ045', 'TSQ066', 'TSQ106',
       'TSQ019', 'TSQ108', 'TSQ007', 'TSQ168', 'TSQ109', 'TSQ001', 'TSQ143',
       'TSQ104', 'TSQ163', 'TSQ174', 'TSQ003', 'TSQ016', 'TSQ070', 'TSQ118',
       'TSQ027', 'TSQ120', 'TSQ006'],
      dtype='object', name='Sample_ID'), Index(['TSQ143', 'TSQ162', 'TSQ038', 'TSQ137', 'TSQ053', 'TSQ100', 'TSQ076',
       'TSQ128', 'TSQ067', 'TSQ144', 'TSQ024', 'TSQ029', 'TSQ043', 'TSQ055',
       'TSQ019', 'TSQ051', 'TSQ118', 'TSQ134', 'TSQ091', 'TSQ037', 'TSQ088',
       'TSQ117', 'TSQ147', 'TSQ013'],
      dtype='object', name='Sample_ID'), Index(['TSQ148', 'TSQ143', 'TSQ122', 'TSQ104', 'TSQ159', 'TSQ032', 'TSQ006',
       'TSQ082', 'TSQ093', 'TSQ060', 'TSQ023', 'TSQ101', 'TSQ051', 'TSQ005',
       'TSQ077', 'TSQ114', 'TSQ167', 'TSQ031', 'TSQ019', 'TSQ024', 'TSQ103',
       'TSQ046', 'TSQ145', 'TSQ119'],
      dtype='object', name='Sample_ID'), Index(['TSQ112', 'TSQ002', 'TSQ128', 'TSQ174', 'TSQ082', 'TSQ087', 'TSQ092',
       'TSQ139', 'TSQ168', 'TSQ031', 'TSQ152', 'TSQ143', 'TSQ038', 'TSQ001',
       'TSQ007', 'TSQ035', 'TSQ045', 'TSQ059', 'TSQ161', 'TSQ157', 'TSQ119',
       'TSQ074', 'TSQ111', 'TSQ107'],
      dtype='object', name='Sample_ID'), Index(['TSQ026', 'TSQ137', 'TSQ148', 'TSQ091', 'TSQ001', 'TSQ171', 'TSQ107',
       'TSQ119', 'TSQ147', 'TSQ049', 'TSQ162', 'TSQ093', 'TSQ087', 'TSQ111',
       'TSQ031', 'TSQ117', 'TSQ109', 'TSQ101', 'TSQ127', 'TSQ170', 'TSQ161',
       'TSQ078', 'TSQ039', 'TSQ163'],
      dtype='object', name='Sample_ID'), Index(['TSQ082', 'TSQ049', 'TSQ119', 'TSQ072', 'TSQ124', 'TSQ135', 'TSQ093',
       'TSQ077', 'TSQ029', 'TSQ086', 'TSQ122', 'TSQ047', 'TSQ057', 'TSQ144',
       'TSQ003', 'TSQ069', 'TSQ034', 'TSQ024', 'TSQ095', 'TSQ103', 'TSQ162',
       'TSQ005', 'TSQ127', 'TSQ168'],
      dtype='object', name='Sample_ID'), Index(['TSQ054', 'TSQ003', 'TSQ068', 'TSQ168', 'TSQ089', 'TSQ159', 'TSQ007',
       'TSQ027', 'TSQ065', 'TSQ055', 'TSQ091', 'TSQ124', 'TSQ053', 'TSQ006',
       'TSQ158', 'TSQ082', 'TSQ114', 'TSQ109', 'TSQ154', 'TSQ136', 'TSQ104',
       'TSQ029', 'TSQ037', 'TSQ147'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ144', 'TSQ053', 'TSQ159', 'TSQ125', 'TSQ152', 'TSQ073', 'TSQ027',
       'TSQ039', 'TSQ130', 'TSQ171',
       ...
       'TSQ167', 'TSQ063', 'TSQ047', 'TSQ154', 'TSQ170', 'TSQ060', 'TSQ031',
       'TSQ092', 'TSQ091', 'TSQ166'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ049', 'TSQ069', 'TSQ134', 'TSQ055', 'TSQ101', 'TSQ091', 'TSQ060',
       'TSQ079', 'TSQ052', 'TSQ007',
       ...
       'TSQ041', 'TSQ066', 'TSQ137', 'TSQ067', 'TSQ087', 'TSQ038', 'TSQ136',
       'TSQ144', 'TSQ095', 'TSQ115'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ041', 'TSQ095', 'TSQ167', 'TSQ122', 'TSQ072', 'TSQ092', 'TSQ065',
       'TSQ013', 'TSQ043', 'TSQ037',
       ...
       'TSQ020', 'TSQ010', 'TSQ123', 'TSQ117', 'TSQ133', 'TSQ021', 'TSQ025',
       'TSQ044', 'TSQ069', 'TSQ175'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ125', 'TSQ169', 'TSQ151', 'TSQ007', 'TSQ005', 'TSQ044', 'TSQ156',
       'TSQ127', 'TSQ101', 'TSQ121',
       ...
       'TSQ066', 'TSQ152', 'TSQ092', 'TSQ119', 'TSQ130', 'TSQ108', 'TSQ002',
       'TSQ040', 'TSQ047', 'TSQ062'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ080', 'TSQ100', 'TSQ171', 'TSQ065', 'TSQ054', 'TSQ136', 'TSQ118',
       'TSQ158', 'TSQ126', 'TSQ055',
       ...
       'TSQ091', 'TSQ154', 'TSQ152', 'TSQ162', 'TSQ010', 'TSQ089', 'TSQ112',
       'TSQ137', 'TSQ146', 'TSQ047'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ148', 'TSQ049', 'TSQ021', 'TSQ118', 'TSQ115', 'TSQ130', 'TSQ165',
       'TSQ009', 'TSQ020', 'TSQ099',
       ...
       'TSQ095', 'TSQ052', 'TSQ166', 'TSQ127', 'TSQ016', 'TSQ175', 'TSQ145',
       'TSQ046', 'TSQ080', 'TSQ129'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ112', 'TSQ076', 'TSQ174', 'TSQ139', 'TSQ134', 'TSQ106', 'TSQ124',
       'TSQ120', 'TSQ158', 'TSQ089',
       ...
       'TSQ066', 'TSQ129', 'TSQ100', 'TSQ168', 'TSQ035', 'TSQ023', 'TSQ077',
       'TSQ114', 'TSQ151', 'TSQ072'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ025', 'TSQ016', 'TSQ153', 'TSQ080', 'TSQ147', 'TSQ115', 'TSQ051',
       'TSQ106', 'TSQ054', 'TSQ175',
       ...
       'TSQ176', 'TSQ167', 'TSQ163', 'TSQ148', 'TSQ088', 'TSQ019', 'TSQ052',
       'TSQ140', 'TSQ031', 'TSQ107'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ073', 'TSQ122', 'TSQ044', 'TSQ175', 'TSQ133', 'TSQ128', 'TSQ100',
       'TSQ045', 'TSQ040', 'TSQ130',
       ...
       'TSQ035', 'TSQ077', 'TSQ087', 'TSQ052', 'TSQ143', 'TSQ115', 'TSQ062',
       'TSQ139', 'TSQ074', 'TSQ127'],
      dtype='object', name='Sample_ID', length=109)]