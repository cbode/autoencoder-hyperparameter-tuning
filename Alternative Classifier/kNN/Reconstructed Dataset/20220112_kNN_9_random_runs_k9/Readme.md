
Readme with Additional Information
==================================


This file contains additional Information for 20220112_kNN_9_random_runs_k9.
# General Information

The reconstructed Dataset was analyzed: 
/Users/Clemens/Documents/MPI Work/Experimental Data and Graphs/Autoencoder/Compressed Data Run/211020_32_dim_2000_Epochs/Data/Reconstructed_merged_df.pkl 
k value = 9 

All other settings were set to default 

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 12.50%	Rep2: 37.50%	Rep3: 33.33%	Rep4: 16.67%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 16.67%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 25.00%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 20.83%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 29.17%	Rep3: 16.67%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	9 		 that equals to 37.50% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 8.33%	Rep2: 33.33%	Rep3: 45.83%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 12.50%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 37.50%	Rep4: 16.67%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 20.83%	Rep3: 50.00%	Rep4: 12.50%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 12.50%

# Random TestSet IDs: 


[Index(['TSQ114', 'TSQ158', 'TSQ053', 'TSQ121', 'TSQ069', 'TSQ055', 'TSQ086',
       'TSQ170', 'TSQ100', 'TSQ166', 'TSQ119', 'TSQ169', 'TSQ009', 'TSQ024',
       'TSQ054', 'TSQ075', 'TSQ082', 'TSQ140', 'TSQ115', 'TSQ125', 'TSQ040',
       'TSQ074', 'TSQ006', 'TSQ117'],
      dtype='object', name='Sample_ID'), Index(['TSQ054', 'TSQ137', 'TSQ068', 'TSQ009', 'TSQ090', 'TSQ127', 'TSQ107',
       'TSQ065', 'TSQ167', 'TSQ059', 'TSQ125', 'TSQ145', 'TSQ002', 'TSQ062',
       'TSQ035', 'TSQ122', 'TSQ024', 'TSQ128', 'TSQ003', 'TSQ095', 'TSQ082',
       'TSQ088', 'TSQ146', 'TSQ114'],
      dtype='object', name='Sample_ID'), Index(['TSQ055', 'TSQ067', 'TSQ167', 'TSQ134', 'TSQ153', 'TSQ005', 'TSQ031',
       'TSQ010', 'TSQ148', 'TSQ046', 'TSQ020', 'TSQ052', 'TSQ128', 'TSQ088',
       'TSQ053', 'TSQ091', 'TSQ079', 'TSQ108', 'TSQ124', 'TSQ037', 'TSQ170',
       'TSQ095', 'TSQ026', 'TSQ175'],
      dtype='object', name='Sample_ID'), Index(['TSQ140', 'TSQ040', 'TSQ095', 'TSQ108', 'TSQ054', 'TSQ031', 'TSQ001',
       'TSQ080', 'TSQ070', 'TSQ093', 'TSQ068', 'TSQ103', 'TSQ175', 'TSQ053',
       'TSQ117', 'TSQ156', 'TSQ037', 'TSQ006', 'TSQ030', 'TSQ035', 'TSQ115',
       'TSQ002', 'TSQ052', 'TSQ079'],
      dtype='object', name='Sample_ID'), Index(['TSQ111', 'TSQ135', 'TSQ062', 'TSQ051', 'TSQ103', 'TSQ157', 'TSQ044',
       'TSQ087', 'TSQ115', 'TSQ104', 'TSQ107', 'TSQ136', 'TSQ065', 'TSQ070',
       'TSQ134', 'TSQ161', 'TSQ021', 'TSQ118', 'TSQ167', 'TSQ124', 'TSQ168',
       'TSQ001', 'TSQ030', 'TSQ075'],
      dtype='object', name='Sample_ID'), Index(['TSQ099', 'TSQ124', 'TSQ065', 'TSQ037', 'TSQ092', 'TSQ068', 'TSQ100',
       'TSQ028', 'TSQ021', 'TSQ087', 'TSQ027', 'TSQ024', 'TSQ060', 'TSQ122',
       'TSQ016', 'TSQ109', 'TSQ047', 'TSQ120', 'TSQ009', 'TSQ088', 'TSQ126',
       'TSQ086', 'TSQ133', 'TSQ077'],
      dtype='object', name='Sample_ID'), Index(['TSQ126', 'TSQ153', 'TSQ143', 'TSQ013', 'TSQ134', 'TSQ103', 'TSQ039',
       'TSQ162', 'TSQ125', 'TSQ109', 'TSQ123', 'TSQ063', 'TSQ024', 'TSQ030',
       'TSQ171', 'TSQ040', 'TSQ158', 'TSQ068', 'TSQ010', 'TSQ002', 'TSQ122',
       'TSQ025', 'TSQ137', 'TSQ067'],
      dtype='object', name='Sample_ID'), Index(['TSQ169', 'TSQ119', 'TSQ144', 'TSQ076', 'TSQ091', 'TSQ010', 'TSQ123',
       'TSQ020', 'TSQ161', 'TSQ156', 'TSQ134', 'TSQ087', 'TSQ145', 'TSQ003',
       'TSQ128', 'TSQ112', 'TSQ037', 'TSQ167', 'TSQ054', 'TSQ113', 'TSQ067',
       'TSQ034', 'TSQ114', 'TSQ124'],
      dtype='object', name='Sample_ID'), Index(['TSQ145', 'TSQ093', 'TSQ062', 'TSQ043', 'TSQ079', 'TSQ001', 'TSQ007',
       'TSQ053', 'TSQ139', 'TSQ133', 'TSQ114', 'TSQ046', 'TSQ077', 'TSQ151',
       'TSQ038', 'TSQ112', 'TSQ059', 'TSQ129', 'TSQ168', 'TSQ072', 'TSQ080',
       'TSQ158', 'TSQ020', 'TSQ006'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ148', 'TSQ025', 'TSQ113', 'TSQ051', 'TSQ038', 'TSQ073', 'TSQ165',
       'TSQ005', 'TSQ029', 'TSQ037',
       ...
       'TSQ028', 'TSQ043', 'TSQ062', 'TSQ057', 'TSQ067', 'TSQ016', 'TSQ068',
       'TSQ090', 'TSQ060', 'TSQ013'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ038', 'TSQ168', 'TSQ075', 'TSQ037', 'TSQ046', 'TSQ154', 'TSQ111',
       'TSQ117', 'TSQ026', 'TSQ021',
       ...
       'TSQ115', 'TSQ119', 'TSQ023', 'TSQ013', 'TSQ157', 'TSQ091', 'TSQ134',
       'TSQ057', 'TSQ073', 'TSQ148'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ082', 'TSQ176', 'TSQ065', 'TSQ109', 'TSQ101', 'TSQ125', 'TSQ054',
       'TSQ130', 'TSQ045', 'TSQ057',
       ...
       'TSQ006', 'TSQ029', 'TSQ068', 'TSQ024', 'TSQ001', 'TSQ156', 'TSQ070',
       'TSQ059', 'TSQ147', 'TSQ129'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ130', 'TSQ067', 'TSQ025', 'TSQ024', 'TSQ099', 'TSQ167', 'TSQ148',
       'TSQ170', 'TSQ166', 'TSQ139',
       ...
       'TSQ075', 'TSQ104', 'TSQ003', 'TSQ144', 'TSQ135', 'TSQ162', 'TSQ145',
       'TSQ062', 'TSQ090', 'TSQ069'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ176', 'TSQ007', 'TSQ092', 'TSQ106', 'TSQ088', 'TSQ123', 'TSQ041',
       'TSQ043', 'TSQ140', 'TSQ086',
       ...
       'TSQ069', 'TSQ114', 'TSQ129', 'TSQ055', 'TSQ089', 'TSQ146', 'TSQ080',
       'TSQ159', 'TSQ171', 'TSQ090'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ119', 'TSQ163', 'TSQ080', 'TSQ026', 'TSQ101', 'TSQ054', 'TSQ002',
       'TSQ073', 'TSQ114', 'TSQ166',
       ...
       'TSQ121', 'TSQ032', 'TSQ052', 'TSQ137', 'TSQ044', 'TSQ125', 'TSQ069',
       'TSQ145', 'TSQ136', 'TSQ023'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ169', 'TSQ112', 'TSQ032', 'TSQ075', 'TSQ156', 'TSQ111',
       'TSQ166', 'TSQ052', 'TSQ129',
       ...
       'TSQ087', 'TSQ062', 'TSQ114', 'TSQ136', 'TSQ099', 'TSQ070', 'TSQ079',
       'TSQ045', 'TSQ049', 'TSQ078'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ068', 'TSQ099', 'TSQ006', 'TSQ121', 'TSQ028', 'TSQ162', 'TSQ095',
       'TSQ052', 'TSQ159', 'TSQ154',
       ...
       'TSQ170', 'TSQ024', 'TSQ140', 'TSQ053', 'TSQ070', 'TSQ060', 'TSQ108',
       'TSQ005', 'TSQ077', 'TSQ078'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ128', 'TSQ146', 'TSQ174', 'TSQ031', 'TSQ107', 'TSQ121', 'TSQ167',
       'TSQ088', 'TSQ144', 'TSQ039',
       ...
       'TSQ120', 'TSQ005', 'TSQ140', 'TSQ002', 'TSQ074', 'TSQ099', 'TSQ024',
       'TSQ165', 'TSQ010', 'TSQ034'],
      dtype='object', name='Sample_ID', length=109)]