
Readme with Additional Information
==================================


This file contains additional Information for 20220112_kNN_9_random_runs_k7.
# General Information


The conmpressed dataset was analyzed: /Users/Clemens/Documents/MPI Work/autoencoder/data/Encoded data/AE/211019_32_dim_2000_Epochs.pkl 
k value = 9 
other settings were default

The Dataset was split randomly into a test and a training set 
# Information about the random Runs: 



## Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 12.50%

## Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 33.33%	Rep3: 16.67%	Rep4: 20.83%

## Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 25.00%	Rep3: 29.17%	Rep4: 12.50%

## Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 33.33%	Rep3: 29.17%	Rep4: 20.83%

## Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 33.33%	Rep3: 41.67%	Rep4: 12.50%

## Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 41.67%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 16.67%

## Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 16.67%

## Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 8.33%	Rep2: 41.67%	Rep3: 16.67%	Rep4: 33.33%

## Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 Oe_RKG	22 		 that equals to 20.18% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 20.83%

# Random TestSet IDs: 


[Index(['TSQ162', 'TSQ077', 'TSQ145', 'TSQ089', 'TSQ034', 'TSQ126', 'TSQ029',
       'TSQ001', 'TSQ118', 'TSQ028', 'TSQ157', 'TSQ076', 'TSQ153', 'TSQ167',
       'TSQ082', 'TSQ026', 'TSQ006', 'TSQ045', 'TSQ059', 'TSQ135', 'TSQ037',
       'TSQ070', 'TSQ047', 'TSQ074'],
      dtype='object', name='Sample_ID'), Index(['TSQ031', 'TSQ016', 'TSQ051', 'TSQ162', 'TSQ119', 'TSQ003', 'TSQ072',
       'TSQ023', 'TSQ120', 'TSQ006', 'TSQ030', 'TSQ174', 'TSQ134', 'TSQ112',
       'TSQ107', 'TSQ068', 'TSQ044', 'TSQ144', 'TSQ067', 'TSQ077', 'TSQ052',
       'TSQ146', 'TSQ032', 'TSQ046'],
      dtype='object', name='Sample_ID'), Index(['TSQ137', 'TSQ074', 'TSQ079', 'TSQ089', 'TSQ114', 'TSQ156', 'TSQ035',
       'TSQ052', 'TSQ117', 'TSQ045', 'TSQ005', 'TSQ121', 'TSQ034', 'TSQ070',
       'TSQ078', 'TSQ063', 'TSQ009', 'TSQ169', 'TSQ040', 'TSQ039', 'TSQ154',
       'TSQ003', 'TSQ115', 'TSQ123'],
      dtype='object', name='Sample_ID'), Index(['TSQ057', 'TSQ119', 'TSQ158', 'TSQ167', 'TSQ007', 'TSQ121', 'TSQ174',
       'TSQ137', 'TSQ020', 'TSQ031', 'TSQ114', 'TSQ122', 'TSQ038', 'TSQ078',
       'TSQ115', 'TSQ043', 'TSQ066', 'TSQ021', 'TSQ019', 'TSQ134', 'TSQ106',
       'TSQ016', 'TSQ099', 'TSQ055'],
      dtype='object', name='Sample_ID'), Index(['TSQ030', 'TSQ041', 'TSQ055', 'TSQ168', 'TSQ052', 'TSQ101', 'TSQ148',
       'TSQ054', 'TSQ171', 'TSQ161', 'TSQ093', 'TSQ031', 'TSQ135', 'TSQ111',
       'TSQ078', 'TSQ126', 'TSQ153', 'TSQ013', 'TSQ007', 'TSQ166', 'TSQ104',
       'TSQ086', 'TSQ023', 'TSQ176'],
      dtype='object', name='Sample_ID'), Index(['TSQ073', 'TSQ026', 'TSQ049', 'TSQ144', 'TSQ154', 'TSQ148', 'TSQ035',
       'TSQ028', 'TSQ086', 'TSQ002', 'TSQ099', 'TSQ119', 'TSQ027', 'TSQ059',
       'TSQ165', 'TSQ070', 'TSQ020', 'TSQ169', 'TSQ159', 'TSQ074', 'TSQ067',
       'TSQ034', 'TSQ134', 'TSQ079'],
      dtype='object', name='Sample_ID'), Index(['TSQ093', 'TSQ032', 'TSQ040', 'TSQ006', 'TSQ106', 'TSQ129', 'TSQ047',
       'TSQ030', 'TSQ005', 'TSQ088', 'TSQ123', 'TSQ046', 'TSQ126', 'TSQ122',
       'TSQ103', 'TSQ104', 'TSQ100', 'TSQ163', 'TSQ026', 'TSQ117', 'TSQ001',
       'TSQ080', 'TSQ057', 'TSQ038'],
      dtype='object', name='Sample_ID'), Index(['TSQ176', 'TSQ134', 'TSQ143', 'TSQ043', 'TSQ069', 'TSQ044', 'TSQ054',
       'TSQ123', 'TSQ066', 'TSQ088', 'TSQ151', 'TSQ095', 'TSQ162', 'TSQ005',
       'TSQ019', 'TSQ139', 'TSQ174', 'TSQ136', 'TSQ133', 'TSQ065', 'TSQ128',
       'TSQ032', 'TSQ041', 'TSQ040'],
      dtype='object', name='Sample_ID'), Index(['TSQ162', 'TSQ024', 'TSQ063', 'TSQ080', 'TSQ125', 'TSQ101', 'TSQ157',
       'TSQ060', 'TSQ166', 'TSQ146', 'TSQ076', 'TSQ040', 'TSQ055', 'TSQ069',
       'TSQ120', 'TSQ046', 'TSQ111', 'TSQ041', 'TSQ152', 'TSQ065', 'TSQ139',
       'TSQ049', 'TSQ153', 'TSQ001'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ170', 'TSQ133', 'TSQ113', 'TSQ086', 'TSQ156', 'TSQ030', 'TSQ168',
       'TSQ165', 'TSQ062', 'TSQ143',
       ...
       'TSQ099', 'TSQ060', 'TSQ148', 'TSQ122', 'TSQ057', 'TSQ121', 'TSQ134',
       'TSQ111', 'TSQ101', 'TSQ066'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ034', 'TSQ167', 'TSQ043', 'TSQ088', 'TSQ127', 'TSQ066',
       'TSQ169', 'TSQ137', 'TSQ059',
       ...
       'TSQ165', 'TSQ121', 'TSQ062', 'TSQ135', 'TSQ129', 'TSQ168', 'TSQ125',
       'TSQ005', 'TSQ037', 'TSQ104'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ059', 'TSQ024', 'TSQ151', 'TSQ157', 'TSQ125', 'TSQ176', 'TSQ069',
       'TSQ093', 'TSQ002', 'TSQ140',
       ...
       'TSQ090', 'TSQ010', 'TSQ092', 'TSQ088', 'TSQ057', 'TSQ162', 'TSQ077',
       'TSQ161', 'TSQ049', 'TSQ109'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ156', 'TSQ060', 'TSQ152', 'TSQ013', 'TSQ112', 'TSQ059', 'TSQ028',
       'TSQ024', 'TSQ003', 'TSQ154',
       ...
       'TSQ032', 'TSQ143', 'TSQ086', 'TSQ165', 'TSQ026', 'TSQ034', 'TSQ104',
       'TSQ166', 'TSQ117', 'TSQ073'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ006', 'TSQ059', 'TSQ124', 'TSQ034', 'TSQ143', 'TSQ154', 'TSQ163',
       'TSQ024', 'TSQ005', 'TSQ122',
       ...
       'TSQ001', 'TSQ120', 'TSQ134', 'TSQ144', 'TSQ065', 'TSQ082', 'TSQ174',
       'TSQ043', 'TSQ147', 'TSQ119'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ090', 'TSQ101', 'TSQ055', 'TSQ025', 'TSQ112', 'TSQ133', 'TSQ127',
       'TSQ016', 'TSQ003', 'TSQ057',
       ...
       'TSQ054', 'TSQ158', 'TSQ104', 'TSQ168', 'TSQ121', 'TSQ075', 'TSQ080',
       'TSQ123', 'TSQ040', 'TSQ146'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ051', 'TSQ118', 'TSQ170', 'TSQ059', 'TSQ120', 'TSQ034', 'TSQ156',
       'TSQ090', 'TSQ035', 'TSQ127',
       ...
       'TSQ175', 'TSQ028', 'TSQ176', 'TSQ146', 'TSQ112', 'TSQ044', 'TSQ147',
       'TSQ065', 'TSQ162', 'TSQ077'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ060', 'TSQ127', 'TSQ086', 'TSQ013', 'TSQ130', 'TSQ073', 'TSQ016',
       'TSQ156', 'TSQ145', 'TSQ114',
       ...
       'TSQ168', 'TSQ029', 'TSQ112', 'TSQ122', 'TSQ074', 'TSQ100', 'TSQ052',
       'TSQ009', 'TSQ027', 'TSQ024'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ059', 'TSQ047', 'TSQ128', 'TSQ003', 'TSQ118', 'TSQ005', 'TSQ163',
       'TSQ167', 'TSQ027', 'TSQ108',
       ...
       'TSQ175', 'TSQ010', 'TSQ034', 'TSQ038', 'TSQ169', 'TSQ045', 'TSQ165',
       'TSQ127', 'TSQ054', 'TSQ009'],
      dtype='object', name='Sample_ID', length=109)]