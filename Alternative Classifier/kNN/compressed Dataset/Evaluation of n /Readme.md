#Readme 
File with additional information. 

This experiment was done to try to find the optimal k value for the dataset compressed Datsaet (32 dim compressed with the Autoencoder in 19.10.2021). 
The Dataset was split in a stratified Sampled approach 3 times and the error rate was calculated for every k from 1 to 50. The respective plots can be seen in the powerpoint file. The data was stored in a DataFrame (columns for the 9 runs (Dataset 1,2,3 and this was repeated 3 times) and index for the k-value). The k value with the overall lowest Error will be chosen. The kNN method had default setting (except for k value)