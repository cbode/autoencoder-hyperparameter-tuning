
Readme with Additional Information
==================================


This file contains additional Information for 20220111_kNN_9_random_runs_k9.
# General Information

kNN algorithm was used with: 
random splits 
k = 9 (nearest neighbours)
otherwise default settings 


The Dataset was split randomly into a test and a training set 
# #Information about the random Runs: 



##Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 20.83%

##Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Fb_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 37.50%	Rep2: 20.83%	Rep3: 20.83%	Rep4: 20.83%

##Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 37.50%	Rep3: 12.50%	Rep4: 29.17%

##Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 25.00%

##Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 16.67%	Rep3: 50.00%	Rep4: 20.83%

##Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 20.83%

##Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 20.83%

##Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 8.33%

##Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 25.00%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 20.83%

# #Random TestSet IDs: 


[Index(['TSQ027', 'TSQ115', 'TSQ159', 'TSQ074', 'TSQ051', 'TSQ148', 'TSQ005',
       'TSQ047', 'TSQ041', 'TSQ070', 'TSQ080', 'TSQ139', 'TSQ100', 'TSQ121',
       'TSQ065', 'TSQ130', 'TSQ045', 'TSQ019', 'TSQ029', 'TSQ068', 'TSQ125',
       'TSQ157', 'TSQ163', 'TSQ118'],
      dtype='object', name='Sample_ID'), Index(['TSQ148', 'TSQ026', 'TSQ145', 'TSQ059', 'TSQ161', 'TSQ074', 'TSQ024',
       'TSQ025', 'TSQ140', 'TSQ129', 'TSQ062', 'TSQ065', 'TSQ113', 'TSQ088',
       'TSQ049', 'TSQ108', 'TSQ080', 'TSQ072', 'TSQ057', 'TSQ115', 'TSQ002',
       'TSQ127', 'TSQ125', 'TSQ032'],
      dtype='object', name='Sample_ID'), Index(['TSQ009', 'TSQ140', 'TSQ176', 'TSQ117', 'TSQ088', 'TSQ065', 'TSQ007',
       'TSQ108', 'TSQ139', 'TSQ174', 'TSQ069', 'TSQ044', 'TSQ030', 'TSQ053',
       'TSQ034', 'TSQ082', 'TSQ166', 'TSQ156', 'TSQ002', 'TSQ039', 'TSQ107',
       'TSQ010', 'TSQ045', 'TSQ100'],
      dtype='object', name='Sample_ID'), Index(['TSQ174', 'TSQ021', 'TSQ167', 'TSQ030', 'TSQ137', 'TSQ072', 'TSQ162',
       'TSQ104', 'TSQ140', 'TSQ111', 'TSQ005', 'TSQ151', 'TSQ063', 'TSQ032',
       'TSQ114', 'TSQ020', 'TSQ016', 'TSQ060', 'TSQ028', 'TSQ118', 'TSQ003',
       'TSQ139', 'TSQ069', 'TSQ170'],
      dtype='object', name='Sample_ID'), Index(['TSQ176', 'TSQ086', 'TSQ136', 'TSQ159', 'TSQ103', 'TSQ082', 'TSQ038',
       'TSQ166', 'TSQ002', 'TSQ174', 'TSQ153', 'TSQ020', 'TSQ162', 'TSQ167',
       'TSQ145', 'TSQ053', 'TSQ124', 'TSQ125', 'TSQ066', 'TSQ113', 'TSQ109',
       'TSQ101', 'TSQ147', 'TSQ104'],
      dtype='object', name='Sample_ID'), Index(['TSQ075', 'TSQ176', 'TSQ146', 'TSQ073', 'TSQ143', 'TSQ125', 'TSQ134',
       'TSQ126', 'TSQ003', 'TSQ039', 'TSQ129', 'TSQ035', 'TSQ066', 'TSQ051',
       'TSQ175', 'TSQ078', 'TSQ139', 'TSQ168', 'TSQ053', 'TSQ040', 'TSQ100',
       'TSQ082', 'TSQ157', 'TSQ091'],
      dtype='object', name='Sample_ID'), Index(['TSQ124', 'TSQ013', 'TSQ175', 'TSQ031', 'TSQ130', 'TSQ063', 'TSQ107',
       'TSQ055', 'TSQ146', 'TSQ034', 'TSQ103', 'TSQ122', 'TSQ040', 'TSQ135',
       'TSQ069', 'TSQ080', 'TSQ044', 'TSQ171', 'TSQ047', 'TSQ062', 'TSQ151',
       'TSQ093', 'TSQ170', 'TSQ127'],
      dtype='object', name='Sample_ID'), Index(['TSQ037', 'TSQ052', 'TSQ134', 'TSQ035', 'TSQ126', 'TSQ151', 'TSQ068',
       'TSQ148', 'TSQ112', 'TSQ065', 'TSQ078', 'TSQ046', 'TSQ159', 'TSQ005',
       'TSQ076', 'TSQ137', 'TSQ032', 'TSQ001', 'TSQ043', 'TSQ114', 'TSQ013',
       'TSQ062', 'TSQ175', 'TSQ123'],
      dtype='object', name='Sample_ID'), Index(['TSQ111', 'TSQ112', 'TSQ108', 'TSQ157', 'TSQ005', 'TSQ103', 'TSQ024',
       'TSQ076', 'TSQ148', 'TSQ001', 'TSQ158', 'TSQ109', 'TSQ114', 'TSQ030',
       'TSQ074', 'TSQ052', 'TSQ159', 'TSQ053', 'TSQ047', 'TSQ176', 'TSQ023',
       'TSQ107', 'TSQ068', 'TSQ154'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ066', 'TSQ171', 'TSQ144', 'TSQ077', 'TSQ166', 'TSQ114', 'TSQ016',
       'TSQ143', 'TSQ128', 'TSQ034',
       ...
       'TSQ069', 'TSQ046', 'TSQ057', 'TSQ089', 'TSQ175', 'TSQ023', 'TSQ024',
       'TSQ055', 'TSQ140', 'TSQ001'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ060', 'TSQ066', 'TSQ119', 'TSQ151', 'TSQ087', 'TSQ166', 'TSQ106',
       'TSQ029', 'TSQ120', 'TSQ016',
       ...
       'TSQ167', 'TSQ123', 'TSQ121', 'TSQ159', 'TSQ157', 'TSQ001', 'TSQ020',
       'TSQ126', 'TSQ006', 'TSQ040'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ165', 'TSQ067', 'TSQ005', 'TSQ101', 'TSQ163', 'TSQ031', 'TSQ078',
       'TSQ168', 'TSQ074', 'TSQ059',
       ...
       'TSQ077', 'TSQ127', 'TSQ153', 'TSQ129', 'TSQ073', 'TSQ114', 'TSQ147',
       'TSQ028', 'TSQ115', 'TSQ135'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ002', 'TSQ175', 'TSQ106', 'TSQ134', 'TSQ026', 'TSQ034', 'TSQ055',
       'TSQ119', 'TSQ049', 'TSQ078',
       ...
       'TSQ052', 'TSQ043', 'TSQ100', 'TSQ019', 'TSQ054', 'TSQ109', 'TSQ136',
       'TSQ113', 'TSQ070', 'TSQ066'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ031', 'TSQ032', 'TSQ107', 'TSQ154', 'TSQ054', 'TSQ111', 'TSQ127',
       'TSQ026', 'TSQ044', 'TSQ046',
       ...
       'TSQ100', 'TSQ143', 'TSQ001', 'TSQ027', 'TSQ055', 'TSQ074', 'TSQ019',
       'TSQ028', 'TSQ059', 'TSQ123'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ092', 'TSQ025', 'TSQ046', 'TSQ069', 'TSQ074', 'TSQ034', 'TSQ006',
       'TSQ052', 'TSQ104', 'TSQ093',
       ...
       'TSQ043', 'TSQ174', 'TSQ002', 'TSQ103', 'TSQ095', 'TSQ032', 'TSQ007',
       'TSQ087', 'TSQ060', 'TSQ027'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ123', 'TSQ082', 'TSQ147', 'TSQ099', 'TSQ066', 'TSQ045', 'TSQ157',
       'TSQ161', 'TSQ072', 'TSQ101',
       ...
       'TSQ117', 'TSQ026', 'TSQ049', 'TSQ028', 'TSQ119', 'TSQ057', 'TSQ148',
       'TSQ006', 'TSQ167', 'TSQ067'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ024', 'TSQ136', 'TSQ087', 'TSQ124', 'TSQ003', 'TSQ108', 'TSQ055',
       'TSQ020', 'TSQ117', 'TSQ145',
       ...
       'TSQ007', 'TSQ157', 'TSQ107', 'TSQ092', 'TSQ063', 'TSQ030', 'TSQ028',
       'TSQ031', 'TSQ034', 'TSQ115'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ003', 'TSQ162', 'TSQ151', 'TSQ073', 'TSQ128', 'TSQ021', 'TSQ143',
       'TSQ121', 'TSQ139', 'TSQ115',
       ...
       'TSQ026', 'TSQ040', 'TSQ118', 'TSQ049', 'TSQ059', 'TSQ133', 'TSQ086',
       'TSQ119', 'TSQ079', 'TSQ123'],
      dtype='object', name='Sample_ID', length=109)]