
Readme with Additional Information
==================================


This file contains additional Information for 20220111_kNN_9_random_runs.
# General Information

No of Runs: 9
Algorithm: kNN with Standard settings 
n_neighbours = 2 

The Dataset was split randomly into a test and a training set 
# #Information about the random Runs: 



##Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 16.67%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 25.00%

##Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 12.50%	Rep2: 29.17%	Rep3: 29.17%	Rep4: 29.17%

##Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 33.33%	Rep2: 20.83%	Rep3: 29.17%	Rep4: 16.67%

##Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	8 		 that equals to 33.33% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 25.00%	Rep3: 41.67%	Rep4: 16.67%

##Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 8.33%	Rep2: 25.00%	Rep3: 33.33%	Rep4: 33.33%

##Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	24 		 that equals to 22.02% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Fb_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 12.50%	Rep2: 20.83%	Rep3: 25.00%	Rep4: 41.67%

##Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 37.50%	Rep3: 16.67%	Rep4: 25.00%

##Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	23 		 that equals to 21.10% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	14 		 that equals to 12.84% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	8 		 that equals to 33.33% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 29.17%	Rep3: 33.33%	Rep4: 16.67%

##Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 8.33%	Rep2: 33.33%	Rep3: 16.67%	Rep4: 41.67%

# #Random TestSet IDs: 



##Round 0:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_RKG	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
		 He_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 He_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_RKG	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 20.83%	Rep2: 33.33%	Rep3: 25.00%	Rep4: 20.83%

##Round 1:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Oe_G3	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 12.50%	Rep3: 33.33%	Rep4: 20.83%

##Round 2:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	19 		 that equals to 17.43% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 He_G3	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 12.50%	Rep2: 8.33%	Rep3: 50.00%	Rep4: 29.17%

##Round 3:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	2 		 that equals to 8.33% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 33.33%	Rep2: 25.00%	Rep3: 25.00%	Rep4: 16.67%

##Round 4:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 He_RKG	16 		 that equals to 14.68% of the total amount of Genes
		 Oe_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 He_RKG	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Fb_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 29.17%	Rep3: 25.00%	Rep4: 29.17%

##Round 5:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	21 		 that equals to 19.27% of the total amount of Genes
		 He_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	13 		 that equals to 11.93% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_RKG	7 		 that equals to 29.17% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Oe_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	2 		 that equals to 8.33% of the total amount of Genes
		 Oe_G3	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 20.83%	Rep2: 20.83%	Rep3: 33.33%	Rep4: 25.00%

##Round 6:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Fb_G3	22 		 that equals to 20.18% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Oe_G3	17 		 that equals to 15.60% of the total amount of Genes
		 He_G3	17 		 that equals to 15.60% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_G3	3 		 that equals to 12.50% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	3 		 that equals to 12.50% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 16.67%	Rep4: 29.17%

##Round 7:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_RKG	21 		 that equals to 19.27% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_RKG	19 		 that equals to 17.43% of the total amount of Genes
		 Fb_RKG	17 		 that equals to 15.60% of the total amount of Genes
		 Oe_G3	16 		 that equals to 14.68% of the total amount of Genes
		 He_G3	16 		 that equals to 14.68% of the total amount of Genes
test_df:
	Overview of Data:
		 Oe_G3	6 		 that equals to 25.00% of the total amount of Genes
		 He_G3	6 		 that equals to 25.00% of the total amount of Genes
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Fb_RKG	3 		 that equals to 12.50% of the total amount of Genes
		 Oe_RKG	1 		 that equals to 4.17% of the total amount of Genes
	Rep1: 16.67%	Rep2: 29.17%	Rep3: 20.83%	Rep4: 33.33%

##Round 8:
Splitting into training and test data was successfull.
train_df:
	Overview of Data:
		 Oe_G3	20 		 that equals to 18.35% of the total amount of Genes
		 Fb_G3	20 		 that equals to 18.35% of the total amount of Genes
		 He_G3	18 		 that equals to 16.51% of the total amount of Genes
		 He_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Oe_RKG	18 		 that equals to 16.51% of the total amount of Genes
		 Fb_RKG	15 		 that equals to 13.76% of the total amount of Genes
test_df:
	Overview of Data:
		 Fb_G3	5 		 that equals to 20.83% of the total amount of Genes
		 Fb_RKG	5 		 that equals to 20.83% of the total amount of Genes
		 He_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 He_G3	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_RKG	4 		 that equals to 16.67% of the total amount of Genes
		 Oe_G3	2 		 that equals to 8.33% of the total amount of Genes
	Rep1: 29.17%	Rep2: 25.00%	Rep3: 37.50%	Rep4: 8.33%

# #Random TestSet IDs: 


[Index(['TSQ108', 'TSQ060', 'TSQ041', 'TSQ146', 'TSQ019', 'TSQ117', 'TSQ049',
       'TSQ121', 'TSQ157', 'TSQ021', 'TSQ030', 'TSQ086', 'TSQ074', 'TSQ093',
       'TSQ067', 'TSQ158', 'TSQ151', 'TSQ078', 'TSQ134', 'TSQ130', 'TSQ029',
       'TSQ001', 'TSQ028', 'TSQ168'],
      dtype='object', name='Sample_ID'), Index(['TSQ092', 'TSQ147', 'TSQ119', 'TSQ068', 'TSQ152', 'TSQ121', 'TSQ093',
       'TSQ145', 'TSQ069', 'TSQ170', 'TSQ034', 'TSQ039', 'TSQ176', 'TSQ067',
       'TSQ020', 'TSQ091', 'TSQ082', 'TSQ148', 'TSQ095', 'TSQ028', 'TSQ166',
       'TSQ003', 'TSQ070', 'TSQ025'],
      dtype='object', name='Sample_ID'), Index(['TSQ025', 'TSQ059', 'TSQ089', 'TSQ127', 'TSQ112', 'TSQ106', 'TSQ109',
       'TSQ099', 'TSQ075', 'TSQ068', 'TSQ152', 'TSQ133', 'TSQ162', 'TSQ122',
       'TSQ100', 'TSQ137', 'TSQ125', 'TSQ123', 'TSQ148', 'TSQ144', 'TSQ165',
       'TSQ124', 'TSQ135', 'TSQ031'],
      dtype='object', name='Sample_ID'), Index(['TSQ030', 'TSQ175', 'TSQ169', 'TSQ089', 'TSQ028', 'TSQ119', 'TSQ086',
       'TSQ103', 'TSQ147', 'TSQ078', 'TSQ170', 'TSQ057', 'TSQ137', 'TSQ007',
       'TSQ002', 'TSQ010', 'TSQ060', 'TSQ019', 'TSQ025', 'TSQ023', 'TSQ079',
       'TSQ128', 'TSQ082', 'TSQ152'],
      dtype='object', name='Sample_ID'), Index(['TSQ175', 'TSQ046', 'TSQ038', 'TSQ039', 'TSQ162', 'TSQ108', 'TSQ088',
       'TSQ026', 'TSQ076', 'TSQ154', 'TSQ136', 'TSQ128', 'TSQ043', 'TSQ117',
       'TSQ006', 'TSQ113', 'TSQ114', 'TSQ093', 'TSQ003', 'TSQ021', 'TSQ169',
       'TSQ170', 'TSQ161', 'TSQ032'],
      dtype='object', name='Sample_ID'), Index(['TSQ076', 'TSQ104', 'TSQ001', 'TSQ072', 'TSQ091', 'TSQ118', 'TSQ038',
       'TSQ174', 'TSQ127', 'TSQ136', 'TSQ153', 'TSQ145', 'TSQ165', 'TSQ148',
       'TSQ171', 'TSQ158', 'TSQ070', 'TSQ066', 'TSQ051', 'TSQ043', 'TSQ057',
       'TSQ020', 'TSQ144', 'TSQ128'],
      dtype='object', name='Sample_ID'), Index(['TSQ069', 'TSQ040', 'TSQ003', 'TSQ154', 'TSQ095', 'TSQ019', 'TSQ070',
       'TSQ030', 'TSQ086', 'TSQ035', 'TSQ157', 'TSQ106', 'TSQ144', 'TSQ090',
       'TSQ121', 'TSQ120', 'TSQ075', 'TSQ020', 'TSQ140', 'TSQ092', 'TSQ038',
       'TSQ153', 'TSQ023', 'TSQ080'],
      dtype='object', name='Sample_ID'), Index(['TSQ167', 'TSQ023', 'TSQ123', 'TSQ106', 'TSQ079', 'TSQ073', 'TSQ154',
       'TSQ075', 'TSQ129', 'TSQ049', 'TSQ052', 'TSQ176', 'TSQ078', 'TSQ086',
       'TSQ124', 'TSQ127', 'TSQ019', 'TSQ077', 'TSQ117', 'TSQ039', 'TSQ174',
       'TSQ147', 'TSQ152', 'TSQ135'],
      dtype='object', name='Sample_ID'), Index(['TSQ118', 'TSQ067', 'TSQ107', 'TSQ093', 'TSQ134', 'TSQ065', 'TSQ055',
       'TSQ122', 'TSQ037', 'TSQ125', 'TSQ001', 'TSQ077', 'TSQ158', 'TSQ170',
       'TSQ009', 'TSQ045', 'TSQ057', 'TSQ075', 'TSQ159', 'TSQ135', 'TSQ113',
       'TSQ072', 'TSQ035', 'TSQ044'],
      dtype='object', name='Sample_ID')]
# Random TrainingSet IDs: 


[Index(['TSQ128', 'TSQ127', 'TSQ154', 'TSQ072', 'TSQ095', 'TSQ003', 'TSQ148',
       'TSQ103', 'TSQ034', 'TSQ066',
       ...
       'TSQ123', 'TSQ013', 'TSQ122', 'TSQ140', 'TSQ118', 'TSQ166', 'TSQ109',
       'TSQ005', 'TSQ025', 'TSQ057'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ168', 'TSQ031', 'TSQ135', 'TSQ167', 'TSQ154', 'TSQ126', 'TSQ049',
       'TSQ107', 'TSQ106', 'TSQ136',
       ...
       'TSQ044', 'TSQ043', 'TSQ016', 'TSQ023', 'TSQ029', 'TSQ109', 'TSQ060',
       'TSQ158', 'TSQ162', 'TSQ100'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ157', 'TSQ041', 'TSQ040', 'TSQ044', 'TSQ006', 'TSQ134', 'TSQ003',
       'TSQ161', 'TSQ129', 'TSQ130',
       ...
       'TSQ043', 'TSQ035', 'TSQ052', 'TSQ080', 'TSQ121', 'TSQ101', 'TSQ120',
       'TSQ030', 'TSQ143', 'TSQ139'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ159', 'TSQ174', 'TSQ154', 'TSQ168', 'TSQ063', 'TSQ095', 'TSQ087',
       'TSQ052', 'TSQ029', 'TSQ129',
       ...
       'TSQ139', 'TSQ027', 'TSQ111', 'TSQ037', 'TSQ134', 'TSQ099', 'TSQ075',
       'TSQ120', 'TSQ125', 'TSQ035'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ144', 'TSQ166', 'TSQ034', 'TSQ133', 'TSQ126', 'TSQ112', 'TSQ091',
       'TSQ121', 'TSQ030', 'TSQ075',
       ...
       'TSQ118', 'TSQ049', 'TSQ055', 'TSQ069', 'TSQ140', 'TSQ020', 'TSQ129',
       'TSQ041', 'TSQ159', 'TSQ167'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ143', 'TSQ039', 'TSQ161', 'TSQ029', 'TSQ044', 'TSQ045', 'TSQ032',
       'TSQ157', 'TSQ101', 'TSQ175',
       ...
       'TSQ139', 'TSQ114', 'TSQ133', 'TSQ099', 'TSQ007', 'TSQ126', 'TSQ117',
       'TSQ003', 'TSQ078', 'TSQ006'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ005', 'TSQ165', 'TSQ156', 'TSQ074', 'TSQ170', 'TSQ047', 'TSQ088',
       'TSQ059', 'TSQ079', 'TSQ134',
       ...
       'TSQ123', 'TSQ137', 'TSQ171', 'TSQ125', 'TSQ118', 'TSQ152', 'TSQ007',
       'TSQ009', 'TSQ175', 'TSQ031'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ035', 'TSQ158', 'TSQ002', 'TSQ151', 'TSQ057', 'TSQ157', 'TSQ092',
       'TSQ029', 'TSQ161', 'TSQ122',
       ...
       'TSQ120', 'TSQ070', 'TSQ067', 'TSQ115', 'TSQ051', 'TSQ119', 'TSQ024',
       'TSQ101', 'TSQ082', 'TSQ055'],
      dtype='object', name='Sample_ID', length=109), Index(['TSQ049', 'TSQ115', 'TSQ124', 'TSQ165', 'TSQ006', 'TSQ163', 'TSQ161',
       'TSQ046', 'TSQ051', 'TSQ171',
       ...
       'TSQ169', 'TSQ140', 'TSQ128', 'TSQ010', 'TSQ133', 'TSQ025', 'TSQ023',
       'TSQ104', 'TSQ120', 'TSQ034'],
      dtype='object', name='Sample_ID', length=109)]